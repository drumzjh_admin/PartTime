package com.example.parttime.task;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.parttime.R;
import com.example.parttime.ScanCodeActivity;
import com.example.parttime.adapter.MyAcceptAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_tasksign;
import com.example.parttime.service.Url;
import com.example.parttime.square.SquareDetailedActivity;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.rxjava.rxlife.RxLife;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//我接受的任务页面
public class MyAcceptActivity extends AppCompatActivity {

    TextView MA_textView_title;
    TextView MA_textView_peopleName;
    TextView MA_textView_money;
    TextView MA_textView_location;
    TextView MA_textView_timeData;
    ImageView MA_imageView_peopleHead;
    Button MA_button_sign;
    RecyclerView MA_recyclerView;
    View MA_view;
    ConstraintLayout constraintLayout13;

    Entity_taskData.ResultBean.DsBean squareDetailed;
    private String taskId;//任务ID
    private String stringPeopleId;//登录人ID
    private String isSignOutIn;//签到或签退得

    private MyAcceptAdapter myAcceptAdapter;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_accept);
        initActivity();

        //获取传值任务的ID
        taskId = getIntent().getStringExtra("taskId");

        if (taskId != null) {
            getTaskData();
        } else {
            Toasty.warning(getApplicationContext(), "任务加载出错", Toasty.LENGTH_SHORT).show();
            finish();
        }

        //已完成的任务隐藏扫码按钮
        if(getIntent().getBooleanExtra("isScanButton",true) == false){
            MA_button_sign.setVisibility(View.GONE);
        }

        //获取从首页扫码跳转到此页面的二维码数据
        if(getIntent().getStringExtra("scanresult") != null){
            toDealWithScanCode(getIntent().getStringExtra("scanresult"));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK && requestCode == 1 && data !=null){
            //获取扫码数据
            String result = data.getStringExtra("scanCode");
            toDealWithScanCode(result);
        }

    }

    //处理扫码数据
    private void toDealWithScanCode(String result){
        //处理扫码数据
        if(result ==null || result.equals("")){
            Toasty.warning(getApplicationContext(),"请扫描正确的二维码",Toasty.LENGTH_SHORT).show();
        }else {
            if(isSignOutIn.equals("0")){//签到
                new XPopup.Builder(MyAcceptActivity.this).asConfirm("提示", "确认签到","取消","确定", new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        setTasksign(result,isSignOutIn,"签到");
                    }
                },null,false).show();
            }else {//签退
                new XPopup.Builder(MyAcceptActivity.this).asConfirm("提示", "确认签退","取消","确定", new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
                        setTasksign(result,isSignOutIn,"签退");
                    }
                },null,false).show();
            }
        }
    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //扫码
    public void click_scan(View view){
        startActivityForResult(new Intent(this, ScanCodeActivity.class),1);
    }

    //加载任务信息
    private void getTaskData() {

        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        //String uploadData = "M_task_list.remark2='1' and M_task_list_id ='"+taskId+"'";
        String uploadData = "M_task_list.id ='" + taskId + "'";

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", uploadData)//添加请求参数，该方法可调用多次
                .add("sindex","0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        squareDetailed = entity_taskData.getResult().getDs().get(0);
                        //界面值渲染
                        MA_textView_title.setText(squareDetailed.getM_task_list_remark5());
                        MA_textView_peopleName.setText(squareDetailed.getM_employee_list_remark1());
                        MA_textView_money.setText(squareDetailed.getMoney() + "元/天");
                        MA_textView_location.setText("工作地点:" + squareDetailed.getAddress4());
                        MA_textView_timeData.setText("工作时间:" + squareDetailed.getWorktimeStart() + "-" + squareDetailed.getWorktimeEnd());
                        if(squareDetailed.getM_task_list_remark3().equals("") || squareDetailed.getM_task_list_remark3() == null){
                            MA_imageView_peopleHead.setImageResource(R.drawable.zhanweihead);
                        }else {
                            Glide.with(getApplicationContext()).load(Url.url_img+squareDetailed.getM_task_list_remark3()).into(MA_imageView_peopleHead);
                        }
                        getTaskSignData();

                        constraintLayout13.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //去任务详情页
                                Intent intent = new Intent(getApplicationContext(), SquareDetailedActivity.class);
                                intent.putExtra("squareDetailed", squareDetailed);
                                intent.putExtra("from","1");
                                startActivity(intent);
                            }
                        });

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Log.v("entity_taskData", "无数据");
                        } else {
                            Toasty.warning(getApplicationContext(), "未获取到任务信息", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), "任务加载错误", Toasty.LENGTH_SHORT).show();
                });


    }

    //获取签到状态
    private void getTaskSignData() {

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTasksign)//发送Get请求（获取使用中的任务）
                .add("employee_listid", stringPeopleId)//添加请求参数，该方法可调用多次
                .add("taskid", taskId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {

                        Entity_tasksign entity_tasksign = gson.fromJson(jsonResult, Entity_tasksign.class);
                        //未签退
                        if (entity_tasksign.getResult().getDs().get(0).getEndtime() ==null || entity_tasksign.getResult().getDs().get(0).getEndtime().equals("")){
                            isSignOutIn = "1";
                            MA_button_sign.setText("扫码签退");
                        }else {
                            isSignOutIn = "0";
                            MA_button_sign.setText("扫码签到");
                        }

                        myAcceptAdapter.setList(entity_tasksign.getResult().getDs());
                        MA_recyclerView.setAdapter(myAcceptAdapter);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            isSignOutIn = "0";
                            Log.v("entity_taskData", "无数据");
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), "签到加载错误", Toasty.LENGTH_SHORT).show();
                });

    }

    //签到
    private void setTasksign(String code,String type,String typeMsg){

        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading(typeMsg+"中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_addTasksign)//发送Get请求（获取使用中的任务）
                .add("employee_listid", stringPeopleId)//添加请求参数，该方法可调用多次
                .add("taskid", code)//添加请求参数，该方法可调用多次
                .add("datetime", HelpTool.getDateTime())//添加请求参数，该方法可调用多次
                .add("type", type)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();
                    //请求成功
                    MA_button_sign.setText("扫码"+typeMsg);
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(getApplicationContext(), typeMsg+"成功", Toasty.LENGTH_SHORT).show();
                        getTaskSignData();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if(entity_httpResult.getErrorMsg().equals("没有此任务不能添加")){
                            Toasty.warning(getApplicationContext(), "请扫描正确的签到码", Toasty.LENGTH_SHORT).show();
                        }else {
                            Toasty.warning(getApplicationContext(), typeMsg+"失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), typeMsg+"错误", Toasty.LENGTH_SHORT).show();
                });

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MA_textView_title = this.findViewById(R.id.MA_textView_title);
        MA_textView_peopleName = this.findViewById(R.id.MA_textView_peopleName);
        MA_textView_money = this.findViewById(R.id.MA_textView_money);
        MA_textView_location = this.findViewById(R.id.MA_textView_location);
        MA_textView_timeData = this.findViewById(R.id.MA_textView_timeData);
        MA_imageView_peopleHead = this.findViewById(R.id.MA_imageView_peopleHead);
        MA_button_sign = this.findViewById(R.id.MA_button_sign);
        MA_recyclerView = this.findViewById(R.id.MA_recyclerView);
        MA_view = this.findViewById(R.id.MA_view);
        MA_recyclerView.setLayoutManager(new LinearLayoutManager(this));

        myAcceptAdapter = new MyAcceptAdapter(R.layout.item_myaccept);
        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

        constraintLayout13 = findViewById(R.id.constraintLayout13);

    }

}