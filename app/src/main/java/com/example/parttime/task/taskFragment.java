package com.example.parttime.task;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.duma.ld.mylibrary.SwitchView;
import com.example.parttime.R;
import com.example.parttime.adapter.TaskAcceptManagementAdapter;
import com.example.parttime.adapter.TaskPublishManagementAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_taskManagement;
import com.example.parttime.massage.ImCommunicationActivity;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.rxjava.rxlife.RxLife;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

/**
 * A simple {@link Fragment} subclass.
 */
//个人任务页面
public class taskFragment extends Fragment {

    SwitchView FT_switchView;
    TabLayout FT_tabLayout;
    RecyclerView FT_recyclerView;

    //存储已接和发布任务数据
    private List<Entity_taskManagement.ResultBean.DsBean> acceptTaskListWKS = new ArrayList<>();
    private List<Entity_taskManagement.ResultBean.DsBean> acceptTaskListJXZ = new ArrayList<>();
    private List<Entity_taskManagement.ResultBean.DsBean> acceptTaskListYWC = new ArrayList<>();
    private List<Entity_taskData.ResultBean.DsBean> publishTaskList;

    private TaskAcceptManagementAdapter taskAcceptManagementAdapter;
    private TaskPublishManagementAdapter taskPublishManagementAdapter;

    private String stringPeopleId;//登录人ID

    private int selectedTabID = 0;//存储选中的tabId
    private int selectedSwitchID = 0;//存储选中的switchId

    public taskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View taskView = inflater.inflate(R.layout.fragment_task, container, false);
        return taskView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initActivity();
    }

    //加载已接任务
    private void getAcceptTask() {

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_acceptTaskList)//发送Get请求（获取使用中的任务）
                .add("employee_listid", stringPeopleId)
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskManagement entity_taskManagement = gson.fromJson(jsonResult, Entity_taskManagement.class);

                        //循环设置数组
                        List<Entity_taskManagement.ResultBean.DsBean> listWKS = new ArrayList<>();
                        List<Entity_taskManagement.ResultBean.DsBean> listJXZ = new ArrayList<>();
                        List<Entity_taskManagement.ResultBean.DsBean> listYWC = new ArrayList<>();

                        for (int i = 0; i < entity_taskManagement.getResult().getDs().size(); i++) {

                            if (entity_taskManagement.getResult().getDs().get(i).getType().equals("未开始")) {//未开始
                                listWKS.add(entity_taskManagement.getResult().getDs().get(i));
                            } else if (entity_taskManagement.getResult().getDs().get(i).getType().equals("进行中")) {//进行中
                                listJXZ.add(entity_taskManagement.getResult().getDs().get(i));
                            } else if (entity_taskManagement.getResult().getDs().get(i).getType().equals("已结束")) {//已完成
                                listYWC.add(entity_taskManagement.getResult().getDs().get(i));
                            }

                        }

                        //未开始
                        if (acceptTaskListWKS.size() < listWKS.size()) {

                            acceptTaskListWKS = new ArrayList<>();
                            acceptTaskListWKS = listWKS;
                            if (selectedTabID == 0) {
                                taskAcceptManagementAdapter.setList(acceptTaskListWKS);
                            }

                        }
                        //进行中
                        if (acceptTaskListJXZ.size() < listJXZ.size()) {

                            acceptTaskListJXZ = new ArrayList<>();
                            acceptTaskListJXZ = listJXZ;
                            if (selectedTabID == 1) {
                                taskAcceptManagementAdapter.setList(acceptTaskListJXZ);
                            }

                            //存储进行中任务用于扫码时校验是否有当前任务
                            String[] jxzTask = new String[acceptTaskListJXZ.size()];
                            for (int i = 0; i < acceptTaskListJXZ.size(); i++) {
                                jxzTask[i] = acceptTaskListJXZ.get(i).getM_task_listid();
                            }
                            HelpTool.setSharedPreferenceArray(StorageConfig.EDITOR_TASKID_JXZ, jxzTask, getActivity());

                        }
                        //已完成
                        if (acceptTaskListYWC.size() < listYWC.size()) {

                            acceptTaskListYWC = new ArrayList<>();
                            acceptTaskListYWC = listYWC;
                            if (selectedTabID == 2) {
                                taskAcceptManagementAdapter.setList(acceptTaskListYWC);
                            }

                        }

                    } else if (jsonStates.equals("0")) {
                        Log.v("获取任务列表", "未获取到数据");
                    }

                }, throwable -> {
                    //请求失败
                    Log.v("获取任务列表", "失败");
                });

    }

    //加载发布任务
    private void getTaskData() {

        String uploadData = "M_task_list.remark2='1' and employee_list_id ='" + stringPeopleId + "'";

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", uploadData)//添加请求参数，该方法可调用多次
                .add("sindex", "0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        //显示任务至页面
                        publishTaskList = new ArrayList<>();
                        publishTaskList = entity_taskData.getResult().getDs();

                        if (publishTaskList.size() > 0) {

                            if (selectedSwitchID == 1) {
                                taskPublishManagementAdapter.setList(publishTaskList);
                                FT_recyclerView.setAdapter(taskPublishManagementAdapter);
                            }

                        } else {
                            Toasty.warning(getContext(), "暂无任务信息", Toasty.LENGTH_SHORT).show();
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Log.v("entity_taskData", "无数据");
                        } else {
                            Toasty.warning(getContext(), "任务加载失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(getContext(), "任务加载错误", Toasty.LENGTH_SHORT).show();
                });

    }

    //取消任务
    private void cancelTask(int position) {

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_cancelTask)//发送Get请求（获取使用中的任务）
                .add("id", acceptTaskListWKS.get(position).getM_OrderTakingListid())//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("取消成功")) {
                            Toasty.warning(getContext(), "任务取消成功", Toasty.LENGTH_SHORT).show();
                            getAcceptTask();
                        } else {
                            Toasty.warning(getContext(), "任务取消失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(getContext(), "任务取消错误", Toasty.LENGTH_SHORT).show();
                });

    }

    //顶部switch切换
    SwitchView.onClickCheckedListener onClickCheckedListenerSwitchTask = new SwitchView.onClickCheckedListener() {
        @Override
        public void onClick() {

            if (FT_switchView.isChecked()) {//发布

                selectedSwitchID = 1;
                FT_tabLayout.setVisibility(View.GONE);
                //展示已发布任务
                if (publishTaskList != null) {
                    taskPublishManagementAdapter.setList(publishTaskList);
                    FT_recyclerView.setAdapter(taskPublishManagementAdapter);
                } else {
                    getTaskData();
                }

            } else {//接受

                selectedSwitchID = 0;
                FT_tabLayout.getTabAt(0).select();
                FT_tabLayout.setVisibility(View.VISIBLE);
                taskAcceptManagementAdapter.setList(acceptTaskListWKS);
                FT_recyclerView.setAdapter(taskAcceptManagementAdapter);

            }

        }
    };

    //顶部tab切换
    TabLayout.OnTabSelectedListener onTabSelectedListenerTabTask = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {//选中

            if (tab.getText().equals("未开始")) {//未开始

                selectedTabID = 0;
                taskAcceptManagementAdapter.setList(acceptTaskListWKS);
                FT_recyclerView.setAdapter(taskAcceptManagementAdapter);

            } else if (tab.getText().equals("进行中")) {//进行中

                selectedTabID = 1;
                taskAcceptManagementAdapter.setList(acceptTaskListJXZ);
                FT_recyclerView.setAdapter(taskAcceptManagementAdapter);


            } else if (tab.getText().equals("已结束")) {//已结束

                selectedTabID = 2;
                taskAcceptManagementAdapter.setList(acceptTaskListYWC);
                FT_recyclerView.setAdapter(taskAcceptManagementAdapter);

            }

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {//取消选中时

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {//点击已选中

        }
    };

    //点击接受页面
    OnItemClickListener onItemClickListenerAccept = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

            String taskId = "0";

            Intent intent = new Intent(getContext(), MyAcceptActivity.class);
            if (FT_tabLayout.getTabAt(0).isSelected()) {
                taskId = acceptTaskListWKS.get(position).getM_task_listid();
                intent.putExtra("isScanButton", false);
            } else if (FT_tabLayout.getTabAt(1).isSelected()) {
                taskId = acceptTaskListJXZ.get(position).getM_task_listid();
                intent.putExtra("isScanButton", true);
            } else if (FT_tabLayout.getTabAt(2).isSelected()) {
                taskId = acceptTaskListYWC.get(position).getM_task_listid();
                intent.putExtra("isScanButton", false);
            }
            intent.putExtra("taskId", taskId);
            startActivity(intent);

        }
    };

    //点击发布页面
    OnItemClickListener onItemClickListenerPublish = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
            String taskId = publishTaskList.get(position).getM_task_list_id();

            Intent intent = new Intent(getContext(), MyPublishActivity.class);
            intent.putExtra("taskId", taskId);
            startActivity(intent);
        }
    };

    //点击item里的button
    OnItemChildClickListener onItemChildClickListenerAdapter = new OnItemChildClickListener() {
        @Override
        public void onItemChildClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {

            if (view.getId() == R.id.IT_button_call) {//联系雇主

                String taskId = "0";
                if (FT_tabLayout.getTabAt(0).isSelected()) {
                    taskId = acceptTaskListWKS.get(position).getM_task_listid();
                } else if (FT_tabLayout.getTabAt(1).isSelected()) {
                    taskId = acceptTaskListJXZ.get(position).getM_task_listid();
                } else if (FT_tabLayout.getTabAt(2).isSelected()) {
                    taskId = acceptTaskListYWC.get(position).getM_task_listid();
                }

                Intent intent = new Intent(getContext(), ImCommunicationActivity.class);
                intent.putExtra("taskId", taskId);
                startActivity(intent);

            } else if (view.getId() == R.id.IT_button_cancel) {//取消任务
                cancelTask(position);
            } else if (view.getId() == R.id.IT_button_evaluation) {//评价

            }

        }
    };

    //初始化页面工作
    private void initActivity() {

        //页面控件绑定
        FT_switchView = getActivity().findViewById(R.id.FT_switchView);
        FT_tabLayout = getActivity().findViewById(R.id.FT_tabLayout);
        FT_recyclerView = getActivity().findViewById(R.id.FT_recyclerView);
        if(FT_recyclerView!=null) {
            FT_recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }

        //适配器绑定
        taskAcceptManagementAdapter = new TaskAcceptManagementAdapter(null);
        taskPublishManagementAdapter = new TaskPublishManagementAdapter(null);

        //监听控件事件
        if(taskAcceptManagementAdapter!=null) {
            taskAcceptManagementAdapter.addChildClickViewIds(R.id.IT_button_call, R.id.IT_button_cancel, R.id.IT_button_evaluation);
            taskAcceptManagementAdapter.setOnItemChildClickListener(onItemChildClickListenerAdapter);
            taskAcceptManagementAdapter.setOnItemClickListener(onItemClickListenerAccept);
            taskPublishManagementAdapter.setOnItemClickListener(onItemClickListenerPublish);
        }

        if(FT_switchView!=null) {
            FT_switchView.setOnClickCheckedListener(onClickCheckedListenerSwitchTask);
        }

        if(FT_tabLayout!=null) {
            FT_tabLayout.addOnTabSelectedListener(onTabSelectedListenerTabTask);
        }

        //登录人ID
        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getContext());

    }

    @Override
    public void onResume() {
        super.onResume();

        //加载已接任务(switch为任务并且tab选中未开始时)
//        if (!FT_switchView.isChecked() && FT_tabLayout.getTabAt(0).isSelected()) {
//            getAcceptTask();
//        }
        getAcceptTask();
        //加载发布任务
        getTaskData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //ImmersionBar.destroy(this);
    }


}