package com.example.parttime.task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parttime.R;
import com.example.parttime.ScanCodeShowActivity;
import com.example.parttime.adapter.MyPublishAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_acceptPeopleNumber;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.service.Url;
import com.example.parttime.square.SquareDetailedActivity;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.rxjava.rxlife.RxLife;

import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//我发布的任务页面
public class MyPublishActivity extends AppCompatActivity {
    MyPublishActivity thisActivity;
    TextView MP_textView_title;
    TextView MP_textView_money;
    TextView MP_textView_location;
    TextView MP_textView_timeData;
    RecyclerView MP_recyclerView;
    FloatingActionButton MP_floatingActionButton;
    View MP_view;

    Entity_taskData.ResultBean.DsBean squareDetailed;
    private String taskId;//任务ID
    private String stringPeopleId;//登录人ID

    private MyPublishAdapter myPublishAdapter;
    //加载窗
    private BasePopupView xPopupLoading;

    private ConstraintLayout constraintLayout13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_publish);
        initActivity();

        //获取传值任务的ID
        taskId = getIntent().getStringExtra("taskId");
        if (taskId != null) {
            getTaskData();
        } else {
            Toasty.warning(getApplicationContext(), "任务加载出错", Toasty.LENGTH_SHORT).show();
            finish();
        }
    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //出现二维码
    public void click_showCode(View view){
        startActivity(new Intent(this, ScanCodeShowActivity.class).putExtra("taskId",taskId));
    }

    //加载任务信息
    private void getTaskData() {

        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        //String uploadData = "M_task_list.remark2='1' and M_task_list_id ='"+taskId+"'";
        String uploadData = "M_task_list.id ='" + taskId + "'";

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", uploadData)//添加请求参数，该方法可调用多次
                .add("sindex","0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        squareDetailed = entity_taskData.getResult().getDs().get(0);
                        //界面值渲染
                        MP_textView_title.setText(squareDetailed.getM_task_list_remark5());
                        MP_textView_money.setText(squareDetailed.getMoney() + "元/天");
                        MP_textView_location.setText("工作地点:" + squareDetailed.getAddress4());
                        MP_textView_timeData.setText("工作时间:" + squareDetailed.getWorktimeStart() + "-" + squareDetailed.getWorktimeEnd());
                        MP_view.setVisibility(View.GONE);
                        getTaskPeopleData();

                        constraintLayout13.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //去任务详情页
                                Intent intent = new Intent(getApplicationContext(), SquareDetailedActivity.class);
                                intent.putExtra("squareDetailed", squareDetailed);
                                intent.putExtra("from","1");
                                startActivity(intent);
                            }
                        });

                    } else if (jsonStates.equals("0")) {
                        xPopupLoading.dismiss();
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Log.v("entity_taskData", "无数据");
                        } else {
                            Toasty.warning(getApplicationContext(), "未获取到任务信息", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), "任务加载错误", Toasty.LENGTH_SHORT).show();
                });


    }

    //获取员工信息
    private void getTaskPeopleData() {

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getAcceptPeople)//发送Get请求（获取使用中的任务）
                .add("taskid", taskId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {

                        Entity_acceptPeopleNumber entity_acceptPeopleNumber = gson.fromJson(jsonResult, Entity_acceptPeopleNumber.class);
                        myPublishAdapter.setList(entity_acceptPeopleNumber.getResult().getDs());
                        MP_recyclerView.setAdapter(myPublishAdapter);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Log.v("entity_taskData", "无数据");
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), "人员信息加载错误", Toasty.LENGTH_SHORT).show();
                });

    }

    //初始化页面工作
    private void initActivity() {
        thisActivity = this;
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MP_textView_title = this.findViewById(R.id.MP_textView_title);
        MP_textView_money = this.findViewById(R.id.MP_textView_money);
        MP_textView_location = this.findViewById(R.id.MP_textView_location);
        MP_textView_timeData = this.findViewById(R.id.MP_textView_timeData);
        MP_recyclerView = this.findViewById(R.id.MP_recyclerView);
        MP_floatingActionButton = this.findViewById(R.id.MP_floatingActionButton);
        MP_view = this.findViewById(R.id.MP_view);
        MP_recyclerView.setLayoutManager(new LinearLayoutManager(this));

        constraintLayout13 = this.findViewById(R.id.constraintLayout13);

        myPublishAdapter = new MyPublishAdapter(R.layout.item_mypublish);
        //监听单击事件
        //MP_floatingActionButton.setOnClickListener(onClickListenerShowCode);

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //ImmersionBar.destroy(this);
    }

}