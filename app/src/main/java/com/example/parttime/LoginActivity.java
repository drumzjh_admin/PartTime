package com.example.parttime;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;

import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lihang.smartloadview.SmartLoadingView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.rxjava.rxlife.RxLife;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import rxhttp.wrapper.param.RxHttp;

//验证码登录
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.editText_phone)
    EditText editTextPhone;
    @BindView(R.id.editText_code)
    EditText editTextCode;
    @BindView(R.id.text_bottom)
    TextView textBottom;
    @BindView(R.id.imageButton_wechat)
    ImageButton imageButtonWechat;
    @BindView(R.id.button_login)
    SmartLoadingView buttonLogin;
    @BindView(R.id.button_getcode)
    Button buttonGetCode;

    //存储
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editorUP;

    //加载窗
    private BasePopupView xPopupLoading;

    //EventHandler eventHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initActivity();
        //验证码输入框监听事件
        editTextCode.addTextChangedListener(editTextCodeTextWatcher);
    }

    //打开免责条款
    public void click_statement(View view){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(Url.url_statement)));
    }

    //登录
    public void click_login(View view) {
        if (editTextCode.getText() == null || editTextCode.getText().length() != 4) {
            Toasty.warning(this,"请输入正确的验证码",Toasty.LENGTH_SHORT).show();
            return;
        }
        //触发验证码提交操作
        SMSSDK.submitVerificationCode("86", editTextPhone.getText().toString(), editTextCode.getText().toString());
    }
    //获取验证码
    public void click_getcode(View view) {

        if (editTextPhone.getText() == null || editTextPhone.getText().length() != 11) {
            Toasty.warning(this,"请输入正确的手机号码",Toasty.LENGTH_SHORT).show();
            return;
        }

        //发送验证码
        final int count = 60;
        getCodeTimer(count);
        //触发验证码发送操作
        SMSSDK.getVerificationCode("86", editTextPhone.getText().toString());

    }

    //进行密码登录
    public void click_passwordlogin(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginPwdActivity.class);
        intent.putExtra("phoneNumber", editTextPhone.getText().toString());
        startActivity(intent);
        finish();
    }

    //进行微信登录
    public void click_wechatlogin(View view) {

        Platform platform = ShareSDK.getPlatform(Wechat.NAME);
        if (!platform.isClientValid()) {//判断是否已安装微信客户端
            Toasty.warning(LoginActivity.this, "请先安装微信客户端", Toasty.LENGTH_SHORT).show();
        } else {
            if (platform.isAuthValid()) {//判断是否已经授权
                Log.v("微信登录", "已授权"+platform.getDb().exportData());//用户公共信息
                //移除授权状态和本地缓存，下次授权会重新授权
                //platform.removeAccount(true);
                //调用获取人员信息接口
                getUserInfo(platform.getDb().getUserId());
            }else {
                platform.showUser(null);//要数据不要功能，主要体现在不会重复出现授权界面（authorize()要功能不要数据）
                platform.SSOSetting(false);//设置为客户端授权
                platform.setPlatformActionListener(platformActionListener);
            }
        }

    }

    //授权回调监听
    PlatformActionListener platformActionListener = new PlatformActionListener() {
        @Override
        public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
            Log.v("微信登录", "登录成功"+platform.getDb().exportData());//用户公共信息
            //调用获取人员信息接口
            getUserInfo(platform.getDb().getUserId());
        }

        @Override
        public void onError(Platform platform, int i, Throwable throwable) {
            Toasty.warning(LoginActivity.this, "微信授权失败，请重试", Toasty.LENGTH_SHORT).show();
            Log.v("微信登录", "onError ---->  登录失败" + throwable.toString());
            Log.v("微信登录", "onError ---->  登录失败" + throwable.getStackTrace().toString());
            Log.v("微信登录", "onError ---->  登录失败" + throwable.getMessage());
        }

        @Override
        public void onCancel(Platform platform, int i) {
            Log.v("微信登录", "用户取消登录");
        }
    };

    //跳过
    public void click_jump(View view) {
        editorUP.putString("loginState", "jump");
        editorUP.commit();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        finish();
    }

    //监听验证码输入ing
    TextWatcher editTextCodeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            //验证码长度可点击
            if (s.toString().trim().length() == 4 && editTextPhone.getText().length() == 11) {
                buttonLogin.setSmartClickable(true);
            } else {
                buttonLogin.setSmartClickable(false);
            }
        }
    };

    //注册验证码事件接收器
    private void registerEventHandlerCodeSMS() {
        SMSSDK.registerEventHandler(new EventHandler() {
            @Override
            public void afterEvent(int event, int result, Object data) {
                Message msg = new Message();
                msg.arg1 = event;
                msg.arg2 = result;
                msg.obj = data;
                new Handler(
                        Looper.getMainLooper(), new Handler.Callback() {
                    @Override
                    public boolean handleMessage(@androidx.annotation.NonNull Message msg) {
                        if (msg.arg1 == SMSSDK.EVENT_GET_VERIFICATION_CODE) {//获取验证码
                            if (msg.arg2 == SMSSDK.RESULT_COMPLETE) {//处理验证成功

                            } else {//处理错误结果
                                Toasty.error(LoginActivity.this,"获取验证码错误",Toasty.LENGTH_SHORT).show();
                            }
                        } else if (msg.arg1 == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {//提交验证码
                            if (msg.arg2 == SMSSDK.RESULT_COMPLETE) {//处理验证成功
                                login();
                            } else {//处理错误结果
                                Toasty.error(LoginActivity.this,"验证码错误",Toasty.LENGTH_SHORT).show();
                            }
                        }
                        return false;
                    }
                }
                ).sendMessage(msg);
            }
        });
    }

    //登录
    private void login() {
        buttonLogin.start();

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getLoginPhoneData)//发送Get请求
                .add("phone", editTextPhone.getText().toString().trim())//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_phoneData entity_phoneData = gson.fromJson(jsonResult, Entity_phoneData.class);
                        //数据存储加页面跳转
                        editorUP.putString(StorageConfig.EDITOR_LOGINSTATE, "1");
                        editorUP.putString(StorageConfig.EDITOR_LOGINID, entity_phoneData.getResult().getDs().get(0).getId());
                        editorUP.putString(StorageConfig.EDITOR_LOGINDATA, jsonResult);
                        editorUP.commit();
                        buttonLogin.onSuccess(new SmartLoadingView.AnimationOKListener() {
                            @Override
                            public void animationOKFinish() {
                                //跳转至主页面
                                Toasty.warning(LoginActivity.this,"登录成功",Toasty.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                finish();
                            }
                        });

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("帐号不存在。") || entity_httpResult.getErrorMsg().equals("无数据")) {
                            Toasty.error(this,"当前用户未注册",Toasty.LENGTH_SHORT).show();
                        } else if (entity_httpResult.getErrorMsg().equals("密码错误。")) {
                            Toasty.error(this,"密码错误",Toasty.LENGTH_SHORT).show();
                        }else {
                            Toasty.warning(this,"获取用户信息失败",Toasty.LENGTH_SHORT).show();
                        }
                        buttonLogin.backToStart();
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(this,"登录失败请重试",Toasty.LENGTH_SHORT).show();
                    buttonLogin.backToStart();
                });

    }

    //使用微信ID获取人员信息
    private void getUserInfo(String openId) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("登录中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getWechatPhoneData)//发送Get请求
                .add("Remark9", openId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_phoneData entity_phoneData = gson.fromJson(jsonResult, Entity_phoneData.class);
                        //数据存储加页面跳转
                        editorUP.putString(StorageConfig.EDITOR_LOGINSTATE, "1");
                        editorUP.putString(StorageConfig.EDITOR_LOGINID, entity_phoneData.getResult().getDs().get(0).getId());
                        editorUP.putString(StorageConfig.EDITOR_LOGINDATA, jsonResult);
                        editorUP.commit();
                        //跳转至主页面
                        Toasty.warning(LoginActivity.this, "登录成功", Toasty.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        finish();

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("无数据")) {

                            //前往注册界面完善手机号
                            startActivity(new Intent(getApplicationContext(), RegisteredActivity.class).putExtra("wechatId",openId));

                        }else {
                            Toasty.warning(this, "微信授权失败，请重试", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "微信授权失败，请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //验证码倒计时(Rxjava)
    private void getCodeTimer(int count) {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                .take(count + 1)
                .observeOn(AndroidSchedulers.mainThread())//主线程操作UI
                .map(aLong -> count - aLong)//lamata表达式
                .doOnSubscribe(disposable -> buttonGetCode.setEnabled(false))//lamata表达式
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        buttonGetCode.setText(aLong + "s后重新获取");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        //还原button状态
                        buttonGetCode.setText("重新获取");
                        buttonGetCode.setEnabled(true);
                    }
                });
    }

    //初始化页面工作
    private void initActivity() {
        //状态栏
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.colorWhite).init();

        //存储
        sharedPreferences = getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        editorUP = sharedPreferences.edit();
        //权限
        HelpTool.permissionGroupData(this);
        //注册验证码事件接收器
        registerEventHandlerCodeSMS();

        //获取屏幕信息
//        DisplayMetrics metric = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metric);
//
//        int width = metric.widthPixels; // 宽度（PX）
//        int height = metric.heightPixels; // 高度（PX）
//
//        float density = metric.density; // 密度（0.75 / 1.0 / 1.5）
//        int densityDpi = metric.densityDpi; // 密度DPI（120 / 160 / 240）
//
//        //屏幕宽度算法:屏幕宽度（像素）/屏幕密度
//        int screenWidth = (int) (width / density);//屏幕宽度(dp)
//        int screenHeight = (int) (height / density);//屏幕高度(dp)
//
//        Toast.makeText(LoginActivity.this, "宽度:" + width + " 高度:" + height + " 密度:" + density + " 密度DPI:" + densityDpi
//                + "\r\n屏幕dp宽度：" + screenWidth + " 屏幕dp高度：" + screenHeight, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //用完回调注销否则可能出现内存泄漏
        SMSSDK.unregisterAllEventHandler();
    }
}