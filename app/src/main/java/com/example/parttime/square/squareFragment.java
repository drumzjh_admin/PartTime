package com.example.parttime.square;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.example.parttime.HomeActivity;
import com.example.parttime.R;
import com.example.parttime.ScanCodeActivity;
import com.example.parttime.adapter.TaskAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_taskType;
import com.example.parttime.service.Url;
import com.example.parttime.tool.FiltrateBean;
import com.example.parttime.tool.FlowPopWindow;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.SoftKeyBoard;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.rxjava.rxlife.RxLife;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

/**
 * A simple {@link Fragment} subclass.
 */
//广场页
public class squareFragment extends Fragment {

    RefreshLayout FSQ_smartRefreshLayout;
    RecyclerView recyclerView_task;
    TextView textView_city;
    TextView textView_search;
    TextView textView_workType;
    TextView textView_workRequirements;
    ImageButton imageButton_citySwitch;
    ImageButton imageButton_scan;
    ImageButton imageButton_workType;
    ImageButton imageButton_workRequirements;
    EditText editText_search;
    LinearLayout linearLayout_top;
    LinearLayout linearLayout_workType;
    LinearLayout linearLayout_workRequirements;
    LottieAnimationView FSQ_lottie_loading;
    ConstraintLayout FSQ_constraintLayout_nodata;

    //任务列表适配器
    private TaskAdapter taskAdapter;
    //任务信息存储
    private List<Entity_taskData.ResultBean.DsBean> taskDataList;
    //下拉筛选条件
    private String getParameter = "M_task_list.remark2='1'";//总筛选条件
    private String getParameterLX = "";//类型筛选条件
    private String getParameterYQ = "";//要求筛选条件
    private List<FiltrateBean> filtrateBeanListLX;//筛选类型数组
    private List<FiltrateBean> filtrateBeanListYQ;//筛选要求数组
    private FlowPopWindow flowPopWindow;
    private String[] taskTypeArray;//类别
    private String[] taskTypeIdArray;//类别ID

    public AMapLocationClient mLocationClient = null;//声明AMapLocationClient类对象
    public AMapLocationClientOption mLocationOption = null;//声明AMapLocationClientOption对象

    public squareFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View squareView = inflater.inflate(R.layout.fragment_square, container, false);
//        OkHttpClient okHttpClient = new OkHttpClient();
//        final Request request  = new Request.Builder().url("http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/GetListtask_list?strwhere=M_task_list.remark2='1'").get().build();
//        Call call = okHttpClient.newCall(request);
//        call.enqueue(new Callback() {
//            @Override
//            public void onFailure(@NotNull Call call, @NotNull IOException e) {
//
//            }
//
//            @Override
//            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
//
//                Gson gson = new Gson();
//                Entity_taskData entity_taskData = gson.fromJson(HelpTool.ParseHttpResult(response.body().string()), Entity_taskData.class);;
//                if (entity_taskData.getStates().equals("1")) {
//
//
//                } else if (entity_taskData.getStates().equals("0")) {
//                    Toast.makeText(getContext(), "任务列表加载失败1", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });

        return squareView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initActivity();
    }

    //获取定位数据
    private void getLocation() {
        //设置为单次定位
        mLocationOption.setOnceLocation(true);
        //设置为高精度模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        //设置定位回调监听
        mLocationClient.setLocationListener(mAMapLocationListener);
        //启动定位
        mLocationClient.startLocation();
    }

    //异步获取定位结果
    AMapLocationListener mAMapLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation amapLocation) {

            if (amapLocation != null) {
                if (amapLocation.getErrorCode() == 0) {

                    //存储当前定位数据
                    HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOCATIONJD, String.valueOf(amapLocation.getLatitude()), getActivity());
                    HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOCATIONWD, String.valueOf(amapLocation.getLongitude()), getActivity());
                    HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOCATIONMZ, amapLocation.getPoiName(), getActivity());

                    Log.v("经度", String.valueOf(amapLocation.getLatitude()));
                    Log.v("纬度", String.valueOf(amapLocation.getLongitude()));
                    Log.v("位置", amapLocation.getAddress());

                    taskAdapter = new TaskAdapter(R.layout.item_square, taskDataList, String.valueOf(amapLocation.getLatitude()), String.valueOf(amapLocation.getLongitude()));

                } else {
                    //定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                    Log.v("AmapError", "location Error, ErrCode:"
                            + amapLocation.getErrorCode() + ", errInfo:"
                            + amapLocation.getErrorInfo());
                    taskAdapter = new TaskAdapter(R.layout.item_square, taskDataList, null, null);
                }
            } else {
                taskAdapter = new TaskAdapter(R.layout.item_square, taskDataList, null, null);
            }

            taskAdapter.setAnimationEnable(true);
            recyclerView_task.setAdapter(taskAdapter);
            taskAdapter.setOnItemClickListener(onItemClickListener);//监听单击事件

            //获取广场数据
            updateTaskData("M_task_list.remark2='1'", "任务列表加载失败");

        }
    };

    //搜索
    public void click_searchTask() {

        HelpTool.showhideKeyboard(getActivity());//软键盘关闭控制
        String searchContent = "";
        if(editText_search!=null&&editText_search.getText()!=null) {
            searchContent = editText_search.getText().toString().trim();
        }
        if (!searchContent.equals("")) {

            updateTaskData("pt_ConstantDetail.display_name like '%" + searchContent + "%'", "未搜索到任务");

        } else {//展示全部任务
            updateTaskData("M_task_list.remark2='1'", "任务列表加载失败");
        }

    }

    //扫描
    public void click_scan() {
        startActivity(new Intent(getActivity(), ScanCodeActivity.class).putExtra("squareScanButton", "1"));
    }

    //城市
    public void click_selectCity() {

    }

    //类型
    public void click_selectType() {
        if (taskTypeArray == null || taskTypeArray.length == 0) {
            updateTaskTypeData();
        } else {

            textView_workType.setTextColor(getResources().getColor(R.color.colorAppYellow));
            imageButton_workType.setBackgroundResource(R.drawable.shanglabalck);

            //展示筛选数据
            flowPopWindow = new FlowPopWindow(getActivity(), filtrateBeanListLX);
            flowPopWindow.showAsDropDown(linearLayout_top);
            flowPopWindow.setOnConfirmClickListener(onConfirmClickListenerXialaLX);//筛选确定事件
            flowPopWindow.setOnDismissListener(onDismissListenerXiala);//筛选关闭事件
        }
    }

    //要求
    public void click_selectRequirements() {

        //改变界面样式
        textView_workRequirements.setTextColor(getResources().getColor(R.color.colorAppYellow));
        imageButton_workRequirements.setBackgroundResource(R.drawable.shanglabalck);

        //展示筛选数据
        flowPopWindow = new FlowPopWindow(getActivity(), filtrateBeanListYQ);
        flowPopWindow.showAsDropDown(linearLayout_top);
        flowPopWindow.setOnConfirmClickListener(onConfirmClickListenerXialaYQ);//筛选确定事件
        flowPopWindow.setOnDismissListener(onDismissListenerXiala);//筛选关闭事件

    }

    //获取广场数据
    private void updateTaskData(String addData, String msg) {

        showView(2);

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", addData)//添加请求参数，该方法可调用多次
                .add("sindex", "0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    FSQ_smartRefreshLayout.finishRefresh(1000);

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        //显示任务至页面
                        taskDataList = new ArrayList<>();
                        taskDataList = entity_taskData.getResult().getDs();

                        if (taskDataList.size() != 0) {
                            showView(1);
                            taskAdapter.setList(taskDataList);
                        } else {
                            showView(0);
                            Toasty.warning(getContext(), "暂无任务信息", Toasty.LENGTH_SHORT).show();
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Toasty.warning(getContext(), "未搜索到任务", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(getContext(), "数据加载失败", Toasty.LENGTH_SHORT).show();
                        }
                        showView(0);
                    }

                }, throwable -> {
                    //请求失败
                    FSQ_smartRefreshLayout.finishRefresh(false);
                    Toasty.warning(getContext(), msg, Toasty.LENGTH_SHORT).show();
                    showView(0);
                });

    }

    //获取广场数据(上拉)
    private void updateTaskDataUp(String addData) {
        RxHttp.setDebug(true);
        if(taskDataList==null){
            taskDataList = new ArrayList<Entity_taskData.ResultBean.DsBean>();
        }
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", addData)//添加请求参数，该方法可调用多次
                .add("sindex", taskDataList.size())
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    FSQ_smartRefreshLayout.finishLoadMore(1000);

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        //显示任务至页面
                        if (taskDataList.size() != 0) {
                            taskAdapter.addData(entity_taskData.getResult().getDs());
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (!entity_taskData.getErrorMsg().equals("无数据")) {
                            Toasty.warning(getContext(), "数据加载失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    //请求失败
                    FSQ_smartRefreshLayout.finishLoadMore(false);
                    Toasty.warning(getContext(), "加载数据失败", Toasty.LENGTH_SHORT).show();
                });

    }

    //获取筛选分类数据
    private void updateTaskTypeData() {

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskType)//发送Get请求（获取使用中的任务）
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskType entity_taskType = gson.fromJson(jsonResult, Entity_taskType.class);
                        //循环设置类型数组值
                        taskTypeArray = new String[entity_taskType.getResult().getDs().size()];
                        taskTypeIdArray = new String[entity_taskType.getResult().getDs().size()];
                        for (int i = 0; i < entity_taskType.getResult().getDs().size(); i++) {
                            taskTypeArray[i] = entity_taskType.getResult().getDs().get(i).getDisplay_name();
                            taskTypeIdArray[i] = entity_taskType.getResult().getDs().get(i).getId();
                        }

                        //保存分类至缓存中
                        HelpTool.setSharedPreferenceArray(StorageConfig.EDITOR_TYPE, taskTypeArray, getActivity());
                        HelpTool.setSharedPreferenceArray(StorageConfig.EDITOR_TYPEID, taskTypeIdArray, getActivity());

                        //设置分类筛选数据
                        filtrateBeanListLX = new ArrayList<>();
                        filtrateBeanListLX = HelpTool.initFlowPopDataType(filtrateBeanListLX, taskTypeArray);

                    } else if (jsonStates.equals("0")) {
                        Log.v("获取筛选分类数据", "未获取到数据");
                    }

                }, throwable -> {
                    //请求失败
                    Log.v("获取筛选分类数据", "失败");
                });

    }

    //根据状态展示不同页面
    private void showView(int i) {
        if (i == 0) {//无数据
            FSQ_lottie_loading.setVisibility(View.GONE);
            recyclerView_task.setVisibility(View.GONE);
            FSQ_constraintLayout_nodata.setVisibility(View.VISIBLE);
        } else if (i == 1) {//出数据
            FSQ_lottie_loading.setVisibility(View.GONE);
            recyclerView_task.setVisibility(View.VISIBLE);
            FSQ_constraintLayout_nodata.setVisibility(View.GONE);
        } else if (i == 2) {//加载中
            FSQ_lottie_loading.setVisibility(View.VISIBLE);
            recyclerView_task.setVisibility(View.GONE);
            FSQ_constraintLayout_nodata.setVisibility(View.GONE);
        }
    }

    //单击
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.textView_search) {//搜索
                click_searchTask();
            } else if (v.getId() == R.id.imageButton_scan) {//扫描
                click_scan();
            } else if (v.getId() == R.id.imageButton_citySwitch) {//城市
                click_selectCity();
            } else if (v.getId() == R.id.textView_city) {//城市
                click_selectCity();
            } else if (v.getId() == R.id.linearLayout_workType) {//类型
                click_selectType();
            } else if (v.getId() == R.id.linearLayout_workRequirements) {//要求
                click_selectRequirements();
            }

        }
    };

    //下拉刷新任务
    OnRefreshListener onRefreshListener = new OnRefreshListener() {
        @Override
        public void onRefresh(@NonNull RefreshLayout refreshLayout) {
            updateTaskData("M_task_list.remark2='1'", "任务列表加载失败");
        }
    };

    //上拉加载任务
    OnLoadMoreListener onLoadMoreListener = new OnLoadMoreListener() {
        @Override
        public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
            updateTaskDataUp("M_task_list.remark2='1'");
        }
    };

    //下拉筛选确定LX（flowPopWindow的监听）
    FlowPopWindow.OnConfirmClickListener onConfirmClickListenerXialaLX = new FlowPopWindow.OnConfirmClickListener() {
        @Override
        public void onConfirmClick() {

            getParameterLX = "";
            String workType = null;

            for (FiltrateBean filtrateBean : filtrateBeanListLX) {
                List<FiltrateBean.Children> childrenList = filtrateBean.getChildren();
                for (int i = 0; i < childrenList.size(); i++) {
                    if (childrenList.get(i).isSelected()) {
                        getParameterLX = "and M_task_list.worktype=" + taskTypeIdArray[i];
//                        if (filtrateBean.getTypeName().equals("全部")) {
//                            workType = childrenList.get(i).getValue();
//                            getParameterLX = "and M_task_list.worktype=" + taskTypeIdArray[i];
//                        }
                    }
                }
            }

            updateTaskData(getParameter + getParameterLX + getParameterYQ, "");

        }
    };

    //下拉筛选确定YQ（flowPopWindow的监听）
    FlowPopWindow.OnConfirmClickListener onConfirmClickListenerXialaYQ = new FlowPopWindow.OnConfirmClickListener() {
        @Override
        public void onConfirmClick() {

            getParameterYQ = "";
            String age = null;
            String sex = null;
            String schooling = null;

            for (FiltrateBean filtrateBean : filtrateBeanListYQ) {
                List<FiltrateBean.Children> childrenList = filtrateBean.getChildren();
                for (int i = 0; i < childrenList.size(); i++) {
                    if (childrenList.get(i).isSelected()) {
                        if (filtrateBean.getTypeName().equals("年龄")) {
                            age = childrenList.get(i).getValue();
                            getParameterYQ += " and M_task_list.old_year='" + age + "'";
                        } else if (filtrateBean.getTypeName().equals("性别")) {
                            switch (childrenList.get(i).getValue()) {
                                case "男":
                                    sex = "1";
                                    break;
                                case "女":
                                    sex = "2";
                                    break;
                                case "不限":
                                    sex = "3";
                                    break;
                            }
                            getParameterYQ += " and M_task_list.sex='" + sex + "'";
                        } else if (filtrateBean.getTypeName().equals("学历")) {
                            schooling = childrenList.get(i).getValue();
                            getParameterYQ += " and M_task_list.Education='" + schooling + "'";
                        }

                    }
                }
            }

            updateTaskData(getParameter + getParameterLX + getParameterYQ, "");

        }
    };

    //下拉筛选关闭（flowPopWindow的监听）
    PopupWindow.OnDismissListener onDismissListenerXiala = new PopupWindow.OnDismissListener() {
        @Override
        public void onDismiss() {
            textView_workType.setTextColor(getResources().getColor(R.color.textGray));
            imageButton_workType.setBackgroundResource(R.drawable.xialablack);
            textView_workRequirements.setTextColor(getResources().getColor(R.color.textGray));
            imageButton_workRequirements.setBackgroundResource(R.drawable.xialablack);
        }
    };

    //展示搜索按钮（监听软键盘事件）
    private void softKeyBoardListener() {
        SoftKeyBoard softKeyBoard = new SoftKeyBoard(getActivity());
        //软键盘状态监听
        softKeyBoard.setListener(new SoftKeyBoard.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {//软键盘显示
                imageButton_scan.setVisibility(View.INVISIBLE);
                textView_search.setVisibility(View.VISIBLE);
            }

            @Override
            public void keyBoardHide(int height) {//软键盘隐藏
                editText_search.clearFocus();
                textView_search.setVisibility(View.INVISIBLE);
                imageButton_scan.setVisibility(View.VISIBLE);
            }
        });
    }

    //打开任务详情(adapter的单击监听)
    OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

            Intent intent = new Intent(getContext(), SquareDetailedActivity.class);
            intent.putExtra("squareDetailed", taskDataList.get(position));
            startActivity(intent);

        }
    };

    //初始化页面工作
    private void initActivity() {

        //页面控件绑定
        FSQ_smartRefreshLayout = getActivity().findViewById(R.id.FSQ_smartRefreshLayout);
        recyclerView_task = getActivity().findViewById(R.id.FSQ_recyclerView_task);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView_task.setLayoutManager(linearLayoutManager);

        textView_city = getActivity().findViewById(R.id.textView_city);
        textView_search = getActivity().findViewById(R.id.textView_search);
        textView_workType = getActivity().findViewById(R.id.textView_workType);
        textView_workRequirements = getActivity().findViewById(R.id.textView_workRequirements);
        imageButton_citySwitch = getActivity().findViewById(R.id.imageButton_citySwitch);
        imageButton_scan = getActivity().findViewById(R.id.imageButton_scan);
        imageButton_workType = getActivity().findViewById(R.id.imageButton_workType);
        imageButton_workRequirements = getActivity().findViewById(R.id.imageButton_workRequirements);
        editText_search = getActivity().findViewById(R.id.editText_search);
        linearLayout_top = getActivity().findViewById(R.id.linearLayout_top);
        linearLayout_workType = getActivity().findViewById(R.id.linearLayout_workType);
        linearLayout_workRequirements = getActivity().findViewById(R.id.linearLayout_workRequirements);
        FSQ_lottie_loading = getActivity().findViewById(R.id.FSQ_lottie_loading);
        FSQ_constraintLayout_nodata = getActivity().findViewById(R.id.FSQ_constraintLayout_nodata);

        //初始化定位
        mLocationClient = new AMapLocationClient(getActivity());
        mLocationOption = new AMapLocationClientOption();
        getLocation();

        //监听单击事件

        textView_search.setOnClickListener(onClickListener);
        imageButton_scan.setOnClickListener(onClickListener);
        textView_city.setOnClickListener(onClickListener);
        imageButton_citySwitch.setOnClickListener(onClickListener);
        linearLayout_workType.setOnClickListener(onClickListener);
        linearLayout_workRequirements.setOnClickListener(onClickListener);

        //设置下拉刷新上拉加载
        FSQ_smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getActivity()));
        FSQ_smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getActivity()));
        FSQ_smartRefreshLayout.setOnRefreshListener(onRefreshListener);//下拉
        FSQ_smartRefreshLayout.setOnLoadMoreListener(onLoadMoreListener);//上拉


        //设置要求筛选数据
        filtrateBeanListYQ = new ArrayList<>();
        filtrateBeanListYQ = HelpTool.initFlowPopDataRequirements(filtrateBeanListYQ);

        //监听软键盘事件
        softKeyBoardListener();

        //获取筛选分类数据
        updateTaskTypeData();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mLocationClient != null) {
            mLocationClient.stopLocation();//停止定位后，本地定位服务并不会被销毁
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocationClient != null) {
            mLocationClient.onDestroy();//销毁定位客户端，同时销毁本地定位服务。
        }
    }

}
