package com.example.parttime.square;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.bumptech.glide.Glide;
import com.example.parttime.LoginActivity;
import com.example.parttime.R;
import com.example.parttime.add.MapActivity;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.massage.ImCommunicationActivity;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpIM;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.rxjava.rxlife.RxLife;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.jzvd.JzvdStd;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.rong.imkit.RongIM;
import io.rong.imlib.model.Conversation;
import rxhttp.wrapper.param.RxHttp;

//任务详情页面
public class SquareDetailedActivity extends AppCompatActivity {

    @BindView(R.id.SD_videoView_top)
    JzvdStd SD_videoView_top;
    @BindView(R.id.SD_textView_title)
    TextView SD_textView_title;
    @BindView(R.id.SD_textView_money)
    TextView SD_textView_money;
    @BindView(R.id.SD_textView_characteristics0)
    TextView SD_textView_characteristics0;
    @BindView(R.id.SD_textView_characteristics1)
    TextView SD_textView_characteristics1;
    @BindView(R.id.SD_textView_characteristics2)
    TextView SD_textView_characteristics2;
    @BindView(R.id.SD_textView_characteristics3)
    TextView SD_textView_characteristics3;
    @BindView(R.id.SD_textView_timeData)
    TextView SD_textView_timeData;
    @BindView(R.id.SD_textView_peopleNumber)
    TextView SD_textView_peopleNumber;
    @BindView(R.id.SD_textView_tip)
    TextView SD_textView_tip;
    @BindView(R.id.SD_textView_location)
    TextView SD_textView_location;
    @BindView(R.id.SD_imageView_peopleHead)
    CircleImageView SD_imageView_peopleHead;
    @BindView(R.id.SD_textView_peopleName)
    TextView SD_textView_peopleName;
    @BindView(R.id.SD_textView_wageState)
    TextView SD_textView_wageState;
    @BindView(R.id.SD_imageButton_fenxiang)
    ImageView SD_imageButton_fenxiang;
    @BindView(R.id.SD_imageButton_shoucang)
    ImageView SD_imageButton_shoucang;
    @BindView(R.id.SD_constraintLayout_map)
    ConstraintLayout SD_constraintLayout_map;
    @BindView(R.id.SD_map)
    MapView SD_map;

    @BindView(R.id.SD_button_communication)
    Button SD_button_communication;
//    @BindView(R.id.SD_imageView_companyHead)
//    ImageView SD_imageView_companyHead;
//    @BindView(R.id.SD_textView_companyName)
//    TextView SD_textView_companyName;

    public AMap aMap;//初始化地图控制器对象

    private UploadCollection uploadCollection;
    Entity_taskData.ResultBean.DsBean squareDetailed;
    private String stringPeopleId;
    private boolean isCollection = false;//是否被收藏

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square_detailed);
        ButterKnife.bind(this);
        initActivity();
        //接收squarefragment的传参
        squareDetailed = (Entity_taskData.ResultBean.DsBean) getIntent().getSerializableExtra("squareDetailed");

        String fromExtra = getIntent().getStringExtra("from");
        if(fromExtra!=null && fromExtra.equals("1")){
            SD_button_communication.setVisibility(View.INVISIBLE);
        }

        if (squareDetailed != null) {

            //加载当前任务是否被收藏
            checkCollection();

            //上传类赋值
            uploadCollection = new UploadCollection();
            uploadCollection.setEmpid(stringPeopleId);
            uploadCollection.setTask_id(squareDetailed.getM_task_list_id());

            //****************************************************************赋值
            String sex = "不限";
            String name = "***";
            String tip = "无";
            String companyName = "***";
            if (squareDetailed.getSex().equals("1")) {
                sex = "男";
            } else if (squareDetailed.getSex().equals("2")) {
                sex = "女";
            }
            if (!squareDetailed.getM_employee_list_remark1().equals("") && squareDetailed.getM_employee_list_remark1() != null) {
                name = squareDetailed.getM_employee_list_remark1();
            }
            if (!squareDetailed.getM_task_list_remark().equals("") && squareDetailed.getM_task_list_remark() != null) {
                tip = squareDetailed.getM_task_list_remark();
            }

            SD_textView_title.setText(squareDetailed.getM_task_list_remark5());
            SD_textView_money.setText(squareDetailed.getMoney() + "元/天");
            SD_textView_timeData.setText(squareDetailed.getWorktimeStart() + "-" + squareDetailed.getWorktimeEnd());
            SD_textView_location.setText(squareDetailed.getAddress1() + "-" + squareDetailed.getAddress2() + "-" + squareDetailed.getAddress3() + "-" + squareDetailed.getAddress4());
            SD_textView_characteristics0.setText("性别" + sex);
            SD_textView_characteristics1.setText("年龄" + squareDetailed.getOld_year());
            SD_textView_characteristics2.setText("学历" + squareDetailed.getEducation());
            SD_textView_characteristics3.setText("籍贯不限");
            SD_textView_peopleName.setText(name);
            SD_textView_peopleNumber.setText(squareDetailed.getPersonalCount());
            SD_textView_tip.setText(tip);
//            SD_textView_companyName.setText(companyName);

            //设置发布头像
            if (squareDetailed.getM_employee_list_remark4().equals("") || squareDetailed.getM_employee_list_remark4() == null) {
                SD_imageView_peopleHead.setImageResource(R.drawable.zhanweihead);
            } else {
                Glide.with(this).load(Url.url_img + squareDetailed.getM_employee_list_remark4()).into(SD_imageView_peopleHead);
            }

            //设置播放视频
            if (!squareDetailed.getAduioUrl().equals("") && squareDetailed.getAduioUrl() != null) {
                SD_videoView_top.setUp(Url.url_img + squareDetailed.getAduioUrl(), "");
                if (!squareDetailed.getM_task_list_remark7().equals("") && squareDetailed.getM_task_list_remark7() != null) {
                    SD_videoView_top.posterImageView.setImageURI(Uri.parse(Url.url_img + squareDetailed.getM_task_list_remark7()));
                    Glide.with(getApplicationContext()).load(Url.url_img + squareDetailed.getM_task_list_remark7()).into(SD_videoView_top.posterImageView);
                } else {//显示暂无封面
                    Glide.with(getApplicationContext()).load(R.drawable.wufengmian).into(SD_videoView_top.posterImageView);
                }

            } else {//显示暂无视频
                Glide.with(getApplicationContext()).load(R.drawable.wushiping).into(SD_videoView_top.posterImageView);
            }

            //设置地图显示
            SD_map.onCreate(savedInstanceState);//此方法须覆写，虚拟机需要在很多情况下保存地图绘制的当前状态。
            if (aMap == null) {
                aMap = SD_map.getMap();//设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false
            }
            //设置了一个可视范围的初始化位置
            LatLng latLng = new LatLng(Double.parseDouble(squareDetailed.getLatitude()), Double.parseDouble(squareDetailed.getDiameter()));//初始化一个经度纬度坐标点
            CameraPosition cameraPosition = new CameraPosition(latLng, 16, 0, 0);
            aMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            aMap.addMarker(new MarkerOptions().position(latLng).title(squareDetailed.getAddress4()));//设置标记点

        } else {//加载任务信息失败
            Toasty.warning(getApplicationContext(), "加载任务信息出错", Toasty.LENGTH_SHORT).show();
            finish();
        }

    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //分享
    public void click_share(View view) {
        //startActivity(new Intent(this, MapActivity.class));
    }

    //收藏
    public void click_collection(View view) {
        if (isCollection == true) {
            new XPopup.Builder(SquareDetailedActivity.this).asConfirm("提示", "是否取消收藏", new OnConfirmListener() {
                @Override
                public void onConfirm() {
                    cancelCollection();
                }
            }).show();
        } else {
            addCollection();
        }
    }

    //打开地图页
    public void click_openMap(View view) {
        startActivity(new Intent(this, MapActivity.class).putExtra("latitude", squareDetailed.getLatitude()).putExtra("longitude", squareDetailed.getDiameter()).putExtra("titleName", squareDetailed.getAddress4()));
    }

    //沟通
    public void click_communication(View view) {

        //检测是否当前任务为自己发起
        String stringTaskPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

        if (stringTaskPeopleId.equals("") || stringTaskPeopleId == null) {
            Toasty.warning(getApplicationContext(), "登录后才可发起聊天", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (squareDetailed.getEmployee_list_id().equals(stringTaskPeopleId)) {
            Toasty.warning(getApplicationContext(), "当前任务为自己发起", Toasty.LENGTH_SHORT).show();
            return;
        }
        //设置聊天对象的信息
        //HelpIM.RIM_setUser(squareDetailed.getM_employee_list_remark1(),Url.url_img + squareDetailed.getM_employee_list_remark4());

        HelpTool.setSharedPreferenceString("taskId", squareDetailed.getM_task_list_id(), this);
        //添加会话对象
        Conversation.ConversationType conversationType = Conversation.ConversationType.PRIVATE;
        String targetId = squareDetailed.getEmployee_list_id();
        String title = squareDetailed.getM_task_list_remark5();
        RongIM.getInstance().setMessageAttachedUserInfo(true);//设置消息体内携带用户信息，接收人直接显示发送人信息
        RongIM.getInstance().startConversation(this, conversationType, targetId, title, null);

    }

    //打开发布人页
    public void click_openPeople(View view) {

    }

    //打开公司页
    public void click_openCompany(View view) {

    }

    //增加收藏
    private void addCollection() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("添加中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        String uploadData = new Gson().toJson(uploadCollection);
        RxHttp.setDebug(true);
        RxHttp.get(Url.url_addCollection)//发送Get请求（获取使用中的任务）
                .add("strjSon", uploadData)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        isCollection = true;
                        SD_imageButton_shoucang.setBackgroundResource(R.drawable.shoucang);
                        Toasty.warning(this, "收藏成功", Toasty.LENGTH_SHORT).show();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "收藏出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //取消收藏
    private void cancelCollection() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("取消中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        String uploadData = new Gson().toJson(uploadCollection);
        RxHttp.setDebug(true);
        RxHttp.get(Url.url_cancelCollection)//发送Get请求（获取使用中的任务）
                .add("strjSon", uploadData)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        isCollection = false;
                        SD_imageButton_shoucang.setBackgroundResource(R.drawable.icon_collect);
                        Toasty.warning(this, "取消成功", Toasty.LENGTH_SHORT).show();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "取消出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //查看收藏
    private void checkCollection() {
        //xPopupLoading = new XPopup.Builder(this).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_checkCollection)//发送Get请求（获取使用中的任务）
                .add("task_id", squareDetailed.getM_task_list_id())//添加请求参数，该方法可调用多次
                .add("empid", stringPeopleId)
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //xPopupLoading.dismiss();

                    //请求成功
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];

                    if (jsonStates.equals("1")) {
                        isCollection = true;
                        SD_imageButton_shoucang.setBackgroundResource(R.drawable.shoucang);
                    } else if (jsonStates.equals("0")) {
                        isCollection = false;
                        SD_imageButton_shoucang.setBackgroundResource(R.drawable.icon_collect);
                    }

                }, throwable -> {
                    //请求失败
                    //xPopupLoading.dismiss();
                });

    }

    //初始化页面工作
    private void initActivity() {
        //状态栏
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

    }


    @Override
    protected void onPause() {
        super.onPause();
        SD_map.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SD_map.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SD_map.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
        SD_map.onSaveInstanceState(outState);
    }


    class UploadCollection {

        /**
         * task_id : 1
         * empid : 1
         */

        private String task_id;
        private String empid;

        public String getTask_id() {
            return task_id;
        }

        public void setTask_id(String task_id) {
            this.task_id = task_id;
        }

        public String getEmpid() {
            return empid;
        }

        public void setEmpid(String empid) {
            this.empid = empid;
        }
    }

}