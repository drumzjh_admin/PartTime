package com.example.parttime;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lihang.smartloadview.SmartLoadingView;
import com.mob.MobSDK;
import com.rxjava.rxlife.RxLife;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import es.dmoral.toasty.Toasty;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import rxhttp.wrapper.param.RxHttp;


//修改用户密码
public class ForgetPwdActivity extends AppCompatActivity {

    @BindView(R.id.editText_phone)
    EditText editTextPhone;
    @BindView(R.id.editText_pwd)
    EditText editTextPwd;
    @BindView(R.id.editText_code)
    EditText editTextCode;
    @BindView(R.id.button_getcode)
    Button buttonGetCode;
    @BindView(R.id.linearLayout_center)
    LinearLayout linearLayoutCenter;
    @BindView(R.id.button_resetPwd)
    SmartLoadingView buttonResetPwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);
        ButterKnife.bind(this);
        initActivity();
        Intent intent = getIntent();
        if (intent.getStringExtra("phoneNumber") != null) {
            editTextPhone.setText(intent.getStringExtra("phoneNumber"));
        }
        //验证码输入框监听事件
        editTextCode.addTextChangedListener(editTextCodeTextWatcher);
    }

    //返回上一页
    public void click_return(View view) {
        finish();
    }

    //重置
    public void click_resetPwd(View view) {
        if (editTextPhone.getText() == null || editTextPhone.getText().length() != 11) {
            Toast.makeText(this, "请输入正确的手机号", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editTextCode.getText() == null || editTextCode.getText().length() != 4) {
            Toast.makeText(this, "请输入正确的验证码", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editTextPwd.getText() == null || editTextPwd.getText().length() < 6) {
            Toast.makeText(this, "请检查密码输入格式", Toast.LENGTH_SHORT).show();
            return;
        }
        //触发验证码提交操作
        SMSSDK.submitVerificationCode("86", editTextPhone.getText().toString(), editTextCode.getText().toString());
    }

    //获取验证码
    public void click_getcode(View view) {

        if (editTextPhone.getText() == null || editTextPhone.getText().length() != 11) {

            Toast.makeText(this, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
            return;
        }

        //发送验证码
        final int count = 60;
        getCodeTimer(count);
        //触发验证码发送操作
        SMSSDK.getVerificationCode("86", editTextPhone.getText().toString());

    }

    //监听验证码输入ing
    TextWatcher editTextCodeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            //验证码长度可点击
            if (s.toString().trim().length() == 4 && editTextPhone.getText().length() == 11 && editTextPwd.getText().length() >= 6) {
                buttonResetPwd.setSmartClickable(true);
            } else {
                buttonResetPwd.setSmartClickable(false);
            }
        }
    };

    //注册验证码事件接收器
    private void registerEventHandlerCodeSMS() {
        SMSSDK.registerEventHandler(new EventHandler() {
            @Override
            public void afterEvent(int event, int result, Object data) {
                Message msg = new Message();
                msg.arg1 = event;
                msg.arg2 = result;
                msg.obj = data;
                new Handler(
                        Looper.getMainLooper(), new Handler.Callback() {
                    @Override
                    public boolean handleMessage(@androidx.annotation.NonNull Message msg) {
                        if (msg.arg1 == SMSSDK.EVENT_GET_VERIFICATION_CODE) {//获取验证码
                            if (msg.arg2 == SMSSDK.RESULT_COMPLETE) {//处理验证成功

                            } else {//处理错误结果
                                Toast.makeText(ForgetPwdActivity.this, "获取验证码错误", Toast.LENGTH_SHORT).show();
                            }
                        } else if (msg.arg1 == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {//提交验证码
                            if (msg.arg2 == SMSSDK.RESULT_COMPLETE) {//处理验证成功
                                reset();
                            } else {//处理错误结果
                                Toast.makeText(ForgetPwdActivity.this, "验证码错误", Toast.LENGTH_SHORT).show();
                            }
                        }
                        return false;
                    }
                }
                ).sendMessage(msg);
            }
        });
    }

    //重置
    private void reset() {
        buttonResetPwd.start();

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_updatePwd)//发送Get请求
                .add("phone", editTextPhone.getText().toString().trim())//添加请求参数，该方法可调用多次
                .add("pwd", editTextPwd.getText().toString().trim())
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];

                    if (jsonStates.equals("1")) {
                        //加页面跳转
                        buttonResetPwd.onSuccess(new SmartLoadingView.AnimationOKListener() {
                            @Override
                            public void animationOKFinish() {
                                //跳转至登录页面
                                Toasty.warning(ForgetPwdActivity.this,"重置成功",Toasty.LENGTH_SHORT).show();
                                finish();
                            }
                        });

                    } else if (jsonStates.equals("0")) {
                        Toasty.warning(this,"重置失败",Toasty.LENGTH_SHORT).show();
                        buttonResetPwd.backToStart();
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(this,"重置失败请重试",Toasty.LENGTH_SHORT).show();
                    buttonResetPwd.backToStart();
                });

    }

    //验证码倒计时(Rxjava)
    private void getCodeTimer(int count) {
        Observable.interval(0, 1, TimeUnit.SECONDS)
                .take(count + 1)
                .observeOn(AndroidSchedulers.mainThread())//主线程操作UI
                .map(aLong -> count - aLong)//lamata表达式
                .doOnSubscribe(disposable -> buttonGetCode.setEnabled(false))//lamata表达式
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        buttonGetCode.setText(aLong + "s后重新获取");
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        //还原button状态
                        buttonGetCode.setText("重新获取");
                        buttonGetCode.setEnabled(true);
                    }
                });
    }

    //初始化页面工作
    private void initActivity() {
        //状态栏
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.colorWhite).init();
        MobSDK.submitPolicyGrantResult(true, null);
        //注册验证码事件接收器
        registerEventHandlerCodeSMS();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //用完回调注销否则可能出现内存泄漏
        SMSSDK.unregisterAllEventHandler();
    }

}
