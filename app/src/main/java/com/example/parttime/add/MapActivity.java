package com.example.parttime.add;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.InputtipsQuery;
import com.amap.api.services.help.Tip;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.example.parttime.R;
import com.example.parttime.adapter.MapAdapter;
import com.example.parttime.adapter.MyCollectionAdapter;
import com.example.parttime.entity.Upload_taskData;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.LocationData;
import com.example.parttime.tool.StorageConfig;
import com.gyf.immersionbar.ImmersionBar;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

//地图页
public class MapActivity extends AppCompatActivity {

    MapView mapView;//定义了一个地图view
    ConstraintLayout MAP_constraintLayout_selectLocation;
    ConstraintLayout MAP_constraintLayout2;
    ConstraintLayout MAP_constraintLayout_mapTopGaode;
    EditText MAP_editText_search;
    TextView MAP_textView_location;
    TextView MAP_textView_locationGaode;
    TextView MAP_textView_title;
    RecyclerView MAP_recyclerView;


    public AMap aMap;//初始化地图控制器对象
    public AMapLocationClient mLocationClient = null;//声明AMapLocationClient类对象
    public AMapLocationClientOption mLocationOption = null;//声明AMapLocationClientOption对象

    private MapAdapter mapAdapter;//定位适配器
    private LocationData locationData;//定位位置信息
    private List<Tip> tipList;//存储查询数据返回数据

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initActivity();

        //设置地图显示
        mapView.onCreate(savedInstanceState);// 此方法须覆写，虚拟机需要在很多情况下保存地图绘制的当前状态。
        if (aMap == null) {
            aMap = mapView.getMap();
        }

        //如果为任务详情页打开地图页则只显示位置和导航按钮
        if(getIntent().getStringExtra("latitude")!=null){
            MAP_constraintLayout_mapTopGaode.setVisibility(View.VISIBLE);
            MAP_textView_locationGaode.setText("导航至 : "+getIntent().getStringExtra("titleName"));
            //设置了一个可视范围的初始化位置
            LatLng latLng = new LatLng(Double.parseDouble(getIntent().getStringExtra("latitude")),Double.parseDouble(getIntent().getStringExtra("longitude")));//初始化一个经度纬度坐标点
            CameraPosition cameraPosition = new CameraPosition(latLng,16,0,0);
            aMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            aMap.addMarker(new MarkerOptions().position(latLng).title(getIntent().getStringExtra("titleName"))).showInfoWindow();//设置标记点
        }else {//发布页打开地图页可选位置
            MAP_constraintLayout_selectLocation.setVisibility(View.VISIBLE);
            aMap.setMyLocationEnabled(true);//设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false
            aMap.moveCamera(CameraUpdateFactory.zoomTo(16));//地图缩放大小
            //初始化定位
            mLocationClient = new AMapLocationClient(getApplicationContext());
            mLocationOption = new AMapLocationClientOption();
            getLocation();
        }
    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //选择定位位置
    public void click_selectLocation(View view) {

        if (locationData == null) {
            getLocation();
        } else {
            //传值至上一页面(发布页)
            setResult(RESULT_OK, new Intent().putExtra("mapCode", locationData));
            finish();
        }

    }

    //导航至某个位置
    public void click_openGaode(View view){

        //判断是否已安装
        if(HelpTool.isAvilible(this,"com.autonavi.minimap")){

            //打开高德地图规划路径
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setPackage("com.autonavi.minimap");
            try {
                intent = Intent.getIntentOld("amapuri://route/plan/?dlat="+getIntent().getStringExtra("latitude")+
                        "&dlon="+getIntent().getStringExtra("longitude")+"&dname="+getIntent().getStringExtra("titleName")+"&dev=0&t=0");
                startActivity(intent);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }else {
            Toasty.warning(this,"尚未安装高德地图",Toasty.LENGTH_SHORT).show();
        }

    }

    //搜索
    public void click_search(View view) {

        if (MAP_editText_search.getText().toString() == null || MAP_editText_search.getText().toString() == "") {
            Toasty.warning(this, "请输入搜索关键词", Toasty.LENGTH_SHORT).show();
        } else {
//            PoiSearch.Query poiQuery = new PoiSearch.Query(MAP_editText_search.getText().toString(),"","");//设置搜索条件
//            PoiSearch poiSearch = new PoiSearch(getApplicationContext(),poiQuery);
//            poiSearch.searchPOIAsyn();//设置异步搜索
//            poiSearch.setOnPoiSearchListener(poiSearchListener);//设置搜索回调监听

            InputtipsQuery inputtipsQuery = new InputtipsQuery(MAP_editText_search.getText().toString(), "上海");//设置搜索条件
            inputtipsQuery.setCityLimit(true);//限制搜索内容为当前城市
            Inputtips inputtips = new Inputtips(this, inputtipsQuery);
            inputtips.requestInputtipsAsyn();//设置异步搜索
            inputtips.setInputtipsListener(inputtipsListener);//设置搜索回调监听

        }

    }

    //搜索回调监听
    Inputtips.InputtipsListener inputtipsListener = new Inputtips.InputtipsListener() {
        @Override
        public void onGetInputtips(List<Tip> list, int i) {

            if (i == AMapException.CODE_AMAP_SUCCESS) {//如果搜索成功

                //循环去除空地点
                tipList = new ArrayList<>();
                for (Tip t : list) {
                    if (t.getPoiID() != null && t.getPoint() != null) {
                        tipList.add(t);
                    }
                }

                MAP_recyclerView.setVisibility(View.VISIBLE);
                mapAdapter.setList(tipList);

            }

        }
    };

    //获取定位数据
    private void getLocation() {
        //设置为单次定位
        mLocationOption.setOnceLocation(true);
        //设置为高精度模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        //设置定位回调监听
        mLocationClient.setLocationListener(mAMapLocationListener);
        //启动定位
        mLocationClient.startLocation();
    }

    //异步获取定位结果
    AMapLocationListener mAMapLocationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation amapLocation) {
            if (amapLocation != null) {
                if (amapLocation.getErrorCode() == 0) {

                    locationData = new LocationData();
                    //解析定位结果
                    locationData.setLatitude(String.valueOf(amapLocation.getLatitude()));
                    locationData.setLongitude(String.valueOf(amapLocation.getLongitude()));
                    locationData.setAddress(String.valueOf(amapLocation.getAddress()));
                    locationData.setCountry(String.valueOf(amapLocation.getCountry()));
                    locationData.setProvince(String.valueOf(amapLocation.getProvince()));
                    locationData.setCity(String.valueOf(amapLocation.getCity()));
                    locationData.setDistrict(String.valueOf(amapLocation.getDistrict()));
                    locationData.setStreet(String.valueOf(amapLocation.getStreet()));
                    locationData.setStreetNum(String.valueOf(amapLocation.getStreetNum()));
                    locationData.setPoiName(String.valueOf(amapLocation.getPoiName()));

                    Log.v("经度", locationData.getLatitude());
                    Log.v("纬度", locationData.getLongitude());
                    Log.v("位置", locationData.getAddress());

                    MAP_textView_location.setText(locationData.getPoiName());

                } else {
                    //定位失败时，可通过ErrCode（错误码）信息来确定失败的原因，errInfo是错误信息，详见错误码表。
                    Log.v("AmapError", "location Error, ErrCode:"
                            + amapLocation.getErrorCode() + ", errInfo:"
                            + amapLocation.getErrorInfo());
                    MAP_textView_location.setText("定位失败，点击重新定位");
                }
            }
        }
    };

    //单击
    OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

            Log.v("Tip", tipList.get(position).getAddress());
            Log.v("Tip", tipList.get(position).getAdcode());
            Log.v("Tip", tipList.get(position).getDistrict());
            Log.v("Tip", tipList.get(position).getName());
            Log.v("Tip", tipList.get(position).getPoiID());
            Log.v("Tip", tipList.get(position).getTypeCode());
            Log.v("Tip", String.valueOf(tipList.get(position).getPoint()));

            String LatitudeLongitude = String.valueOf(tipList.get(position).getPoint());
            String[] LatitudeLongitudes = LatitudeLongitude.split(",");

            String  Latitude =LatitudeLongitudes[0];
            String  Longitude =LatitudeLongitudes[1];
            String  Province ="";
            String  City ="";
            String  District ="";
            String  PoiName =tipList.get(position).getName();

            //选择搜索地点
            locationData = new LocationData();
            //解析定位结果
            locationData.setLatitude(Latitude);
            locationData.setLongitude(Longitude);
            locationData.setProvince(Province);
            locationData.setCity(City);
            locationData.setDistrict(District);
            locationData.setPoiName(PoiName);

            Log.v("经度", locationData.getLatitude());
            Log.v("纬度", locationData.getLongitude());
            Log.v("位置", locationData.getPoiName());

            //传值至上一页面(发布页)
            setResult(RESULT_OK, new Intent().putExtra("mapCode", locationData));
            finish();

        }
    };

    //初始化页面工作
    private void initActivity() {
        //状态栏
        ImmersionBar.with(this).statusBarDarkFont(true).statusBarColor(R.color.colorWhite).init();

        //注册页面控件
        mapView = (MapView) findViewById(R.id.MAP_map);
        MAP_constraintLayout_selectLocation = this.findViewById(R.id.MAP_constraintLayout_selectLocation);
        MAP_constraintLayout2 = this.findViewById(R.id.MAP_constraintLayout2);
        MAP_constraintLayout_mapTopGaode = this.findViewById(R.id.MAP_constraintLayout_mapTopGaode);
        MAP_editText_search = this.findViewById(R.id.MAP_editText_search);
        MAP_textView_location = this.findViewById(R.id.MAP_textView_location);
        MAP_textView_locationGaode = this.findViewById(R.id.MAP_textView_locationGaode);
        MAP_textView_title = this.findViewById(R.id.MAP_textView_title);
        MAP_recyclerView = this.findViewById(R.id.MAP_recyclerView);

        MAP_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mapAdapter = new MapAdapter(R.layout.item_map);
        MAP_recyclerView.setAdapter(mapAdapter);
        //监听处理事件
        mapAdapter.setOnItemClickListener(onItemClickListener);//单击

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
        if(mLocationClient!=null){
            mLocationClient.stopLocation();//停止定位后，本地定位服务并不会被销毁
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if(mLocationClient!=null){
            mLocationClient.onDestroy();//销毁定位客户端，同时销毁本地定位服务。
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 定位后的数据信息
     * amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
     * amapLocation.getLatitude();//获取纬度
     * amapLocation.getLongitude();//获取经度
     * amapLocation.getAccuracy();//获取精度信息
     * amapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
     * amapLocation.getCountry();//国家信息
     * amapLocation.getProvince();//省信息
     * amapLocation.getCity();//城市信息
     * amapLocation.getDistrict();//城区信息
     * amapLocation.getStreet();//街道信息
     * amapLocation.getStreetNum();//街道门牌号信息
     * amapLocation.getCityCode();//城市编码
     * amapLocation.getAdCode();//地区编码
     * amapLocation.getAoiName();//获取当前定位点的AOI信息
     * amapLocation.getBuildingId();//获取当前室内定位的建筑物Id
     * amapLocation.getFloor();//获取当前室内定位的楼层
     * amapLocation.getGpsAccuracyStatus();//获取GPS的当前状态
     * //获取定位时间
     * SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     * Date date = new Date(amapLocation.getTime());
     * df.format(date);
     * */

}