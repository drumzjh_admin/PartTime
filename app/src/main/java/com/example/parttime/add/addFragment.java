package com.example.parttime.add;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.example.parttime.LoginActivity;
import com.example.parttime.R;
import com.example.parttime.StartActivity;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.my.MyCertificationActivity;
import com.example.parttime.my.MySettingActivity;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.interfaces.OnConfirmListener;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
//任务发布页面
public class addFragment extends Fragment {

    LottieAnimationView lottieView_people;
    LottieAnimationView lottieimageView_company;

    //个人的信息
    private Entity_phoneData phoneData;

    public addFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initActivity();
    }

    //打开个人发布
    private void click_openPeople(){
        if(phoneData.getResult().getDs().get(0).getRemark8() != null && !phoneData.getResult().getDs().get(0).getCardURl().equals("")){//已认证
            if(phoneData.getResult().getDs().get(0).getCardURl().equals("1")){
                startActivity(new Intent(getActivity(),PeoplePublishActivity.class));
            }else {
                Toasty.warning(getActivity(), "请先前往个人中心进行个人认证", Toasty.LENGTH_SHORT).show();
            }
        }else {
            Toasty.warning(getActivity(), "请先前往个人中心进行个人认证", Toasty.LENGTH_SHORT).show();
        }
    }

    //打开公司发布
    private void click_openCompany(){
        if(phoneData.getResult().getDs().get(0).getRemark6() != null || !phoneData.getResult().getDs().get(0).getRemark6().equals("")){//已认证
            startActivity(new Intent(getActivity(),CompanyPublishActivity.class));
        }else {
            new XPopup.Builder(this.getContext()).asConfirm("提示", "公司认证成功后才可进行此操作，是否前往公司认证","取消","确认", new OnConfirmListener() {
                @Override
                public void onConfirm() {
//                    SharedPreferences sharedPreferences = getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editorUP = sharedPreferences.edit();
//                    editorUP.clear();
//                    editorUP.commit();

                    startActivity(new Intent(getActivity(), MyCertificationActivity.class).putExtra("phoneData",phoneData));
//                    finish();
                }
            },null,false).show();
//            Toasty.warning(getActivity(), "请先前往个人中心进行公司认证", Toasty.LENGTH_SHORT).show();
        }
    }

    //单击
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.FA_lottie_personal) {//搜索
                click_openPeople();
            } else if (v.getId() == R.id.FA_lottie_company) {//扫描
                click_openCompany();
            }
        }
    };

    //初始化页面工作
    private void initActivity() {
        lottieView_people = getActivity().findViewById(R.id.FA_lottie_personal);
        lottieimageView_company = getActivity().findViewById(R.id.FA_lottie_company);

        //监听单击事件
        lottieView_people.setOnClickListener(onClickListener);
        lottieimageView_company.setOnClickListener(onClickListener);

        //设置个人信息
        String peopleData = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINDATA, getActivity());
        phoneData = new Gson().fromJson(peopleData,Entity_phoneData.class);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ImmersionBar.destroy(this);
    }

}
