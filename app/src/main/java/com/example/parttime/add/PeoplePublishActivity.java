package com.example.parttime.add;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_moneyAndMortgage;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Upload_taskData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.LocationData;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.loper7.layout.StringUtils;
import com.loper7.layout.dialog.CardDatePickerDialog;
import com.lxj.matisse.CaptureMode;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.rxjava.rxlife.RxLife;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人发布页面
public class PeoplePublishActivity extends AppCompatActivity {
    EditText PP_editTextText_inputTaskName;
    EditText PP_editTextText_inputPeopleNumber;
    EditText PP_editTextText_inputMoney;
    EditText PP_editTextText_inputTaskTip;
    TextView PP_textView_selectType;
    TextView PP_textView_selectTime;
    TextView PP_textView_selectLocation;
    TextView PP_textView_selectLocation2;
    TextView PP_textView_selectSex;
    TextView PP_textView_selectAge;
    TextView PP_textView_selectSchooling;
    TextView PP_textView_selectVideo;
    TextView PP_textView_saveDraft;
    VideoView PP_videoView;
    Button PP_button_publish;

    private String[] taskTypeArray;//类别
    private String[] taskTypeIdArray;//类别ID

    private Entity_taskData.ResultBean.DsBean entity_taskData;
    private Upload_taskData upload_taskData;
    private UploadMoney uploadMoney;
    private String stringTaskPeopleId;
    private File imageFile;//保存选择视频
    private File videoCoverFile;//视频得第一帧
    private List<Uri> myselectUri;//所选视频的Uri
    private List<String> myselectPath;//所选视频的路径

    //加载窗
    private BasePopupView xPopupLoading;
    private String toastMsgTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_company_publish);
        setContentView(R.layout.activity_people_publish);
        initActivity();
        //接收MyDraftActivity的传参
        entity_taskData = (Entity_taskData.ResultBean.DsBean) getIntent().getSerializableExtra("entity_taskData");
        if (entity_taskData != null) {
            //直接界面赋值
            setDataTaskView();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            if (requestCode == 6 || requestCode == 9) {//获取选择到的视频

                if (requestCode == 6) {//获取选择到的视频
                    //在manifest中设置不以沙盒模式运行android:requestLegacyExternalStorage="true"
                    myselectUri = Matisse.obtainResult(data);
                    myselectPath = Matisse.obtainPathResult(data);

                    //判断视频大小
                    imageFile = new File(myselectPath.get(0));
                } else {
                    //获取拍摄的图片路径，如果是录制视频则是视频的第一帧图片路径
                    String captureImagePath = com.lxj.matisse.Matisse.obtainCaptureImageResult(data);

                    //获取拍摄的视频路径
                    String captureVideoPath = com.lxj.matisse.Matisse.obtainCaptureVideoResult(data);

                    //获取裁剪结果的路径，不管是选择照片裁剪还是拍摄照片裁剪，结果都从这里取
//                  String cropPath = com.lxj.matisse.Matisse.obtainCropResult(data);

                    //获取选择图片或者视频的结果路径，如果开启裁剪的话，获取的是原图的地址
//                  myselectUri = com.lxj.matisse.Matisse.obtainSelectUriResult(data);//uri形式的路径
//                  myselectPath = com.lxj.matisse.Matisse.obtainSelectPathResult(data);//文件形式路径
                    myselectPath = new ArrayList<>();
                    myselectPath.add(captureVideoPath);

                    //判断视频大小
                    imageFile = new File(myselectPath.get(0));
                }

                if (imageFile.length() > (100 * 1024 * 1024)) {
                    Toasty.warning(this, "视频文件不得大小100MB，请重新选择", Toasty.LENGTH_SHORT).show();
                } else {

                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    // 设置数据源，有多种重载，这里用本地文件的绝对路径
                    mmr.setDataSource(myselectPath.get(0));
                    Bitmap frameBitmap = mmr.getFrameAtTime();
                    mmr.release();
                    //String imgSavePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+System.currentTimeMillis() + ".png";
                    videoCoverFile = saveImgToFile(frameBitmap);

                    Log.v("选择视频地址", myselectPath.get(0));
                    PP_textView_selectVideo.setText("已选");
                    PP_videoView.setVisibility(View.VISIBLE);
                    setupVideo(myselectPath.get(0));
                }

            }else if (requestCode == 16){//获取地图返回值

                LocationData locationData = (LocationData) data.getSerializableExtra("mapCode");
                PP_textView_selectLocation2.setText(locationData.getPoiName());
                //上传数据赋值
                upload_taskData.setLatitude(locationData.getLatitude());
                upload_taskData.setDiameter(locationData.getLongitude());
//                upload_taskData.setAddress1(locationData.getProvince());
//                upload_taskData.setAddress2(locationData.getCity());
//                upload_taskData.setAddress3(locationData.getDistrict());
                upload_taskData.setAddress4(locationData.getPoiName());

            }

        }

    }

    //任务界面赋值
    private void setDataTaskView() {
        PP_textView_saveDraft.setVisibility(View.GONE);

        //循环设置类别名
        for (int i=0;i<taskTypeIdArray.length;i++){
            if(taskTypeIdArray[i].equals(entity_taskData.getWorktype())){
                PP_textView_selectType.setText(taskTypeArray[i]);
                break;
            }
        }
        String startTime = entity_taskData.getWorktimeStart().substring(5);
        String endTime = entity_taskData.getWorktimeEnd().substring(5);
        PP_textView_selectTime.setText( startTime+ " - " + endTime);
        PP_textView_selectLocation.setText("上海 - " + entity_taskData.getAddress3());
        PP_textView_selectLocation2.setText(entity_taskData.getAddress4());
        if (entity_taskData.getSex().equals("1")) {
            PP_textView_selectSex.setText("男");
        } else if (entity_taskData.getSex().equals("2")) {
            PP_textView_selectSex.setText("女");
        } else if (entity_taskData.getSex().equals("3")) {
            PP_textView_selectSex.setText("不限");
        }
        PP_textView_selectAge.setText(entity_taskData.getOld_year());
        PP_textView_selectSchooling.setText(entity_taskData.getEducation());

        PP_editTextText_inputTaskName.setText(entity_taskData.getM_task_list_remark5());
        PP_editTextText_inputPeopleNumber.setText(entity_taskData.getPersonalCount());
        String money = entity_taskData.getMoney().split("\\.")[0];
        PP_editTextText_inputMoney.setText(money);
        PP_editTextText_inputTaskTip.setText(entity_taskData.getM_task_list_remark());

        upload_taskData.setId(entity_taskData.getM_task_list_id());
        upload_taskData.setWorktype(entity_taskData.getWorktype());
        upload_taskData.setSex(entity_taskData.getSex());
        upload_taskData.setAddress1("上海");
        upload_taskData.setAddress2("上海");
        upload_taskData.setAddress3(entity_taskData.getAddress3());
        upload_taskData.setLatitude(entity_taskData.getLatitude());
        upload_taskData.setDiameter(entity_taskData.getDiameter());
        upload_taskData.setAddress4(entity_taskData.getAddress4());
        upload_taskData.setWorktimeStart(entity_taskData.getWorktimeStart());
        upload_taskData.setWorktimeEnd(entity_taskData.getWorktimeEnd());
        upload_taskData.setAddress3(entity_taskData.getAddress3());
        upload_taskData.setOld_year(entity_taskData.getOld_year());
        upload_taskData.setEducation(entity_taskData.getEducation());
    }

    //监控视频状态
    private void setupVideo(String videoPath) {
        PP_videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {//准备好
                mp.setVolume(0f, 0f);
                mp.start();
            }
        });
        PP_videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {//播放完
                stopPlaybackVideo();
            }
        });
        PP_videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {//播放错
                stopPlaybackVideo();
                return true;
            }
        });

        try {
            PP_videoView.setVideoPath(videoPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //视频播放停止
    private void stopPlaybackVideo() {
        try {
            PP_videoView.stopPlayback();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //选择工作类型
    public void click_selectType(View view) {

        new XPopup.Builder(this).asBottomList("请选择", taskTypeArray, new OnSelectListener() {
            @Override
            public void onSelect(int position, String text) {
                PP_textView_selectType.setText(text);
                upload_taskData.setWorktype(taskTypeIdArray[position]);
            }
        }).show();

    }

    //选择工作时间
    public void click_selectTime(View view) {

        new CardDatePickerDialog(this).setTitle("请选择开始时间").setBackGroundModel(2)
                .showBackNow(false).showFocusDateInfo(false).setThemeColor(Color.parseColor("#FFCC33"))
                .setMinTime(System.currentTimeMillis()).setOnChooseListener(onChooseListenerSelectTimeStart).show();

    }

    //请选择开始时间
    CardDatePickerDialog.OnChooseListener onChooseListenerSelectTimeStart = new CardDatePickerDialog.OnChooseListener() {
        @Override
        public void onChoose(long l) {

            String timeDate = StringUtils.conversionTime(l, "yyyy/MM/dd HH:mm");
            PP_textView_selectTime.setText(StringUtils.conversionTime(l, "MM/dd HH:mm") + " - ");
            upload_taskData.setWorktimeStart(timeDate);

            new CardDatePickerDialog(PeoplePublishActivity.this).setTitle("请选择结束时间").setBackGroundModel(2)
                    .showBackNow(false).showFocusDateInfo(false).setThemeColor(Color.parseColor("#FFCC33"))
                    .setMinTime(System.currentTimeMillis()).setOnChooseListener(onChooseListenerSelectTimeEnd).show();

        }
    };

    //请选择结束时间
    CardDatePickerDialog.OnChooseListener onChooseListenerSelectTimeEnd = new CardDatePickerDialog.OnChooseListener() {
        @Override
        public void onChoose(long l) {

            String timeDate = StringUtils.conversionTime(l, "yyyy/MM/dd HH:mm");
            PP_textView_selectTime.setText(PP_textView_selectTime.getText() + StringUtils.conversionTime(l, "MM/dd HH:mm"));
            upload_taskData.setWorktimeEnd(timeDate);

        }
    };

    //选择工作地点
    public void click_selectLocation(View view) {

        new XPopup.Builder(this).asBottomList("请选择", StorageConfig.locationArray, new OnSelectListener() {
            @Override
            public void onSelect(int position, String text) {
                PP_textView_selectLocation.setText("上海 - " + text);
                upload_taskData.setAddress1("上海");
                upload_taskData.setAddress2("上海");
                upload_taskData.setAddress3(text);
            }
        }).show();

    }

    //选择详细地点
    public void click_selectLocation2(View view) {
        startActivityForResult(new Intent(this,MapActivity.class),16);
    }

    //选择工作性别
    public void click_selectSex(View view) {

        new XPopup.Builder(this).asBottomList("请选择", StorageConfig.sexArray, new OnSelectListener() {
            @Override
            public void onSelect(int position, String text) {
                PP_textView_selectSex.setText(text);
                if (text.equals("男")) {
                    upload_taskData.setSex("1");
                } else if (text.equals("女")) {
                    upload_taskData.setSex("2");
                } else if (text.equals("不限")) {
                    upload_taskData.setSex("3");
                }
            }
        }).show();

    }

    //选择工作年龄
    public void click_selectAge(View view) {

        new XPopup.Builder(this).asBottomList("请选择", StorageConfig.ageArray, new OnSelectListener() {
            @Override
            public void onSelect(int position, String text) {
                PP_textView_selectAge.setText(text);
                upload_taskData.setOld_year(text);
            }
        }).show();

    }

    //选择工作学历
    public void click_selectSchooling(View view) {

        new XPopup.Builder(this).asBottomList("请选择", StorageConfig.schoolingArray, new OnSelectListener() {
            @Override
            public void onSelect(int position, String text) {
                PP_textView_selectSchooling.setText(text);
                upload_taskData.setEducation(text);
            }
        }).show();

    }

    //选择工作视频
    public void click_selectVideo(View view) {
        //判断存储权限
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    7);
        } else {
            /**
             * 打开选择图片的界面
             */
//            Intent intent = new Intent();
//            //相片类型image/*
//            intent.setType("video/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(intent, 1);

//            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
//            startActivityForResult(intent, 1);

            //调用系统图库
//            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");// 相片类型
//            startActivityForResult(intent, 1);
//
//            Matisse.from(this)
//                    .choose(MimeType.of(MimeType.JPEG, MimeType.PNG, MimeType.GIF))//照片视频全部显示MimeType.allOf()
//                    .countable(true)//true:选中后显示数字;false:选中后显示对号
//                    .maxSelectable(3)//最大选择数量为9
//                    //.addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))//图片显示表格的大小
//                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)//图像选择和预览活动所需的方向
//                    .thumbnailScale(0.85f)//缩放比例
//                    .theme(R.style.Matisse_Zhihu)//主题  暗色主题 R.style.Matisse_Dracula
//                    .imageEngine(new MyGlideEngine())//图片加载方式，Glide4需要自定义实现
//                    .capture(true) //是否提供拍照功能，兼容7.0系统需要下面的配置
//                    //参数1 true表示拍照存储在共有目录，false表示存储在私有目录；参数2与 AndroidManifest中authorities值相同，用于适配7.0系统 必须设置
//                    .captureStrategy(new CaptureStrategy(true, "com.sendtion.matisse.fileprovider"))//存储到哪里
//                    .forResult(REQUEST_CODE_CHOOSE);//请求码

            //弹出选择框
            String[] selectString = {"录制视频", "选择视频"};

            new XPopup.Builder(this).asCenterList("", selectString, new OnSelectListener() {
                @Override
                public void onSelect(int position, String text) {

                    if (position == 0) {

                        //拍摄视频代码
                        com.lxj.matisse.Matisse.from(PeoplePublishActivity.this)
                                //.jumpCapture()//直接跳拍摄，默认可以同时拍摄照片和视频
                                //.jumpCapture(CaptureMode.Image)//只拍照片
                                .jumpCapture(CaptureMode.Video)//只拍视频
                                .isCrop(false) //开启裁剪
                                .forResult(9);

                    } else {

                        //选择视频代码
                        Matisse.from(PeoplePublishActivity.this).choose(MimeType.ofVideo()).
                                showSingleMediaType(true)
                                .maxSelectable(1)
                                .thumbnailScale(0.8f)
                                .imageEngine(new GlideEngine())
                                .forResult(6);

                    }

                }
            }).show();

        }

    }

    //保存发布草稿
    public void click_saveDraft(View view) {

        toastMsgTitle = "保存草稿";
        publishSave("任务发布中","0");

    }

    //发布
    public void click_publish(View view) {

        //upload_taskData.setRemark2("1");
        toastMsgTitle = "任务发布";
        publishSave("任务发布中","0");

    }

    //发布保存任务
    private void publishSave(String toastMsgTitle,String remark2) {

        String stringTaskName = PP_editTextText_inputTaskName.getText().toString().trim();
        String stringTaskPeopleNumber = PP_editTextText_inputPeopleNumber.getText().toString().trim();
        String stringTaskMoney = PP_editTextText_inputMoney.getText().toString().trim();
        String stringTaskTip = PP_editTextText_inputTaskTip.getText().toString().trim();
        String stringTaskType = PP_textView_selectType.getText().toString();
        String stringTaskTime = PP_textView_selectTime.getText().toString();
        String stringTaskSelectLocation = PP_textView_selectLocation.getText().toString();
        String stringTaskSelectLocation2 = PP_textView_selectLocation2.getText().toString();
        String stringTaskSex = PP_textView_selectSex.getText().toString();
        String stringTaskAge = PP_textView_selectAge.getText().toString();
        String stringTaskSchooling = PP_textView_selectSchooling.getText().toString();
        String stringTaskVideo = PP_textView_selectVideo.getText().toString();

        upload_taskData.setEmployee_list_id(stringTaskPeopleId);
        upload_taskData.setRemark5(stringTaskName);
        upload_taskData.setPersonalCount(stringTaskPeopleNumber);
        upload_taskData.setMoney(stringTaskMoney);
        upload_taskData.setRemark2(remark2);
        upload_taskData.setRemark(stringTaskTip);
//        upload_taskData.setRemark9("1");
        upload_taskData.setRemark9("1");

        //测试代码
//        upload_taskData.setLatitude("123");
//        upload_taskData.setDiameter("123");
//                upload_taskData.setAddress1(locationData.getProvince());
//                upload_taskData.setAddress2(locationData.getCity());
//                upload_taskData.setAddress3(locationData.getDistrict());
//        upload_taskData.setAddress4("123");

        //校验输入选择
        if (checkPublishData(stringTaskName, stringTaskPeopleNumber, stringTaskMoney, stringTaskType,
                stringTaskTime, stringTaskSelectLocation, stringTaskSelectLocation2, stringTaskSex, stringTaskAge, stringTaskSchooling, stringTaskVideo)) {

            xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading(toastMsgTitle).bindLayout(R.layout.dialog_loading).show();//初始化加载窗

            //上传任务视频
            uploadTaskVideo(imageFile);

        }

    }

    //检查输入选择
    private Boolean checkPublishData(String stringTaskName, String stringTaskPeopleNumber, String stringTaskMoney,
                                     String stringTaskType, String stringTaskTime, String stringTaskSelectLocation,String stringTaskSelectLocation2, String stringTaskSex,
                                     String stringTaskAge, String stringTaskSchooling, String stringTaskVideo) {

        if (stringTaskName.equals("")) {
            Toasty.warning(this, "请输入任务名称", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskPeopleNumber.equals("")) {
            Toasty.warning(this, "请输入招聘人数", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskMoney.equals("")) {
            Toasty.warning(this, "请输入薪资", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskType.equals("请选择")) {
            Toasty.warning(this, "请选择工作类型", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskTime.equals("请选择")) {
            Toasty.warning(this, "请选择工作时间", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (upload_taskData.getWorktimeEnd() == null) {
            Toasty.warning(this, "请选择工作结束时间", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskSelectLocation.equals("请选择")) {
            Toasty.warning(this, "请选择工作地点", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskSelectLocation2.equals("")) {
            Toasty.warning(this, "请选择详细地址", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskSex.equals("请选择")) {
            Toasty.warning(this, "请选择性别", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskAge.equals("请选择")) {
            Toasty.warning(this, "请选择年龄", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskSchooling.equals("请选择")) {
            Toasty.warning(this, "请选择学历", Toasty.LENGTH_SHORT).show();
            return false;
        }
        if (stringTaskVideo.equals("请上传视频")) {
            Toasty.warning(this, "请选择视频", Toasty.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    //上传任务视频
    private void uploadTaskVideo(File file) {

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addTaskVideo)
                .addFile("index", file)
                .upload(progress -> {
                    //上传进度回调,0-100，仅在进度有更新时才会回调
                    int currentProgress = progress.getProgress(); //当前进度 0-100
                    long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                    long totalSize = progress.getTotalSize();     //要上传的总字节大小
                }, AndroidSchedulers.mainThread())
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        //上传成功
                        upload_taskData.setAduioUrl(entity_httpResult.getResult());
                        uploadTaskVideoImg();
                    } else if (jsonStates.equals("0")) {
                        xPopupLoading.dismiss();
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.error(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //上传失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "发布失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //上传任务视频封面
    private void uploadTaskVideoImg() {

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addTaskVideoImg)
                .addFile("index", videoCoverFile)
                .upload(progress -> {
                    //上传进度回调,0-100，仅在进度有更新时才会回调
                    int currentProgress = progress.getProgress(); //当前进度 0-100
                    long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                    long totalSize = progress.getTotalSize();     //要上传的总字节大小
                }, AndroidSchedulers.mainThread())
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        //增加广场数据
                        upload_taskData.setRemark7(entity_httpResult.getResult());
                        //如果是从草稿箱跳转则直接进行押金支付
                        if(upload_taskData.getId()==null||upload_taskData.getId().equals("")){
                            String urlData = new Gson().toJson(upload_taskData);
                            uploadAddTask(urlData);
                        }else {
                            moneyTransfer(upload_taskData.getId());
                        }

                    } else if (jsonStates.equals("0")) {
                        xPopupLoading.dismiss();
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.error(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //上传失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "发布失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //保存图片为文件
    private File saveImgToFile(Bitmap bmp) {
        // 首先保存图片
        File appDir = new File(Environment.getExternalStorageDirectory(), "PartTime");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;

    }

    //增加广场数据
    private void uploadAddTask(String urlData) {
        //Log.v("上传JSON", urlData);

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_addTaskList)//发送Get请求
                .add("strjSon", urlData)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);

                    if (jsonStates.equals("1")) {

                        if (toastMsgTitle.equals("任务发布") && upload_taskData.getRemark2().equals("0")) {
                            //校验并转移押金金额
                            moneyTransfer(entity_httpResult.getResult());
                        } else {
                            Toasty.warning(this, toastMsgTitle + "成功", Toasty.LENGTH_SHORT).show();
                            this.finish();
                        }


                    } else if (jsonStates.equals("0")) {
                        Toasty.error(this, toastMsgTitle + "失败请重试", Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, toastMsgTitle + "失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }


    //校验并转移押金金额
    private void moneyTransfer(String taskId) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("缴纳押金中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        //获取登陆人余额信息
        String jsonResultMoney = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINMONEY, getApplicationContext());
        Entity_moneyAndMortgage entity_moneyAndMortgage = new Gson().fromJson(jsonResultMoney, Entity_moneyAndMortgage.class);

        int peopleNumber = Integer.parseInt(PP_editTextText_inputPeopleNumber.getText().toString());
        int inputMoney = Integer.parseInt(PP_editTextText_inputMoney.getText().toString());
        int money = peopleNumber * inputMoney;

        uploadMoney.setId(stringTaskPeopleId);
        uploadMoney.setM_task_list_id(taskId);
        uploadMoney.setMoneyY(String.valueOf(money));
        uploadMoney.setLockbalance(entity_moneyAndMortgage.getResult().getDs().get(0).getLockbalance());
        uploadMoney.setLockmargin(entity_moneyAndMortgage.getResult().getDs().get(0).getLockmargin());

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_moneyTransfer)//发送Get请求
                .add("strjSon", new Gson().toJson(uploadMoney))//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {

                        toastMsgTitle = "任务发布";
                        upload_taskData.setRemark2("1");
                        String urlData = new Gson().toJson(upload_taskData);
                        uploadAddTask(urlData);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.error(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_LONG).show();
                    }

                }, throwable -> {
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, toastMsgTitle + "失败请重试", Toasty.LENGTH_LONG).show();
                });
    }

    //监听人数和薪资输入ing
    TextWatcher editTextPeopleNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (PP_editTextText_inputPeopleNumber.getText().toString().length() > 0 && PP_editTextText_inputMoney.getText().toString().length() > 0) {
                int peopleNumber = Integer.parseInt(PP_editTextText_inputPeopleNumber.getText().toString());
                int inputMoney = Integer.parseInt(PP_editTextText_inputMoney.getText().toString());
                int money = peopleNumber * inputMoney;
                PP_button_publish.setText("发布（需支付押金" + money + "元）");
            } else {
                PP_button_publish.setText("发布");
            }

        }
    };

    //初始化页面工作
    private void initActivity() {

        //状态栏
        ImmersionBar.with(this).statusBarDarkFont(true).statusBarColor(R.color.colorWhite).init();
        //注册页面控件
        PP_editTextText_inputTaskName = this.findViewById(R.id.PP_editTextText_inputTaskName);
        PP_editTextText_inputPeopleNumber = this.findViewById(R.id.PP_editTextText_inputPeopleNumber);
        PP_editTextText_inputMoney = this.findViewById(R.id.PP_editTextText_inputMoney);
        PP_editTextText_inputTaskTip = this.findViewById(R.id.PP_editTextText_inputTaskTip);
        PP_textView_selectType = this.findViewById(R.id.PP_textView_selectType);
        PP_textView_selectTime = this.findViewById(R.id.PP_textView_selectTime);
        PP_textView_selectLocation = this.findViewById(R.id.PP_textView_selectLocation);
        PP_textView_selectLocation2 = this.findViewById(R.id.PP_textView_selectLocation2);
        PP_textView_selectSex = this.findViewById(R.id.PP_textView_selectSex);
        PP_textView_selectAge = this.findViewById(R.id.PP_textView_selectAge);
        PP_textView_selectSchooling = this.findViewById(R.id.PP_textView_selectSchooling);
        PP_textView_selectVideo = this.findViewById(R.id.PP_textView_selectVideo);
        PP_textView_saveDraft = this.findViewById(R.id.PP_textView_saveDraft);
        PP_videoView = this.findViewById(R.id.PP_videoView);
        PP_button_publish = this.findViewById(R.id.PP_button_publish);

        //添加控件事件
        PP_editTextText_inputPeopleNumber.addTextChangedListener(editTextPeopleNumberTextWatcher);
        PP_editTextText_inputMoney.addTextChangedListener(editTextPeopleNumberTextWatcher);

        //获取任务类型
        taskTypeArray = HelpTool.getSharedPreferenceArray(StorageConfig.EDITOR_TYPE, this);
        taskTypeIdArray = HelpTool.getSharedPreferenceArray(StorageConfig.EDITOR_TYPEID, this);

        //获取登陆人ID
        stringTaskPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, this);

        //初始化上传数据
        upload_taskData = new Upload_taskData();
        uploadMoney = new UploadMoney();

    }

    @Override
    protected void onResume() {
        super.onResume();
        stopPlaybackVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPlaybackVideo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlaybackVideo();
    }

    class UploadMoney{

        String id;//登陆人ID
        String MoneyY;//转账得金额
        String lockbalance;
        String lockmargin;
        String M_task_list_id;//任务ID

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMoneyY() {
            return MoneyY;
        }

        public void setMoneyY(String moneyY) {
            MoneyY = moneyY;
        }

        public String getLockbalance() {
            return lockbalance;
        }

        public void setLockbalance(String lockbalance) {
            this.lockbalance = lockbalance;
        }

        public String getLockmargin() {
            return lockmargin;
        }

        public void setLockmargin(String lockmargin) {
            this.lockmargin = lockmargin;
        }

        public String getM_task_list_id() {
            return M_task_list_id;
        }

        public void setM_task_list_id(String m_task_list_id) {
            M_task_list_id = m_task_list_id;
        }
    }

}