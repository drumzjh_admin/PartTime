package com.example.parttime.entity;

import java.util.List;

//押金明细实体类
public class Entity_moneyDetail {

    /**
     * states : 1
     * errorMsg : 成功。
     * result : {"ds":[{"Money":"2.00","addtime":"2020-4-26 17:42:02","employee_list_id":"3","remark1":"0"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean {
            /**
             * Money : 2.00
             * addtime : 2020-4-26 17:42:02
             * employee_list_id : 3
             * remark1 : 0
             */

            private String Money;
            private String addtime;
            private String employee_list_id;
            private String remark1;

            public String getMoney() {
                return Money;
            }

            public void setMoney(String Money) {
                this.Money = Money;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getEmployee_list_id() {
                return employee_list_id;
            }

            public void setEmployee_list_id(String employee_list_id) {
                this.employee_list_id = employee_list_id;
            }

            public String getRemark1() {
                return remark1;
            }

            public void setRemark1(String remark1) {
                this.remark1 = remark1;
            }
        }
    }
}
