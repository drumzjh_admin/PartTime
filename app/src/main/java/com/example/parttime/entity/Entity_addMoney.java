package com.example.parttime.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;
//充值实体类
public class Entity_addMoney {

    /**
     * states : 1
     * errorMsg :
     * result : {"WeiXin":{"appid":"wx8748cf377b6a0bf2","partnerid":"1601139984","prepayid":"wx051038282160283f331b58236b06f50000","package":"Sign=WXPay","noncestr":"0Bfp6WKy17SbCU4r","timestamp":"1599273559","sign":"12110B7038843454F2694DFF21147027","return_msg":"OK","return_code":"SUCCESS","orderNo":"5568704934819578703200905103918"},"ds":[{"balance":"0.00","margin":"0.00","lockbalance":"","lockmargin":"","id":"13"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * WeiXin : {"appid":"wx8748cf377b6a0bf2","partnerid":"1601139984","prepayid":"wx051038282160283f331b58236b06f50000","package":"Sign=WXPay","noncestr":"0Bfp6WKy17SbCU4r","timestamp":"1599273559","sign":"12110B7038843454F2694DFF21147027","return_msg":"OK","return_code":"SUCCESS","orderNo":"5568704934819578703200905103918"}
         * ds : [{"balance":"0.00","margin":"0.00","lockbalance":"","lockmargin":"","id":"13"}]
         */

        private WeiXinBean WeiXin;
        private List<DsBean> ds;

        public WeiXinBean getWeiXin() {
            return WeiXin;
        }

        public void setWeiXin(WeiXinBean WeiXin) {
            this.WeiXin = WeiXin;
        }

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class WeiXinBean {
            /**
             * appid : wx8748cf377b6a0bf2
             * partnerid : 1601139984
             * prepayid : wx051038282160283f331b58236b06f50000
             * package : Sign=WXPay
             * noncestr : 0Bfp6WKy17SbCU4r
             * timestamp : 1599273559
             * sign : 12110B7038843454F2694DFF21147027
             * return_msg : OK
             * return_code : SUCCESS
             * orderNo : 5568704934819578703200905103918
             */

            private String appid;
            private String partnerid;
            private String prepayid;
            @SerializedName("package")
            private String packageX;
            private String noncestr;
            private String timestamp;
            private String sign;
            private String return_msg;
            private String return_code;
            private String orderNo;

            public String getAppid() {
                return appid;
            }

            public void setAppid(String appid) {
                this.appid = appid;
            }

            public String getPartnerid() {
                return partnerid;
            }

            public void setPartnerid(String partnerid) {
                this.partnerid = partnerid;
            }

            public String getPrepayid() {
                return prepayid;
            }

            public void setPrepayid(String prepayid) {
                this.prepayid = prepayid;
            }

            public String getPackageX() {
                return packageX;
            }

            public void setPackageX(String packageX) {
                this.packageX = packageX;
            }

            public String getNoncestr() {
                return noncestr;
            }

            public void setNoncestr(String noncestr) {
                this.noncestr = noncestr;
            }

            public String getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(String timestamp) {
                this.timestamp = timestamp;
            }

            public String getSign() {
                return sign;
            }

            public void setSign(String sign) {
                this.sign = sign;
            }

            public String getReturn_msg() {
                return return_msg;
            }

            public void setReturn_msg(String return_msg) {
                this.return_msg = return_msg;
            }

            public String getReturn_code() {
                return return_code;
            }

            public void setReturn_code(String return_code) {
                this.return_code = return_code;
            }

            public String getOrderNo() {
                return orderNo;
            }

            public void setOrderNo(String orderNo) {
                this.orderNo = orderNo;
            }
        }

        public static class DsBean {
            /**
             * balance : 0.00
             * margin : 0.00
             * lockbalance :
             * lockmargin :
             * id : 13
             */

            private String balance;
            private String margin;
            private String lockbalance;
            private String lockmargin;
            private String id;

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getMargin() {
                return margin;
            }

            public void setMargin(String margin) {
                this.margin = margin;
            }

            public String getLockbalance() {
                return lockbalance;
            }

            public void setLockbalance(String lockbalance) {
                this.lockbalance = lockbalance;
            }

            public String getLockmargin() {
                return lockmargin;
            }

            public void setLockmargin(String lockmargin) {
                this.lockmargin = lockmargin;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }
}
