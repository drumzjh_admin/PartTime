package com.example.parttime.entity;
//融云注册信息实体类
public class Entity_rimData {

    /**
     * code : 200
     * userId : 1
     * token : NKRLJ3K25EJVg5ivNlbPjt5Ri+aUB+Ps@y363.cn.rongnav.com;y363.cn.rongcfg.com
     */

    private int code;
    private String userId;
    private String token;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
