package com.example.parttime.entity;

import java.io.Serializable;
import java.util.List;

//接受的和发布的任务实体类
public class Entity_taskManagement implements Serializable {


    /**
     * states : 1
     * errorMsg :
     * result : {"ds":[{"type":"状态","remark1":"","remark4":"","empid":"12","M_OrderTakingListid":"8","M_task_listid":"","M_task_listname":"steven2任务","M_task_listMoney":"800.00","worktimeStart":"2019","worktimeEnd":"2019"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean implements Serializable{
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean implements Serializable{
            /**
             * type : 状态
             * remark1 :
             * remark4 :
             * empid : 12
             * M_OrderTakingListid : 8
             * M_task_listid :
             * M_task_listname : steven2任务
             * M_task_listMoney : 800.00
             * worktimeStart : 2019
             * worktimeEnd : 2019
             */

            private String type;
            private String remark1;
            private String remark4;
            private String empid;
            private String M_Collectid;
            private String Education;
            private String old_year;
            private String sex;
            private String M_OrderTakingListid;
            private String M_task_listid;
            private String M_task_listname;
            private String M_task_listMoney;
            private String worktimeStart;
            private String worktimeEnd;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getRemark1() {
                return remark1;
            }

            public void setRemark1(String remark1) {
                this.remark1 = remark1;
            }

            public String getRemark4() {
                return remark4;
            }

            public void setRemark4(String remark4) {
                this.remark4 = remark4;
            }

            public String getEmpid() {
                return empid;
            }

            public void setEmpid(String empid) {
                this.empid = empid;
            }

            public String getM_Collectid() {
                return M_Collectid;
            }

            public void setM_Collectid(String m_Collectid) {
                M_Collectid = m_Collectid;
            }

            public String getEducation() {
                return Education;
            }

            public void setEducation(String education) {
                Education = education;
            }

            public String getOld_year() {
                return old_year;
            }

            public void setOld_year(String old_year) {
                this.old_year = old_year;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getM_OrderTakingListid() {
                return M_OrderTakingListid;
            }

            public void setM_OrderTakingListid(String M_OrderTakingListid) {
                this.M_OrderTakingListid = M_OrderTakingListid;
            }

            public String getM_task_listid() {
                return M_task_listid;
            }

            public void setM_task_listid(String M_task_listid) {
                this.M_task_listid = M_task_listid;
            }

            public String getM_task_listname() {
                return M_task_listname;
            }

            public void setM_task_listname(String M_task_listname) {
                this.M_task_listname = M_task_listname;
            }

            public String getM_task_listMoney() {
                return M_task_listMoney;
            }

            public void setM_task_listMoney(String M_task_listMoney) {
                this.M_task_listMoney = M_task_listMoney;
            }

            public String getWorktimeStart() {
                return worktimeStart;
            }

            public void setWorktimeStart(String worktimeStart) {
                this.worktimeStart = worktimeStart;
            }

            public String getWorktimeEnd() {
                return worktimeEnd;
            }

            public void setWorktimeEnd(String worktimeEnd) {
                this.worktimeEnd = worktimeEnd;
            }
        }
    }
}
