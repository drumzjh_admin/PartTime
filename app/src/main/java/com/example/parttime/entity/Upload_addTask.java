package com.example.parttime.entity;
//添加任务实体类
public class Upload_addTask {

    /**
     * task_id : 1
     * empid : 1
     */

    private String task_id;
    private String empid;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

}
