package com.example.parttime.entity;
//任务上传实体类
public class Upload_taskData {

    /**
     * id : 1
     * employee_list_id : 昵称
     * worktype :
     * Money :
     * PersonalCount :
     * worktimeStart :
     * worktimeEnd :
     * addtime :
     * edittime :
     * address1 :省
     * address2 :市
     * address3 :镇(区)
     * address4 :详细地址
     * latitude :纬度
     * Diameter :经度
     * sex :
     * Education :学历
     * remark :
     * old_year :年龄
     * AduioUrl :视频地址
     * paytype :
     * remark1 :
     * remark2 :任务状态 1.使用中 0.草稿箱
     * remark3 :视频封面地址
     * remark4 :备注
     * remark5 :任务名称
     * remark6 :
     * remark7 :发布任务用户的头像地址
     * remark8 :
     * remark9 :
     * remark10 :
     */

    private String id;
    private String employee_list_id;
    private String worktype;
    private String Money;
    private String PersonalCount;
    private String worktimeStart;
    private String worktimeEnd;
    private String addtime;
    private String edittime;
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String latitude;
    private String Diameter;
    private String sex;
    private String Education;
    private String remark;
    private String old_year;
    private String AduioUrl;
    private String paytype;
    private String remark1;
    private String remark2;
    private String remark3;
    private String remark4;
    private String remark5;
    private String remark6;
    private String remark7;
    private String remark8;
    private String remark9;
    private String remark10;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployee_list_id() {
        return employee_list_id;
    }

    public void setEmployee_list_id(String employee_list_id) {
        this.employee_list_id = employee_list_id;
    }

    public String getWorktype() {
        return worktype;
    }

    public void setWorktype(String worktype) {
        this.worktype = worktype;
    }

    public String getMoney() {
        return Money;
    }

    public void setMoney(String Money) {
        this.Money = Money;
    }

    public String getPersonalCount() {
        return PersonalCount;
    }

    public void setPersonalCount(String PersonalCount) {
        this.PersonalCount = PersonalCount;
    }

    public String getWorktimeStart() {
        return worktimeStart;
    }

    public void setWorktimeStart(String worktimeStart) {
        this.worktimeStart = worktimeStart;
    }

    public String getWorktimeEnd() {
        return worktimeEnd;
    }

    public void setWorktimeEnd(String worktimeEnd) {
        this.worktimeEnd = worktimeEnd;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public String getEdittime() {
        return edittime;
    }

    public void setEdittime(String edittime) {
        this.edittime = edittime;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDiameter() {
        return Diameter;
    }

    public void setDiameter(String Diameter) {
        this.Diameter = Diameter;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String Education) {
        this.Education = Education;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOld_year() {
        return old_year;
    }

    public void setOld_year(String old_year) {
        this.old_year = old_year;
    }

    public String getAduioUrl() {
        return AduioUrl;
    }

    public void setAduioUrl(String AduioUrl) {
        this.AduioUrl = AduioUrl;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4;
    }

    public String getRemark5() {
        return remark5;
    }

    public void setRemark5(String remark5) {
        this.remark5 = remark5;
    }

    public String getRemark6() {
        return remark6;
    }

    public void setRemark6(String remark6) {
        this.remark6 = remark6;
    }

    public String getRemark7() {
        return remark7;
    }

    public void setRemark7(String remark7) {
        this.remark7 = remark7;
    }

    public String getRemark8() {
        return remark8;
    }

    public void setRemark8(String remark8) {
        this.remark8 = remark8;
    }

    public String getRemark9() {
        return remark9;
    }

    public void setRemark9(String remark9) {
        this.remark9 = remark9;
    }

    public String getRemark10() {
        return remark10;
    }

    public void setRemark10(String remark10) {
        this.remark10 = remark10;
    }
}

