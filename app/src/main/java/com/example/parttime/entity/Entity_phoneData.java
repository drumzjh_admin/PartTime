package com.example.parttime.entity;

import java.io.Serializable;
import java.util.List;

//个人信息实体类
public class Entity_phoneData implements Serializable {

    /**
     * states : 1
     * errorMsg :
     * result : {"ds":[{"id":"13","phone":"13119189840","type":"","addtime":"","edittime":"","address1":"","address2":"","address3":"","address4":"","id_card":"","sex":"","Education":"","latitude":"","Diameter":"","regtype":"","regUrl":"","CardURl":"","jiguan":"","remark1":"","remark2":"7425352","remark3":"","remark4":"","remark5":"","remark6":"","remark7":"","remark8":"","remark9":"","remark10":""}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean implements Serializable {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean implements Serializable {
            /**
             * id : 13
             * phone : 手机号
             * type :绑定类型1,微信，2qq，3微博
             * addtime :添加时间
             * edittime :修改时间
             * address1 :省
             * address2 :市
             * address3 :镇
             * address4 :详细地址
             * id_card :身份证号
             * sex :性别，1男，2女，3保密
             * Education :学历
             * latitude :纬度
             * Diameter :径度
             * regtype :注册类型0个人，1企业
             * regUrl :企业执照地址
             * CardURl :身份证注册地址
             * jiguan :籍贯
             * remark1 :备用1
             * remark2 :备用2
             * remark3 :备用3
             * remark4 :备用4
             * remark5 :备用5
             * remark6 :备用6
             * remark7 :备用7
             * remark8 :备用8
             * remark9 :备用9
             * remark10 :备用10
             */

            private String id;
            private String phone;
            private String type;
            private String addtime;
            private String edittime;
            private String address1;
            private String address2;
            private String address3;
            private String address4;
            private String id_card;
            private String sex;
            private String Education;
            private String latitude;
            private String Diameter;
            private String regtype;
            private String regUrl;
            private String CardURl;

            public String getCardURl2() {
                return CardURl2;
            }

            public void setCardURl2(String cardURl2) {
                CardURl2 = cardURl2;
            }

            private String CardURl2;
            private String jiguan;
            private String remark1;
            private String remark2;
            private String remark3;
            private String remark4;
            private String remark5;
            private String remark6;//营业执照地址
            private String remark7;//身份证有效期结束
            private String remark8;
            private String remark9;
            private String remark10;
            private String remark11;//身份证姓名

            public String getRemark11() {
                return remark11;
            }

            public void setRemark11(String remark11) {
                this.remark11 = remark11;
            }

            public String getRemark12() {
                return remark12;
            }

            public void setRemark12(String remark12) {
                this.remark12 = remark12;
            }

            public String getRemark13() {
                return remark13;
            }

            public void setRemark13(String remark13) {
                this.remark13 = remark13;
            }

            public String getRemark14() {
                return remark14;
            }

            public void setRemark14(String remark14) {
                this.remark14 = remark14;
            }

            public String getRemark15() {
                return remark15;
            }

            public void setRemark15(String remark15) {
                this.remark15 = remark15;
            }

            private String remark12;
            private String remark13;//身份证地址
            private String remark14;//籍贯
            private String remark15;//身份证有效期开始

            private String company1;//社会信用代码

            public String getCompany1() {
                return company1;
            }

            public void setCompany1(String company1) {
                this.company1 = company1;
            }

            public String getCompany2() {
                return company2;
            }

            public void setCompany2(String company2) {
                this.company2 = company2;
            }

            public String getCompany3() {
                return company3;
            }

            public void setCompany3(String company3) {
                this.company3 = company3;
            }

            public String getCompany4() {
                return company4;
            }

            public void setCompany4(String company4) {
                this.company4 = company4;
            }

            public String getCompany5() {
                return company5;
            }

            public void setCompany5(String company5) {
                this.company5 = company5;
            }

            public String getCompany6() {
                return company6;
            }

            public void setCompany6(String company6) {
                this.company6 = company6;
            }

            public String getCompany7() {
                return company7;
            }

            public void setCompany7(String company7) {
                this.company7 = company7;
            }

            public String getCompany8() {
                return company8;
            }

            public void setCompany8(String company8) {
                this.company8 = company8;
            }

            public String getCompany9() {
                return company9;
            }

            public void setCompany9(String company9) {
                this.company9 = company9;
            }

            public String getCompany10() {
                return company10;
            }

            public void setCompany10(String company10) {
                this.company10 = company10;
            }

            public String getCompany11() {
                return company11;
            }

            public void setCompany11(String company11) {
                this.company11 = company11;
            }

            private String company2;
            private String company3;//经营范围
            private String company4;//注册日期
            private String company5;//法人
            private String company6;//注册资本
            private String company7;//证照编号
            private String company8;//地址
            private String company9;//公司名
            private String company10;//到期时期
            private String company11;//公司类型

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getEdittime() {
                return edittime;
            }

            public void setEdittime(String edittime) {
                this.edittime = edittime;
            }

            public String getAddress1() {
                return address1;
            }

            public void setAddress1(String address1) {
                this.address1 = address1;
            }

            public String getAddress2() {
                return address2;
            }

            public void setAddress2(String address2) {
                this.address2 = address2;
            }

            public String getAddress3() {
                return address3;
            }

            public void setAddress3(String address3) {
                this.address3 = address3;
            }

            public String getAddress4() {
                return address4;
            }

            public void setAddress4(String address4) {
                this.address4 = address4;
            }

            public String getId_card() {
                return id_card;
            }

            public void setId_card(String id_card) {
                this.id_card = id_card;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getEducation() {
                return Education;
            }

            public void setEducation(String Education) {
                this.Education = Education;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getDiameter() {
                return Diameter;
            }

            public void setDiameter(String Diameter) {
                this.Diameter = Diameter;
            }

            public String getRegtype() {
                return regtype;
            }

            public void setRegtype(String regtype) {
                this.regtype = regtype;
            }

            public String getRegUrl() {
                return regUrl;
            }

            public void setRegUrl(String regUrl) {
                this.regUrl = regUrl;
            }

            public String getCardURl() {
                return CardURl;
            }

            public void setCardURl(String CardURl) {
                this.CardURl = CardURl;
            }

            public String getJiguan() {
                return jiguan;
            }

            public void setJiguan(String jiguan) {
                this.jiguan = jiguan;
            }

            public String getRemark1() {
                return remark1;
            }

            public void setRemark1(String remark1) {
                this.remark1 = remark1;
            }

            public String getRemark2() {
                return remark2;
            }

            public void setRemark2(String remark2) {
                this.remark2 = remark2;
            }

            public String getRemark3() {
                return remark3;
            }

            public void setRemark3(String remark3) {
                this.remark3 = remark3;
            }

            public String getRemark4() {
                return remark4;
            }

            public void setRemark4(String remark4) {
                this.remark4 = remark4;
            }

            public String getRemark5() {
                return remark5;
            }

            public void setRemark5(String remark5) {
                this.remark5 = remark5;
            }

            public String getRemark6() {
                return remark6;
            }

            public void setRemark6(String remark6) {
                this.remark6 = remark6;
            }

            public String getRemark7() {
                return remark7;
            }

            public void setRemark7(String remark7) {
                this.remark7 = remark7;
            }

            public String getRemark8() {
                return remark8;
            }

            public void setRemark8(String remark8) {
                this.remark8 = remark8;
            }

            public String getRemark9() {
                return remark9;
            }

            public void setRemark9(String remark9) {
                this.remark9 = remark9;
            }

            public String getRemark10() {
                return remark10;
            }

            public void setRemark10(String remark10) {
                this.remark10 = remark10;
            }
        }
    }
}
