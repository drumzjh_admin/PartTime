package com.example.parttime.entity;

//统一返回处理实体类
public class Entity_HttpResult {

    /**
     * states : 0
     * errorMsg :
     * result :
     */

    private String states;
    private String errorMsg;
    private String result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
