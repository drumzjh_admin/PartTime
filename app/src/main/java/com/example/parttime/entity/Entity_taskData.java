package com.example.parttime.entity;

import java.io.Serializable;
import java.util.List;
//任务信息实体类
public class Entity_taskData implements Serializable {

    /**
     * states : 1
     * errorMsg :
     * result : {"ds":[{"M_task_list_remark":"","M_employee_list_remark4":"","M_task_list_remark7":"images/mrlg/20200704171136703.jpg","M_task_list_remark9":"0","M_task_list_remark2":"1","display_name":"传单派发","latitude":"","Diameter":"","address1":"上海","M_task_list_remark5":"发传单","M_task_list_remark21":"1","M_task_list_remark3":"","address2":"上海","address3":"嘉定区","address4":"惠南","sex":"1","Education":"不限","old_year":"不限","M_task_list_id":"1059","employee_list_id":"13","M_employee_list_remark1":"羊羊羊","worktype":"36","Money":"66.00","PersonalCount":"66","worktimeStart":"2020/7/6 17:09:00","worktimeEnd":"2020/7/9 17:09:00","M_task_list_remark6":"","AduioUrl":"Video/mrlg/20200704171136438.mp4"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean implements Serializable {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean implements Serializable {
            /**
             * M_task_list_remark :
             * M_employee_list_remark4 :
             * M_task_list_remark7 : images/mrlg/20200704171136703.jpg
             * M_task_list_remark9 : 0
             * M_task_list_remark2 : 1
             * display_name : 传单派发
             * latitude :
             * Diameter :
             * address1 : 上海
             * M_task_list_remark5 : 发传单
             * M_task_list_remark21 : 1
             * M_task_list_remark3 :
             * address2 : 上海
             * address3 : 嘉定区
             * address4 : 惠南
             * sex : 1
             * Education : 不限
             * old_year : 不限
             * M_task_list_id : 1059
             * employee_list_id : 13
             * M_employee_list_remark1 : 羊羊羊
             * worktype : 36
             * Money : 66.00
             * PersonalCount : 66
             * worktimeStart : 2020/7/6 17:09:00
             * worktimeEnd : 2020/7/9 17:09:00
             * M_task_list_remark6 :
             * AduioUrl : Video/mrlg/20200704171136438.mp4
             */

            private String M_task_list_remark;
            private String M_employee_list_remark4;
            private String M_task_list_remark7;
            private String M_task_list_remark9;
            private String M_task_list_remark2;
            private String display_name;
            private String latitude;
            private String Diameter;
            private String address1;
            private String M_task_list_remark5;
            private String M_task_list_remark21;
            private String M_task_list_remark3;
            private String address2;
            private String address3;
            private String address4;
            private String sex;
            private String Education;
            private String old_year;
            private String M_task_list_id;
            private String employee_list_id;
            private String M_employee_list_remark1;
            private String worktype;
            private String Money;
            private String PersonalCount;
            private String worktimeStart;
            private String worktimeEnd;
            private String M_task_list_remark6;
            private String AduioUrl;

            public String getM_task_list_remark() {
                return M_task_list_remark;
            }

            public void setM_task_list_remark(String M_task_list_remark) {
                this.M_task_list_remark = M_task_list_remark;
            }

            public String getM_employee_list_remark4() {
                return M_employee_list_remark4;
            }

            public void setM_employee_list_remark4(String M_employee_list_remark4) {
                this.M_employee_list_remark4 = M_employee_list_remark4;
            }

            public String getM_task_list_remark7() {
                return M_task_list_remark7;
            }

            public void setM_task_list_remark7(String M_task_list_remark7) {
                this.M_task_list_remark7 = M_task_list_remark7;
            }

            public String getM_task_list_remark9() {
                return M_task_list_remark9;
            }

            public void setM_task_list_remark9(String M_task_list_remark9) {
                this.M_task_list_remark9 = M_task_list_remark9;
            }

            public String getM_task_list_remark2() {
                return M_task_list_remark2;
            }

            public void setM_task_list_remark2(String M_task_list_remark2) {
                this.M_task_list_remark2 = M_task_list_remark2;
            }

            public String getDisplay_name() {
                return display_name;
            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getDiameter() {
                return Diameter;
            }

            public void setDiameter(String Diameter) {
                this.Diameter = Diameter;
            }

            public String getAddress1() {
                return address1;
            }

            public void setAddress1(String address1) {
                this.address1 = address1;
            }

            public String getM_task_list_remark5() {
                return M_task_list_remark5;
            }

            public void setM_task_list_remark5(String M_task_list_remark5) {
                this.M_task_list_remark5 = M_task_list_remark5;
            }

            public String getM_task_list_remark21() {
                return M_task_list_remark21;
            }

            public void setM_task_list_remark21(String M_task_list_remark21) {
                this.M_task_list_remark21 = M_task_list_remark21;
            }

            public String getM_task_list_remark3() {
                return M_task_list_remark3;
            }

            public void setM_task_list_remark3(String M_task_list_remark3) {
                this.M_task_list_remark3 = M_task_list_remark3;
            }

            public String getAddress2() {
                return address2;
            }

            public void setAddress2(String address2) {
                this.address2 = address2;
            }

            public String getAddress3() {
                return address3;
            }

            public void setAddress3(String address3) {
                this.address3 = address3;
            }

            public String getAddress4() {
                return address4;
            }

            public void setAddress4(String address4) {
                this.address4 = address4;
            }

            public String getSex() {
                return sex;
            }

            public void setSex(String sex) {
                this.sex = sex;
            }

            public String getEducation() {
                return Education;
            }

            public void setEducation(String Education) {
                this.Education = Education;
            }

            public String getOld_year() {
                return old_year;
            }

            public void setOld_year(String old_year) {
                this.old_year = old_year;
            }

            public String getM_task_list_id() {
                return M_task_list_id;
            }

            public void setM_task_list_id(String M_task_list_id) {
                this.M_task_list_id = M_task_list_id;
            }

            public String getEmployee_list_id() {
                return employee_list_id;
            }

            public void setEmployee_list_id(String employee_list_id) {
                this.employee_list_id = employee_list_id;
            }

            public String getM_employee_list_remark1() {
                return M_employee_list_remark1;
            }

            public void setM_employee_list_remark1(String M_employee_list_remark1) {
                this.M_employee_list_remark1 = M_employee_list_remark1;
            }

            public String getWorktype() {
                return worktype;
            }

            public void setWorktype(String worktype) {
                this.worktype = worktype;
            }

            public String getMoney() {
                return Money;
            }

            public void setMoney(String Money) {
                this.Money = Money;
            }

            public String getPersonalCount() {
                return PersonalCount;
            }

            public void setPersonalCount(String PersonalCount) {
                this.PersonalCount = PersonalCount;
            }

            public String getWorktimeStart() {
                return worktimeStart;
            }

            public void setWorktimeStart(String worktimeStart) {
                this.worktimeStart = worktimeStart;
            }

            public String getWorktimeEnd() {
                return worktimeEnd;
            }

            public void setWorktimeEnd(String worktimeEnd) {
                this.worktimeEnd = worktimeEnd;
            }

            public String getM_task_list_remark6() {
                return M_task_list_remark6;
            }

            public void setM_task_list_remark6(String M_task_list_remark6) {
                this.M_task_list_remark6 = M_task_list_remark6;
            }

            public String getAduioUrl() {
                return AduioUrl;
            }

            public void setAduioUrl(String AduioUrl) {
                this.AduioUrl = AduioUrl;
            }

        }
    }


    /**
     * states : 1
     * errorMsg :
     * result : {"ds":[{"M_employee_list_remark4":"","M_task_list_remark7":"","M_task_list_remark2":"1","display_name":"测试1","latitude":"31.245105","Diameter":"121.506377","address1":"","M_task_list_remark5":"哈哈","M_task_list_remark21":"1","M_task_list_remark3":"images/mrlg/20191214194516.jpg","address2":"","address3":"","address4":"东方路2000号","sex":"3","Education":"本科","old_year":"36~40岁","M_task_list_id":"1048","employee_list_id":"12","M_employee_list_remark1":"","worktype":"58","Money":"1.00","PersonalCount":"30","worktimeStart":"2019/12/14 19:44:00","worktimeEnd":"2019/12/17 19:44:00","M_task_list_remark6":"1","AduioUrl":"Video/mrlg/20191214194515.mp4"},{"M_employee_list_remark4":"","M_task_list_remark7":"","M_task_list_remark2":"1","display_name":"打包分拣","latitude":"31.189516","Diameter":"121.561075","address1":"","M_task_list_remark5":"搬运工","M_task_list_remark21":"1","M_task_list_remark3":"images/mrlg/20191213144508.jpg","address2":"","address3":"","address4":"北中路6号","sex":"3","Education":"不限","old_year":"18~22岁","M_task_list_id":"1047","employee_list_id":"12","M_employee_list_remark1":"","worktype":"40","Money":"20.00","PersonalCount":"1","worktimeStart":"2019/12/13 14:43:00","worktimeEnd":"2019/12/14 14:43:00","M_task_list_remark6":"","AduioUrl":"Video/mrlg/20191213144507.mp4"},{"M_employee_list_remark4":"","M_task_list_remark7":"","M_task_list_remark2":"1","display_name":"群众演员","latitude":"31.12003","Diameter":"121.581664","address1":"","M_task_list_remark5":"陪聊","M_task_list_remark21":"1","M_task_list_remark3":"images/mrlg/20191207085026.jpg","address2":"","address3":"","address4":"周浦","sex":"2","Education":"大专","old_year":"41~45岁","M_task_list_id":"1046","employee_list_id":"9","M_employee_list_remark1":"","worktype":"56","Money":"5.00","PersonalCount":"2","worktimeStart":"2019/12/7 8:50:00","worktimeEnd":"2019/12/10 8:52:00","M_task_list_remark6":"","AduioUrl":"Video/mrlg/20191207085025.mp4"},{"M_employee_list_remark4":"","M_task_list_remark7":"images/mrlg/20191205144510.jpg","M_task_list_remark2":"1","display_name":"礼仪模特","latitude":"31.243918","Diameter":"121.493626","address1":"","M_task_list_remark5":"steven2任务","M_task_list_remark21":"1","M_task_list_remark3":"images/mrlg/20191205145103.jpg","address2":"","address3":"","address4":"南京东路120号","sex":"1","Education":"初中","old_year":"29~30岁","M_task_list_id":"1045","employee_list_id":"8","M_employee_list_remark1":"steven2","worktype":"54","Money":"800.00","PersonalCount":"1","worktimeStart":"2019/12/5 14:45:00","worktimeEnd":"2019/12/7 14:46:00","M_task_list_remark6":"","AduioUrl":"Video/mrlg/20191205145053.mp4"},{"M_employee_list_remark4":"","M_task_list_remark7":"images/mrlg/20191204144955.jpg","M_task_list_remark2":"1","display_name":"主播","latitude":"31.236025","Diameter":"121.521971","address1":"","M_task_list_remark5":"视频任务","M_task_list_remark21":"1","M_task_list_remark3":"images/mrlg/20191205142722.jpg","address2":"","address3":"","address4":"陆家嘴","sex":"1","Education":"小学","old_year":"23~28岁","M_task_list_id":"2","employee_list_id":"7","M_employee_list_remark1":"steven","worktype":"57","Money":"2000.00","PersonalCount":"1","worktimeStart":"2019/12/3 14:24:00","worktimeEnd":"2019/12/7 14:24:00","M_task_list_remark6":"","AduioUrl":"Video/mrlg/20191205142656.mp4"},{"M_employee_list_remark4":"","M_task_list_remark7":"images/mrlg/20191204144955.jpg","M_task_list_remark2":"1","display_name":"演出","latitude":"31.228989","Diameter":"121.547778","address1":"","M_task_list_remark5":"测试任务","M_task_list_remark21":"1","M_task_list_remark3":"","address2":"","address3":"","address4":"东方艺术中心","sex":"1","Education":"初中","old_year":"29~30岁","M_task_list_id":"1","employee_list_id":"7","M_employee_list_remark1":"steven","worktype":"55","Money":"600.00","PersonalCount":"2","worktimeStart":"2019/12/5 13:47:00","worktimeEnd":"2019/12/7 13:47:00","M_task_list_remark6":"","AduioUrl":"Video/mrlg/20191205134808.mp4"}]}
     */

//    private String states;
//    private String errorMsg;
//    private ResultBean result;
//
//    public String getStates() {
//        return states;
//    }
//
//    public void setStates(String states) {
//        this.states = states;
//    }
//
//    public String getErrorMsg() {
//        return errorMsg;
//    }
//
//    public void setErrorMsg(String errorMsg) {
//        this.errorMsg = errorMsg;
//    }
//
//    public ResultBean getResult() {
//        return result;
//    }
//
//    public void setResult(ResultBean result) {
//        this.result = result;
//    }
//
//    public static class ResultBean implements Serializable {
//        private List<DsBean> ds;
//
//        public List<DsBean> getDs() {
//            return ds;
//        }
//
//        public void setDs(List<DsBean> ds) {
//            this.ds = ds;
//        }
//
//        public static class DsBean implements Serializable {
//            /**
//             * M_employee_list_remark4 :
//             * M_task_list_remark7 :
//             * M_task_list_remark2 : 1
//             * display_name : 测试1
//             * latitude : 31.245105
//             * Diameter : 121.506377
//             * address1 :
//             * M_task_list_remark5 : 哈哈
//             * M_task_list_remark21 : 1
//             * M_task_list_remark3 : images/mrlg/20191214194516.jpg
//             * address2 :
//             * address3 :
//             * address4 : 东方路2000号
//             * sex : 3
//             * Education : 本科
//             * old_year : 36~40岁
//             * M_task_list_id : 1048
//             * employee_list_id : 12
//             * M_employee_list_remark1 :
//             * worktype : 58
//             * Money : 1.00
//             * PersonalCount : 30
//             * worktimeStart : 2019/12/14 19:44:00
//             * worktimeEnd : 2019/12/17 19:44:00
//             * M_task_list_remark6 : 1
//             * AduioUrl : Video/mrlg/20191214194515.mp4
//             */
//
//            private String M_employee_list_remark4;
//            private String M_task_list_remark7;
//            private String M_task_list_remark9;
//            private String M_task_list_remark2;
//            private String display_name;
//            private String latitude;
//            private String Diameter;
//            private String address1;
//            private String M_task_list_remark5;
//            private String M_task_list_remark21;
//            private String M_task_list_remark3;
//            private String address2;
//            private String address3;
//            private String address4;
//            private String sex;
//            private String Education;
//            private String old_year;
//            private String M_task_list_id;
//            private String employee_list_id;
//            private String M_employee_list_remark1;
//            private String worktype;
//            private String Money;
//            private String PersonalCount;
//            private String worktimeStart;
//            private String worktimeEnd;
//            private String M_task_list_remark6;
//            private String AduioUrl;
//
//            public String getM_employee_list_remark4() {
//                return M_employee_list_remark4;
//            }
//
//            public void setM_employee_list_remark4(String M_employee_list_remark4) {
//                this.M_employee_list_remark4 = M_employee_list_remark4;
//            }
//
//            public String getM_task_list_remark7() {
//                return M_task_list_remark7;
//            }
//
//            public void setM_task_list_remark7(String M_task_list_remark7) {
//                this.M_task_list_remark7 = M_task_list_remark7;
//            }
//
//            public String getM_task_list_remark9() {
//                return M_task_list_remark9;
//            }
//
//            public void setM_task_list_remark9(String m_task_list_remark9) {
//                M_task_list_remark9 = m_task_list_remark9;
//            }
//
//            public String getM_task_list_remark2() {
//                return M_task_list_remark2;
//            }
//
//            public void setM_task_list_remark2(String M_task_list_remark2) {
//                this.M_task_list_remark2 = M_task_list_remark2;
//            }
//
//            public String getDisplay_name() {
//                return display_name;
//            }
//
//            public void setDisplay_name(String display_name) {
//                this.display_name = display_name;
//            }
//
//            public String getLatitude() {
//                return latitude;
//            }
//
//            public void setLatitude(String latitude) {
//                this.latitude = latitude;
//            }
//
//            public String getDiameter() {
//                return Diameter;
//            }
//
//            public void setDiameter(String Diameter) {
//                this.Diameter = Diameter;
//            }
//
//            public String getAddress1() {
//                return address1;
//            }
//
//            public void setAddress1(String address1) {
//                this.address1 = address1;
//            }
//
//            public String getM_task_list_remark5() {
//                return M_task_list_remark5;
//            }
//
//            public void setM_task_list_remark5(String M_task_list_remark5) {
//                this.M_task_list_remark5 = M_task_list_remark5;
//            }
//
//            public String getM_task_list_remark21() {
//                return M_task_list_remark21;
//            }
//
//            public void setM_task_list_remark21(String M_task_list_remark21) {
//                this.M_task_list_remark21 = M_task_list_remark21;
//            }
//
//            public String getM_task_list_remark3() {
//                return M_task_list_remark3;
//            }
//
//            public void setM_task_list_remark3(String M_task_list_remark3) {
//                this.M_task_list_remark3 = M_task_list_remark3;
//            }
//
//            public String getAddress2() {
//                return address2;
//            }
//
//            public void setAddress2(String address2) {
//                this.address2 = address2;
//            }
//
//            public String getAddress3() {
//                return address3;
//            }
//
//            public void setAddress3(String address3) {
//                this.address3 = address3;
//            }
//
//            public String getAddress4() {
//                return address4;
//            }
//
//            public void setAddress4(String address4) {
//                this.address4 = address4;
//            }
//
//            public String getSex() {
//                return sex;
//            }
//
//            public void setSex(String sex) {
//                this.sex = sex;
//            }
//
//            public String getEducation() {
//                return Education;
//            }
//
//            public void setEducation(String Education) {
//                this.Education = Education;
//            }
//
//            public String getOld_year() {
//                return old_year;
//            }
//
//            public void setOld_year(String old_year) {
//                this.old_year = old_year;
//            }
//
//            public String getM_task_list_id() {
//                return M_task_list_id;
//            }
//
//            public void setM_task_list_id(String M_task_list_id) {
//                this.M_task_list_id = M_task_list_id;
//            }
//
//            public String getEmployee_list_id() {
//                return employee_list_id;
//            }
//
//            public void setEmployee_list_id(String employee_list_id) {
//                this.employee_list_id = employee_list_id;
//            }
//
//            public String getM_employee_list_remark1() {
//                return M_employee_list_remark1;
//            }
//
//            public void setM_employee_list_remark1(String M_employee_list_remark1) {
//                this.M_employee_list_remark1 = M_employee_list_remark1;
//            }
//
//            public String getWorktype() {
//                return worktype;
//            }
//
//            public void setWorktype(String worktype) {
//                this.worktype = worktype;
//            }
//
//            public String getMoney() {
//                return Money;
//            }
//
//            public void setMoney(String Money) {
//                this.Money = Money;
//            }
//
//            public String getPersonalCount() {
//                return PersonalCount;
//            }
//
//            public void setPersonalCount(String PersonalCount) {
//                this.PersonalCount = PersonalCount;
//            }
//
//            public String getWorktimeStart() {
//                return worktimeStart;
//            }
//
//            public void setWorktimeStart(String worktimeStart) {
//                this.worktimeStart = worktimeStart;
//            }
//
//            public String getWorktimeEnd() {
//                return worktimeEnd;
//            }
//
//            public void setWorktimeEnd(String worktimeEnd) {
//                this.worktimeEnd = worktimeEnd;
//            }
//
//            public String getM_task_list_remark6() {
//                return M_task_list_remark6;
//            }
//
//            public void setM_task_list_remark6(String M_task_list_remark6) {
//                this.M_task_list_remark6 = M_task_list_remark6;
//            }
//
//            public String getAduioUrl() {
//                return AduioUrl;
//            }
//
//            public void setAduioUrl(String AduioUrl) {
//                this.AduioUrl = AduioUrl;
//            }
//        }
//    }
}
