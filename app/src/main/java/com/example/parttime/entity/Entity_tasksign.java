package com.example.parttime.entity;

import java.util.List;

//签到状态的实体类
public class Entity_tasksign {

    /**
     * states : 1
     * errorMsg :
     * result : {"ds":[{"id":"1","addtime":"","empid":"1","endtime":"","remark1":"1","remark2":"","remark3":"","remark4":"","remark5":"","remark6":"","remark7":"","remark8":"","remark9":"","remark10":""}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean {
            /**
             * id : 1
             * addtime :
             * empid : 1
             * endtime :
             * remark1 : 1
             * remark2 :
             * remark3 :
             * remark4 :
             * remark5 :
             * remark6 :
             * remark7 :
             * remark8 :
             * remark9 :
             * remark10 :
             */

            private String id;
            private String addtime;
            private String empid;
            private String endtime;
            private String remark1;
            private String remark2;
            private String remark3;
            private String remark4;
            private String remark5;
            private String remark6;
            private String remark7;
            private String remark8;
            private String remark9;
            private String remark10;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getEmpid() {
                return empid;
            }

            public void setEmpid(String empid) {
                this.empid = empid;
            }

            public String getEndtime() {
                return endtime;
            }

            public void setEndtime(String endtime) {
                this.endtime = endtime;
            }

            public String getRemark1() {
                return remark1;
            }

            public void setRemark1(String remark1) {
                this.remark1 = remark1;
            }

            public String getRemark2() {
                return remark2;
            }

            public void setRemark2(String remark2) {
                this.remark2 = remark2;
            }

            public String getRemark3() {
                return remark3;
            }

            public void setRemark3(String remark3) {
                this.remark3 = remark3;
            }

            public String getRemark4() {
                return remark4;
            }

            public void setRemark4(String remark4) {
                this.remark4 = remark4;
            }

            public String getRemark5() {
                return remark5;
            }

            public void setRemark5(String remark5) {
                this.remark5 = remark5;
            }

            public String getRemark6() {
                return remark6;
            }

            public void setRemark6(String remark6) {
                this.remark6 = remark6;
            }

            public String getRemark7() {
                return remark7;
            }

            public void setRemark7(String remark7) {
                this.remark7 = remark7;
            }

            public String getRemark8() {
                return remark8;
            }

            public void setRemark8(String remark8) {
                this.remark8 = remark8;
            }

            public String getRemark9() {
                return remark9;
            }

            public void setRemark9(String remark9) {
                this.remark9 = remark9;
            }

            public String getRemark10() {
                return remark10;
            }

            public void setRemark10(String remark10) {
                this.remark10 = remark10;
            }
        }
    }
}

