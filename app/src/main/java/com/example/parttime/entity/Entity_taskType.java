package com.example.parttime.entity;

import java.util.List;

//任务类型实体类
public class Entity_taskType {

    /**
     * states : 1
     * errorMsg :
     * result : {"ds":[{"id":"54","parentid":"17","innervalue":"1","display_name":"礼仪模特","other_property":"","remark":"","status":"1","orderid":"1","create_userid":"2999","create_time":"2019/6/2 21:56:09","modify_userid":"","modify_time":""},{"id":"55","parentid":"17","innervalue":"2","display_name":"演出","other_property":"","remark":"","status":"1","orderid":"2","create_userid":"2999","create_time":"2019/6/2 21:56:13","modify_userid":"","modify_time":""},{"id":"56","parentid":"17","innervalue":"3","display_name":"群众演员","other_property":"","remark":"","status":"1","orderid":"3","create_userid":"2999","create_time":"2019/6/2 21:56:17","modify_userid":"","modify_time":""},{"id":"57","parentid":"17","innervalue":"4","display_name":"主播","other_property":"","remark":"","status":"1","orderid":"4","create_userid":"2999","create_time":"2019/6/2 21:56:20","modify_userid":"","modify_time":""},{"id":"36","parentid":"18","innervalue":"1","display_name":"传单派发","other_property":"","remark":"","status":"1","orderid":"1","create_userid":"2999","create_time":"2019/6/2 21:53:27","modify_userid":"","modify_time":""},{"id":"37","parentid":"18","innervalue":"2","display_name":"问卷调查","other_property":"","remark":"","status":"1","orderid":"2","create_userid":"2999","create_time":"2019/6/2 21:53:36","modify_userid":"","modify_time":""},{"id":"38","parentid":"18","innervalue":"3","display_name":"审核录入","other_property":"","remark":"","status":"1","orderid":"3","create_userid":"2999","create_time":"2019/6/2 21:53:44","modify_userid":"","modify_time":""},{"id":"39","parentid":"18","innervalue":"4","display_name":"地推拉访","other_property":"","remark":"","status":"1","orderid":"4","create_userid":"2999","create_time":"2019/6/2 21:53:53","modify_userid":"","modify_time":""},{"id":"40","parentid":"18","innervalue":"5","display_name":"打包分拣","other_property":"","remark":"","status":"1","orderid":"5","create_userid":"2999","create_time":"2019/6/2 21:53:58","modify_userid":"","modify_time":""},{"id":"41","parentid":"18","innervalue":"6","display_name":"充场","other_property":"","remark":"","status":"1","orderid":"6","create_userid":"2999","create_time":"2019/6/2 21:54:02","modify_userid":"","modify_time":""},{"id":"42","parentid":"18","innervalue":"7","display_name":"保洁","other_property":"","remark":"","status":"1","orderid":"7","create_userid":"2999","create_time":"2019/6/2 21:54:07","modify_userid":"","modify_time":""},{"id":"58","parentid":"18","innervalue":"8","display_name":"测试1","other_property":"","remark":"","status":"1","orderid":"8","create_userid":"1","create_time":"2019/6/22 17:21:21","modify_userid":"","modify_time":""},{"id":"59","parentid":"18","innervalue":"9","display_name":"物联网工程","other_property":"","remark":"","status":"1","orderid":"9","create_userid":"1","create_time":"2019/6/22 17:22:41","modify_userid":"","modify_time":""}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean {
            /**
             * id : 54
             * parentid : 17
             * innervalue : 1
             * display_name : 礼仪模特
             * other_property :
             * remark :
             * status : 1
             * orderid : 1
             * create_userid : 2999
             * create_time : 2019/6/2 21:56:09
             * modify_userid :
             * modify_time :
             */

            private String id;
            private String parentid;
            private String innervalue;
            private String display_name;
            private String other_property;
            private String remark;
            private String status;
            private String orderid;
            private String create_userid;
            private String create_time;
            private String modify_userid;
            private String modify_time;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getParentid() {
                return parentid;
            }

            public void setParentid(String parentid) {
                this.parentid = parentid;
            }

            public String getInnervalue() {
                return innervalue;
            }

            public void setInnervalue(String innervalue) {
                this.innervalue = innervalue;
            }

            public String getDisplay_name() {
                return display_name;
            }

            public void setDisplay_name(String display_name) {
                this.display_name = display_name;
            }

            public String getOther_property() {
                return other_property;
            }

            public void setOther_property(String other_property) {
                this.other_property = other_property;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getOrderid() {
                return orderid;
            }

            public void setOrderid(String orderid) {
                this.orderid = orderid;
            }

            public String getCreate_userid() {
                return create_userid;
            }

            public void setCreate_userid(String create_userid) {
                this.create_userid = create_userid;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }

            public String getModify_userid() {
                return modify_userid;
            }

            public void setModify_userid(String modify_userid) {
                this.modify_userid = modify_userid;
            }

            public String getModify_time() {
                return modify_time;
            }

            public void setModify_time(String modify_time) {
                this.modify_time = modify_time;
            }
        }
    }
}
