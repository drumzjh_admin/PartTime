package com.example.parttime.entity;

import java.io.Serializable;
import java.util.List;

//押金和余额实体类
public class Entity_moneyAndMortgage implements Serializable {


    /**
     * states : 1
     * errorMsg : 成功。
     * result : {"ds":[{"balance":"0.00","margin":"0.00","lockbalance":"","lockmargin":"","id":"13"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean implements Serializable {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean implements Serializable {
            /**
             * balance : 0.00
             * margin : 0.00
             * lockbalance :
             * lockmargin :
             * id : 13
             */

            private String balance;
            private String margin;
            private String lockbalance;
            private String lockmargin;
            private String id;

            public String getBalance() {
                return balance;
            }

            public void setBalance(String balance) {
                this.balance = balance;
            }

            public String getMargin() {
                return margin;
            }

            public void setMargin(String margin) {
                this.margin = margin;
            }

            public String getLockbalance() {
                return lockbalance;
            }

            public void setLockbalance(String lockbalance) {
                this.lockbalance = lockbalance;
            }

            public String getLockmargin() {
                return lockmargin;
            }

            public void setLockmargin(String lockmargin) {
                this.lockmargin = lockmargin;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }

}
