package com.example.parttime.entity;
import java.util.List;


//押金详情实体类
public class Entity_mortgage {
    /**
     * states : 1
     * errorMsg : 成功。
     * result : {"ds":[{"MoneyY":"2.00","addtime":"2020-4-26 17:00:11","employee_list_id":"3","type":"0","M_task_list_id":"1","remark5":"测试任务"},{"MoneyY":"2.00","addtime":"2020-4-26 17:18:05","employee_list_id":"3","type":"0","M_task_list_id":"1","remark5":"测试任务"},{"MoneyY":"2.00","addtime":"2020-4-26 17:21:12","employee_list_id":"3","type":"0","M_task_list_id":"1","remark5":"测试任务"},{"MoneyY":"2.00","addtime":"2020-4-26 17:24:43","employee_list_id":"3","type":"0","M_task_list_id":"1","remark5":"测试任务"},{"MoneyY":"2.00","addtime":"2020-4-26 17:26:34","employee_list_id":"3","type":"1","M_task_list_id":"1","remark5":"测试任务"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<DsBean> ds;

        public List<DsBean> getDs() {
            return ds;
        }

        public void setDs(List<DsBean> ds) {
            this.ds = ds;
        }

        public static class DsBean {
            /**
             * MoneyY : 2.00
             * addtime : 2020-4-26 17:00:11
             * employee_list_id : 3
             * type : 0
             * M_task_list_id : 1
             * remark5 : 测试任务
             */

            private String MoneyY;
            private String addtime;
            private String employee_list_id;
            private String type;
            private String M_task_list_id;
            private String remark5;

            public String getMoneyY() {
                return MoneyY;
            }

            public void setMoneyY(String MoneyY) {
                this.MoneyY = MoneyY;
            }

            public String getAddtime() {
                return addtime;
            }

            public void setAddtime(String addtime) {
                this.addtime = addtime;
            }

            public String getEmployee_list_id() {
                return employee_list_id;
            }

            public void setEmployee_list_id(String employee_list_id) {
                this.employee_list_id = employee_list_id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getM_task_list_id() {
                return M_task_list_id;
            }

            public void setM_task_list_id(String M_task_list_id) {
                this.M_task_list_id = M_task_list_id;
            }

            public String getRemark5() {
                return remark5;
            }

            public void setRemark5(String remark5) {
                this.remark5 = remark5;
            }
        }
    }
}
