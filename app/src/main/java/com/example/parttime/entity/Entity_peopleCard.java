package com.example.parttime.entity;

import java.util.List;

//身份证信息实体类
public class Entity_peopleCard {


    /**
     * states : 1
     * errorMsg : 成功
     * result : {"Values":[{"住址":"上海市","出生":"19800208","姓名":"施建军","公民身份号码":"310225198******8121X","性别":"男","民族":"汉","失效日期":"20350915","签发机关":"上海市公安局浦东分局","签发日期":"20150915"}]}
     */

    private String states;
    private String errorMsg;
    private ResultBean result;

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class ResultBean {
        private List<ValuesBean> Values;

        public List<ValuesBean> getValues() {
            return Values;
        }

        public void setValues(List<ValuesBean> Values) {
            this.Values = Values;
        }

        public static class ValuesBean {
            /**
             * 住址 : 上海市
             * 出生 : 19800208
             * 姓名 : 施建军
             * 公民身份号码 : 310225198******8121X
             * 性别 : 男
             * 民族 : 汉
             * 失效日期 : 20350915
             * 签发机关 : 上海市公安局浦东分局
             * 签发日期 : 20150915
             */

            private String 住址;
            private String 出生;
            private String 姓名;
            private String 公民身份号码;
            private String 性别;
            private String 民族;
            private String 失效日期;
            private String 签发机关;
            private String 签发日期;

            public String get住址() {
                return 住址;
            }

            public void set住址(String 住址) {
                this.住址 = 住址;
            }

            public String get出生() {
                return 出生;
            }

            public void set出生(String 出生) {
                this.出生 = 出生;
            }

            public String get姓名() {
                return 姓名;
            }

            public void set姓名(String 姓名) {
                this.姓名 = 姓名;
            }

            public String get公民身份号码() {
                return 公民身份号码;
            }

            public void set公民身份号码(String 公民身份号码) {
                this.公民身份号码 = 公民身份号码;
            }

            public String get性别() {
                return 性别;
            }

            public void set性别(String 性别) {
                this.性别 = 性别;
            }

            public String get民族() {
                return 民族;
            }

            public void set民族(String 民族) {
                this.民族 = 民族;
            }

            public String get失效日期() {
                return 失效日期;
            }

            public void set失效日期(String 失效日期) {
                this.失效日期 = 失效日期;
            }

            public String get签发机关() {
                return 签发机关;
            }

            public void set签发机关(String 签发机关) {
                this.签发机关 = 签发机关;
            }

            public String get签发日期() {
                return 签发日期;
            }

            public void set签发日期(String 签发日期) {
                this.签发日期 = 签发日期;
            }
        }
    }
}
