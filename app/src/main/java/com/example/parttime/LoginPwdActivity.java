package com.example.parttime;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lihang.smartloadview.SmartLoadingView;
import com.rxjava.rxlife.RxLife;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.smssdk.SMSSDK;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//密码登录页面
public class LoginPwdActivity extends AppCompatActivity {

    @BindView(R.id.editText_phone)
    EditText editTextPhone;
    @BindView(R.id.editText_pwd)
    EditText editTextPwd;
    @BindView(R.id.button_return)
    ImageButton buttonReturn;
    @BindView(R.id.button_login)
    SmartLoadingView buttonLogin;
    //存储
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editorUP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_pwd);
        ButterKnife.bind(this);
        initActivity();
        Intent intent = getIntent();
        if (intent.getStringExtra("phoneNumber") != null) {
            editTextPhone.setText(intent.getStringExtra("phoneNumber"));
        }
        //密码输入框监听事件
        editTextPwd.addTextChangedListener(editTextPwdTextWatcher);
    }
    //打开免责条款
    public void click_statement(View view){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Url.url_statement)));
    }

    //登录
    public void click_login(View view) {
        if (editTextPhone.getText() == null || editTextPhone.getText().length() != 11) {
            Toasty.warning(this,"请输入正确的手机号",Toasty.LENGTH_SHORT).show();
            return;
        }
        if (editTextPwd.getText() == null || editTextPwd.getText().length() == 0) {
            Toasty.warning(this,"请检查密码输入",Toasty.LENGTH_SHORT).show();
            return;
        }

        login(editTextPhone.getText().toString(), editTextPwd.getText().toString());
    }

    //返回上一页
    public void click_return(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    //忘记密码
    public void click_forgetPassword(View view) {
        Intent intent = new Intent(getApplicationContext(), ForgetPwdActivity.class);
        intent.putExtra("phoneNumber", editTextPhone.getText().toString());
        startActivity(intent);
    }

    //注册
    public void click_registered(View view) {
        startActivity(new Intent(getApplicationContext(), RegisteredActivity.class));
    }

    //监听密码输入ing
    TextWatcher editTextPwdTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            //密码长度大于才可点击
            if (s.toString().trim().length() != 0 && s.toString().length() >= 6 && editTextPhone.getText().length() == 11) {
                buttonLogin.setSmartClickable(true);
            } else {
                buttonLogin.setSmartClickable(false);
            }
        }
    };

    //登录
    private void login(String phone, String pwd) {
        buttonLogin.start();

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getLoginPwdPhoneData)//发送Get请求
                .add("phone", phone.trim())//添加请求参数，该方法可调用多次
                .add("pwd", pwd.trim())
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {

                        loginPhone(phone);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("帐号不存在。")) {
                            Toasty.error(this,"当前用户未注册",Toasty.LENGTH_SHORT).show();
                        } else if (entity_httpResult.getErrorMsg().equals("密码错误。")) {
                            Toasty.error(this,"密码错误",Toasty.LENGTH_SHORT).show();
                        }else {
                            Toasty.warning(this,"暂无任务信息",Toasty.LENGTH_SHORT).show();
                        }
                        buttonLogin.backToStart();
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(this,"登录失败请重试",Toasty.LENGTH_SHORT).show();
                    buttonLogin.backToStart();
                });

    }

    //登录
    private void loginPhone(String phone) {
        buttonLogin.start();

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getLoginPhoneData)//发送Get请求
                .add("phone", phone)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_phoneData entity_phoneData = gson.fromJson(jsonResult, Entity_phoneData.class);
                        //数据存储加页面跳转
                        editorUP.putString(StorageConfig.EDITOR_LOGINSTATE, "1");
                        editorUP.putString(StorageConfig.EDITOR_LOGINID, entity_phoneData.getResult().getDs().get(0).getId());
                        editorUP.putString(StorageConfig.EDITOR_LOGINDATA, jsonResult);
                        editorUP.commit();
                        buttonLogin.onSuccess(new SmartLoadingView.AnimationOKListener() {
                            @Override
                            public void animationOKFinish() {
                                //跳转至主页面
//                                Toasty.success(LoginPwdActivity.this,"登录成功",Toasty.LENGTH_SHORT).show();
                                Toasty.warning(LoginPwdActivity.this,"登录成功",Toasty.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                finish();
                            }
                        });

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("帐号不存在。")) {
                            Toasty.error(this,"当前用户未注册",Toasty.LENGTH_SHORT).show();
                        } else if (entity_httpResult.getErrorMsg().equals("密码错误。")) {
                            Toasty.error(this,"密码错误",Toasty.LENGTH_SHORT).show();
                        }else {
                            Toasty.warning(this,"暂无任务信息",Toasty.LENGTH_SHORT).show();
                        }
                        buttonLogin.backToStart();
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(this,"登录失败请重试",Toasty.LENGTH_SHORT).show();
                    buttonLogin.backToStart();
                });

    }

    //初始化页面工作
    private void initActivity() {
        //状态栏
        ImmersionBar.with(this).statusBarDarkFont(true).fitsSystemWindows(true).statusBarColor(R.color.colorWhite).init();
        //存储
        sharedPreferences = getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        editorUP = sharedPreferences.edit();

    }

}
