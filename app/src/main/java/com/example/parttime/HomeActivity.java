package com.example.parttime;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.bumptech.glide.Glide;
import com.example.parttime.adapter.ViewpagerAdapter;
import com.example.parttime.add.addFragment;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.massage.messageFragment;
import com.example.parttime.my.myFragment;
import com.example.parttime.service.Url;
import com.example.parttime.square.squareFragment;
import com.example.parttime.task.taskFragment;
import com.example.parttime.tool.Dialog_data_work;
import com.example.parttime.tool.Dialog_home_publish;
import com.example.parttime.tool.HelpIM;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.LocationData;
import com.example.parttime.tool.StorageConfig;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.enums.PopupPosition;
import com.lxj.xpopup.interfaces.XPopupCallback;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nl.joery.animatedbottombar.AnimatedBottomBar;

//主页面
public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.AHE_bottom_bar)
    AnimatedBottomBar bottomBar;
    @BindView(R.id.AHE_view_pager)
    ViewPager2 viewPager;
    @BindView(R.id.AHE_floatingActionButton)
    FloatingActionButton AHE_floatingActionButton;
    @BindView(R.id.AHE_constraintLayout_bottomFloatButton)
    ConstraintLayout AHE_constraintLayout_bottomFloatButton;

    //存储
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editorUP;
    //登录态
    private String loginState;
    private String loginData;
    private String RIM_toke;
    //bottombar合集
    private List<Fragment> fragmentList = new ArrayList<>();
    private ViewpagerAdapter viewpagerAdapter;

    //弹窗
    private BasePopupView xPopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        initActivity();

        //状态栏
        ImmersionBar.with(HomeActivity.this).statusBarColor(R.color.colorAppYellow).navigationBarColor(R.color.colorWhite).init();

        //判断是否登录
        Log.v("loginState", loginState);
        if (loginState.equals("0")|| loginState.equals("jump")) {
            //跳转至登录页
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }else {
            //切换fragment(第一种方式)，绑定viewpager和bottombar（可用第二种方式:viewPager.setCurrentItem(seindex);）
            //bottomBar.setupWithViewPager2(viewPager);
            bottomBar.setOnTabSelectListener(onTabSelectListener);//设置监听器
            loginData = sharedPreferences.getString(StorageConfig.EDITOR_LOGINDATA, "0");//获取登陆人信息
            //设置弹出窗口
            if(bottomBar==null){
                bottomBar = findViewById(R.id.AHE_bottom_bar);
            }
            xPopup = new XPopup.Builder(HomeActivity.this)
                    .setPopupCallback(new XPopupCallback() {
                        @Override
                        public void onCreated() {

                        }

                        @Override
                        public void beforeShow() {

                        }

                        @Override
                        public void onShow() {//显示
                            AHE_floatingActionButton.setImageResource(R.drawable.homepushclose);
                        }

                        @Override
                        public void onDismiss() {//隐藏
//                            AHE_floatingActionButton.setImageResource(R.drawable.businesspush);
                            AHE_floatingActionButton.setImageResource(R.drawable.homepushadd);
                        }


                        @Override
                        public boolean onBackPressed() {
                            return false;
                        }
                    })
                    .atView(bottomBar)
                    .isCenterHorizontal(true)
                    .asCustom(new Dialog_home_publish(HomeActivity.this, loginData));
        }

    }

    //弹出发布弹窗
    public void click_openPublish(View view) {

        if (loginState.equals("0") || loginState.equals("jump")) {
            //跳转至登录页
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        } else {

            if(xPopup.isShow()){//如果弹窗已打开则关闭弹窗
                xPopup.dismiss();
            }else {
                //弹出发布窗口
                xPopup.show();
            }

        }

    }

    //底部bottombar监听器
    AnimatedBottomBar.OnTabSelectListener onTabSelectListener = new AnimatedBottomBar.OnTabSelectListener() {
        @Override
        public void onTabSelected(int i, @Nullable AnimatedBottomBar.Tab tab, int seindex, @NotNull AnimatedBottomBar.Tab setab) {

            //点击除广场外的其他tab时检测是否登录
            if (seindex != 0) {
                if (loginState.equals("0") || loginState.equals("jump")) {
                    //跳转至登录页
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                } else if (seindex == 2) {
                    //弹出发布窗口
                    loginData = sharedPreferences.getString(StorageConfig.EDITOR_LOGINDATA, "0");//登陆人信息
                    new XPopup.Builder(HomeActivity.this).atView(bottomBar).isCenterHorizontal(true).asCustom(new Dialog_home_publish(HomeActivity.this, loginData)).show();
                } else {
                    if (seindex == 0) {
                        ImmersionBar.with(HomeActivity.this).statusBarDarkFont(true).statusBarColor(R.color.colorAppYellow).navigationBarColor(R.color.colorWhite).init();
                    } else if (seindex == 1) {
                        ImmersionBar.with(HomeActivity.this).statusBarDarkFont(true).statusBarColor(R.color.colorWhite).navigationBarColor(R.color.colorWhite).init();
                    } else if (seindex == 3) {
                        ImmersionBar.with(HomeActivity.this).statusBarDarkFont(true).statusBarColor(R.color.colorWhite).navigationBarColor(R.color.colorWhite).init();
                    } else if (seindex == 4) {
                        ImmersionBar.with(HomeActivity.this).statusBarDarkFont(true).statusBarColor(R.color.colorAppYellow).navigationBarColor(R.color.colorWhite).init();
                    }
                    //切换fragment(第二种方式,false解决页面切换时中间页面会显示的问题)
                    viewPager.setCurrentItem(seindex,false);
                }
            } else {
                ImmersionBar.with(HomeActivity.this).statusBarColor(R.color.colorAppYellow).navigationBarColor(R.color.colorWhite).init();
                //切换fragment(第二种方式,false解决页面切换时中间页面会显示的问题)
                viewPager.setCurrentItem(seindex,false);
            }
        }

        @Override
        public void onTabReselected(int i, @NotNull AnimatedBottomBar.Tab tab) {

        }
    };

    //初始化页面工作
    private void initActivity() {
        //存储我的文档打我的
        sharedPreferences = getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        editorUP = sharedPreferences.edit();
        loginState = sharedPreferences.getString("loginState", "0");//登录态
        loginData = sharedPreferences.getString(StorageConfig.EDITOR_LOGINDATA, "0");//登陆人信息
        RIM_toke = sharedPreferences.getString(StorageConfig.EDITOR_RIMTOKEN, "0");//融云token

        fragmentList.add(new squareFragment());
        fragmentList.add(new taskFragment());
        fragmentList.add(new addFragment());
        fragmentList.add(new messageFragment());
        fragmentList.add(new myFragment());
        viewpagerAdapter = new ViewpagerAdapter(this, fragmentList);
        viewPager.setAdapter(viewpagerAdapter);
        viewPager.setUserInputEnabled(false);//禁止左右滑动

        //切换fragment(第二种方式)
        viewPager.setOffscreenPageLimit(1);

        //如果已登录则加载融云IM服务
        if (!loginState.equals("0") && !loginState.equals("jump")) {
            Entity_phoneData phoneData = new Gson().fromJson(loginData, Entity_phoneData.class);
            //判断是否有头像
            String userImg = "";
            if (phoneData.getResult().getDs().get(0).getRemark4() != null && !phoneData.getResult().getDs().get(0).getRemark4().equals("")) {
                userImg = Url.url_img + phoneData.getResult().getDs().get(0).getRemark4();
            } else {
                userImg = "https://oa-weixin-test.morimatsu.cn/InstallPackage/img/logo.png";
            }
            //查看是否已经有token
            if (RIM_toke == null || RIM_toke.equals("0")) {
                HelpIM.RIM_init(phoneData.getResult().getDs().get(0).getId(), phoneData.getResult().getDs().get(0).getRemark5(), userImg, this);//注册
            } else {
                HelpIM.RIM_connect(RIM_toke, phoneData.getResult().getDs().get(0).getRemark5(), userImg);//链接
            }
        }

    }

}
