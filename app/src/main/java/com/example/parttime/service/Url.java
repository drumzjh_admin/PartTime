package com.example.parttime.service;

public class Url {
    //身份证图片地址前缀
    public static String url_img_idcard = "http://pointapp.morimatsu.cn:8033/daily/uploads/";
    //图片地址前缀
    public static String url_img = "http://pointapp.morimatsu.cn:8033/daily/";
    //免责条款地址
    public static String url_statement = "http://pointapp.morimatsu.cn:8033/daily/MRLG/xieyi.html";

    //任务是否能接
    public static String url_taskStatus = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/GetJie";

    //根据手机号返回信息（验证码登录）
    public static String url_getLoginPhoneData = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetM_employee";
    //根据微信ID返回信息
    public static String url_getWechatPhoneData = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetM_employeeRemark9";
    //手机号加密码登录
    public static String url_getLoginPwdPhoneData = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/LoginPhone";
    //修改用户密码（忘记密码）
    public static String url_updatePwd = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/UpdatePwd";
    //修改用户密码（修改密码）
    public static String url_resetPwd = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/UpdatePwd3";
    //增加用户修改用户信息
    public static String url_addUploadUser = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/AddM_employee";
    //增加用户信息(微信登录用)
    public static String url_addUploadUser_wechat = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/AddM_employeeOPEN";
    //获取押金和金额
    public static String url_getMoney = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetListbalanceemployee_list_id";
    //获取押金明细
    public static String url_getMortgageList = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetListMarginemployee_list_id";
    //反馈
    public static String url_submitFeedBack = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/AddM_opinion";
    //身份照片验证
    public static String url_addPeopleCard = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GeneralBasicDemo";
    //执照照片验证
//    public static String url_addCompanyCard = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/SaveImgYYZZ";
    public static String url_addCompanyCard = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/SaveImgYYZZ_ai";
    //充值1
    public static String url_addMoneyOne = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/AddListRoundemployee_list_id";
    //充值2
    public static String url_addMoneyTwo = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/AddListRoundemployee_list_id_WeiXing";
    //提现
    public static String url_extractMoney = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetListbalance";
    //返回充值明细
    public static String url_addMoneyDetail = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetListRoundByemployee_id";

    //返回提现明细
    public static String url_takeMoneyDetail = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetListCashOutByemployee_id";
    //押金余额转移
    public static String url_moneyTransfer = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/GetListM_margin_detailmployee_list_idIn";
    //修改银行卡号
    public static String url_uploadBankCard = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/login.asmx/UpdateBankCard";

    //加载任务广场
    public static String url_getTaskList = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/GetListtask_list";
    //增加任务广场
    public static String url_addTaskList = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/AddM_task_list";
    //加载筛选类型数据
    public static String url_getTaskType = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/GetListZidian";
    //上传任务视频和封面
    public static String url_addTaskVideo = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/SaveVideoHttp";
    public static String url_addTaskVideoImg = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/SaveImgHttp";
    //删除草稿箱
    public static String url_deleteTaskDraft = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/Task.asmx/DeleteM_task_list ";

    //获取工作经历
    public static String url_getWorkList = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/GetM_works";
    //修改工作经历
    public static String url_modifyWork = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/EditM_works";
    //删除工作经历
    public static String url_clearWork = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/DelM_works";
    //增加工作经历
    public static String url_addWork = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/AddM_works";
    //接受任务啦
    public static String url_acceptTask = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/AddM_OrderTakingList";
    //已接任务列表
    public static String url_acceptTaskList = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/GetM_OrderTakingList";
    //返回接单人列表
    public static String url_getAcceptPeople = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/GetM_OrderTakingListByTaskId";
    //返回收藏列表
    public static String url_getCollectionList = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/GetM_Collect";
    //增加收藏
    public static String url_addCollection = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/AddM_Collect";
    //取消收藏
    public static String url_cancelCollection = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/CancelM_Collect";
    //查看收藏
    public static String url_checkCollection = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/ExistsM_Collect";
    //取消任务
    public static String url_cancelTask = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/SubFace.asmx/ExistsM_Collect";

    //获取签到状态
    public static String url_getTasksign = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/M_check.asmx/GetM_check";
    //签到
    public static String url_addTasksign = "http://pointapp.morimatsu.cn:8033/daily/MRLG/InterFace/M_check.asmx/SetM_check";



}