package com.example.parttime.tool;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.airbnb.lottie.LottieAnimationView;
import com.example.parttime.R;
import com.example.parttime.add.CompanyPublishActivity;
import com.example.parttime.add.PeoplePublishActivity;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.my.MyCertificationActivity;
import com.google.gson.Gson;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.AttachPopupView;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.impl.PartShadowPopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;

import es.dmoral.toasty.Toasty;

//发布按钮弹窗(自定义局部阴影弹窗)
public class Dialog_home_publish extends PartShadowPopupView {

    ImageView DHP_igb_personal;
    ImageView DHP_igb_company;
    Dialog_home_publish thisView;

    //个人的信息
    private Entity_phoneData phoneData;

    public Dialog_home_publish(@NonNull Context context,String peopleData) {
        super(context);
        //设置个人信息
        phoneData = new Gson().fromJson(peopleData, Entity_phoneData.class);
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.dialog_home_publish;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        thisView = this;
        DHP_igb_personal = findViewById(R.id.DHP_igb_personal);
        DHP_igb_company = findViewById(R.id.DHP_igb_company);

        DHP_igb_personal.setOnClickListener(onClickListenerPersonal);
        DHP_igb_company.setOnClickListener(onClickListenerCompany);

    }

    OnClickListener onClickListenerPersonal = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(phoneData.getResult().getDs().get(0).getRemark8() != null && !phoneData.getResult().getDs().get(0).getCardURl().equals("")){//已认证
//                if(phoneData.getResult().getDs().get(0).getCardURl().equals("1")){
//                    getContext().startActivity(new Intent(getContext(), PeoplePublishActivity.class));
//                }else {
//                    Toasty.warning(getContext(), "请先前往个人中心进行个人认证", Toasty.LENGTH_SHORT).show();
//                }
                if(phoneData.getResult().getDs().get(0).getRemark8().equals("1")){
                    getContext().startActivity(new Intent(getContext(), PeoplePublishActivity.class));
                }else {
                    Toasty.warning(getContext(), "请先前往个人中心进行个人认证", Toasty.LENGTH_SHORT).show();
                }
            }else {
                Toasty.warning(getContext(), "请先前往个人中心进行个人认证", Toasty.LENGTH_SHORT).show();
            }
        }
    };

    OnClickListener onClickListenerCompany = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(phoneData.getResult().getDs().get(0).getRemark6() != null && !phoneData.getResult().getDs().get(0).getRemark6().equals("")){//已认证
                getContext().startActivity(new Intent(getContext(), CompanyPublishActivity.class));
            }else {
                new XPopup.Builder(thisView.getContext()).asConfirm("提示", "公司认证成功后才可进行此操作，是否前往公司认证","取消","确认", new OnConfirmListener() {
                    @Override
                    public void onConfirm() {
//                    SharedPreferences sharedPreferences = getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editorUP = sharedPreferences.edit();
//                    editorUP.clear();
//                    editorUP.commit();
//                        thisView.getContext().startActivity();

                        thisView.getContext().startActivity(new Intent(thisView.getContext(),
                                MyCertificationActivity.class).putExtra("phoneData",phoneData));
//                    finish();
                    }
                },null,false).show();
//            Toasty.warning(getActivity(), "请先前往个人中心进行公司认证", Toasty.LENGTH_SHORT).show();
//                Toasty.warning(getContext(), "请先前往个人中心进行公司认证", Toasty.LENGTH_SHORT).show();

            }
        }
    };

}
