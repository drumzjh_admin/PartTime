package com.example.parttime.tool;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.example.parttime.R;
import com.loper7.layout.DateTimePicker;
import com.loper7.layout.StringUtils;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;

import es.dmoral.toasty.Toasty;

//工作经历添加弹出框
public class Dialog_data_work extends BottomPopupView {

    EditText DDW_editTextText_name;
    DateTimePicker DDW_dateTimePicker_start;
    DateTimePicker DDW_dateTimePicker_end;
    Button DDW_button_cancel;
    Button DDW_button_add;

    String nameText = "";
    String timeDateStart = "";
    String timeDateEnd = "";

    public static OnClickListenerAdd onClickListenerAdd;//监听器对象

    //监听器接口
    public interface OnClickListenerAdd {
        void onClick(String nameText, String start, String end);//单击事件处理接口
    }

    //监听器实现
    public void setOnClickListenerAdd(OnClickListenerAdd onClickListenerAdd) {
        this.onClickListenerAdd = onClickListenerAdd;
    }

    public Dialog_data_work(@NonNull Context context, String nameText, String start, String end) {
        super(context);
        this.nameText = nameText;
        this.timeDateStart = start;
        this.timeDateEnd = end;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.dialog_data_work;
    }

    // 最大高度为Window的0.85
//    @Override
//    protected int getMaxHeight() {
//        return (int) (XPopupUtils.getWindowHeight(getContext()) * .85f);
//    }

    @Override
    protected void onCreate() {
        super.onCreate();

        DDW_editTextText_name = findViewById(R.id.DDW_editTextText_name);
        DDW_dateTimePicker_start = findViewById(R.id.DDW_dateTimePicker_start);
        DDW_dateTimePicker_end = findViewById(R.id.DDW_dateTimePicker_end);
        DDW_button_cancel = findViewById(R.id.DDW_button_cancel);
        DDW_button_add = findViewById(R.id.DDW_button_add);

        //设置显示类型
        int[] intType = new int[]{DateTimePicker.YEAR, DateTimePicker.MONTH, DateTimePicker.DAY};
        DDW_dateTimePicker_start.setDisplayType(intType);
        DDW_dateTimePicker_end.setDisplayType(intType);
        //设置默认选中
        if(timeDateStart !=null && !timeDateStart.equals("")){
            DDW_dateTimePicker_start.setDefaultMillisecond(HelpTool.getDateTimeLong(timeDateStart));
        }else {
            DDW_dateTimePicker_start.setDefaultMillisecond(System.currentTimeMillis());
        }
        if(timeDateEnd !=null && !timeDateEnd.equals("")){
            DDW_dateTimePicker_end.setDefaultMillisecond(HelpTool.getDateTimeLong(timeDateEnd));
        }else {
            DDW_dateTimePicker_end.setDefaultMillisecond(System.currentTimeMillis());
        }
        //设置最小选中
//        DDW_dateTimePicker_start.setMinMillisecond(System.currentTimeMillis());
//        DDW_dateTimePicker_end.setMinMillisecond(System.currentTimeMillis());
        //设置最大选中
        DDW_dateTimePicker_start.setMaxMillisecond(System.currentTimeMillis());
        DDW_dateTimePicker_end.setMaxMillisecond(System.currentTimeMillis());

        //设置事件监听
        DDW_button_add.setOnClickListener(onClickListener_Add);
        DDW_button_cancel.setOnClickListener(onClickListener_Cancel);
        DDW_dateTimePicker_start.setOnDateTimeChangedListener(onDateTimeChangedListenerStart);
        DDW_dateTimePicker_end.setOnDateTimeChangedListener(onDateTimeChangedListenerEnd);

    }

    OnClickListener onClickListener_Cancel = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //隐藏这个弹窗
            dismiss();
        }
    };

    OnClickListener onClickListener_Add = new OnClickListener() {
        @Override
        public void onClick(View v) {
            nameText = DDW_editTextText_name.getText().toString();

            if (nameText != null && !nameText.equals("")) {
                if (!timeDateStart.equals("")) {
                    if (!timeDateEnd.equals("")) {
                        Log.v("DDW_editTextText_name", nameText);
                        Log.v("timeDateStart", timeDateStart);
                        Log.v("timeDateEnd", timeDateEnd);
                        if (onClickListenerAdd != null) {
                            //往外传递参数
                            onClickListenerAdd.onClick(nameText, timeDateStart, timeDateEnd);
                            dismiss();
                        }
                    } else {
                        Toasty.warning(getContext(), "请选择结束时间", Toasty.LENGTH_SHORT).show();
                    }
                } else {
                    Toasty.warning(getContext(), "请选择开始时间", Toasty.LENGTH_SHORT).show();
                }

            } else {
                Toasty.warning(getContext(), "请输入工作内容", Toasty.LENGTH_SHORT).show();
            }
        }
    };

    DateTimePicker.OnDateTimeChangedListener onDateTimeChangedListenerStart = new DateTimePicker.OnDateTimeChangedListener() {
        @Override
        public void onDateTimeChanged(DateTimePicker view, long millisecond, int year, int month, int day, int hour, int minute) {
            timeDateStart = StringUtils.conversionTime(millisecond, "yyyy/MM/dd");
        }
    };

    DateTimePicker.OnDateTimeChangedListener onDateTimeChangedListenerEnd = new DateTimePicker.OnDateTimeChangedListener() {
        @Override
        public void onDateTimeChanged(DateTimePicker view, long millisecond, int year, int month, int day, int hour, int minute) {
            timeDateEnd = StringUtils.conversionTime(millisecond, "yyyy/MM/dd");
        }
    };

}
