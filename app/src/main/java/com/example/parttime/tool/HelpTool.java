package com.example.parttime.tool;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.airbnb.lottie.animation.content.Content;
import com.example.parttime.entity.Entity_rimData;
import com.example.parttime.service.Url;
import com.google.gson.Gson;
import com.rxjava.rxlife.RxLife;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//集成方法工具类
public class HelpTool {

    //解析xml
    public static String[] ParseHttpResult(String httpResult) {

        //获取json
        int firstIndex = httpResult.indexOf("{");
        int lastIndex = httpResult.lastIndexOf("}");

        httpResult = httpResult.substring(firstIndex, lastIndex + 1);
        //Log.v("httpResult", httpResult);

        //获取states
        int indexState = httpResult.indexOf("states");
        String states = httpResult.substring(indexState + 9, indexState + 10);

        String[] httpResultStates = new String[2];
        httpResultStates[0] = states;
        httpResultStates[1] = httpResult;

        return httpResultStates;
    }

    //单个权限信息管理
    public static boolean permissionData(Context context, String perm) {
        final boolean[] isOK = {false};
        //权限管理
        RxPermissions rxPermissions = new RxPermissions((FragmentActivity) context);
        rxPermissions.request(perm)
                .subscribe(permission -> {
                    if (permission) {//权限被授予
                        Log.v("permission", "权限被允许");
                        isOK[0] = true;
                    } else {//权限被禁止
                        Log.v("permission", "权限被禁止");

                    }
                });
        return isOK[0];
    }

    //权限信息管理
    public static void permissionGroupData(Context context) {
        //权限管理
        RxPermissions rxPermissions = new RxPermissions((FragmentActivity) context);
        rxPermissions.requestEach(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA)
                .subscribe(permission -> {
                    if (permission.granted) {//权限被授予
                        Log.v("permission", permission.name + "被允许");
                    } else if (permission.shouldShowRequestPermissionRationale) {

                    } else {//权限被禁止
                        Log.v("permission", permission.name + "被禁止");
                        Toast.makeText(context, "权限[" + permission.name + "]被禁止，部分功能不可用，请前往设置中打开", Toast.LENGTH_LONG).show();
                    }
                });
    }

    //获取触摸位置
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;

    }

    //打开关闭软键盘
    public static void showhideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            if (activity.getCurrentFocus()!=null && activity.getCurrentFocus().getWindowToken() != null) {
                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    //获取当前时间
    public static String getDateTime(){
        //获取当前时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        String str = formatter.format(curDate);

        return  str;
    }

    //将时间转换为时间戳
    public static long getDateTimeLong(String dateTimes){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
            Date date = new Date();
            try{
                date = simpleDateFormat.parse(dateTimes);
            } catch(ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return date.getTime();
    }

    //设置多条筛选数据(类型)
    public static List<FiltrateBean> initFlowPopDataType(List<FiltrateBean> filtrateBeanList, String[] typeArray) {

        FiltrateBean fb1 = new FiltrateBean();
        fb1.setTypeName("全部");
        List<FiltrateBean.Children> childrenList = new ArrayList<>();
        for (int x = 0; x < typeArray.length; x++) {
            FiltrateBean.Children cd = new FiltrateBean.Children();
            cd.setValue(typeArray[x]);
            childrenList.add(cd);
        }

        fb1.setChildren(childrenList);
        filtrateBeanList.add(fb1);

        return filtrateBeanList;

    }

    //设置多条筛选数据(要求)
    public static List<FiltrateBean> initFlowPopDataRequirements(List<FiltrateBean> filtrateBeanList) {

        String[] schoolingArray = StorageConfig.schoolingArray;//学历
        String[] ageArray = StorageConfig.ageArray;//年龄
        String[] sexArray = StorageConfig.sexArray;//性别

        FiltrateBean fb1 = new FiltrateBean();
        fb1.setTypeName("性别");
        List<FiltrateBean.Children> childrenList = new ArrayList<>();
        for (int x = 0; x < sexArray.length; x++) {
            FiltrateBean.Children cd = new FiltrateBean.Children();
            cd.setValue(sexArray[x]);
            childrenList.add(cd);
        }
        fb1.setChildren(childrenList);

        FiltrateBean fb2 = new FiltrateBean();
        fb2.setTypeName("年龄");
        List<FiltrateBean.Children> childrenList2 = new ArrayList<>();
        for (int x = 0; x < ageArray.length; x++) {
            FiltrateBean.Children cd = new FiltrateBean.Children();
            cd.setValue(ageArray[x]);
            childrenList2.add(cd);
        }
        fb2.setChildren(childrenList2);

        FiltrateBean fb3 = new FiltrateBean();
        fb3.setTypeName("学历");
        List<FiltrateBean.Children> childrenList3 = new ArrayList<>();
        for (int x = 0; x < schoolingArray.length; x++) {
            FiltrateBean.Children cd = new FiltrateBean.Children();
            cd.setValue(schoolingArray[x]);
            childrenList3.add(cd);
        }
        fb3.setChildren(childrenList3);

        filtrateBeanList.add(fb1);
        filtrateBeanList.add(fb2);
        filtrateBeanList.add(fb3);

        return filtrateBeanList;

    }

    //取出字符串
    public static String getSharedPreferenceString(String key,Context context) {
        String regularEx = "#";
        String[] str = null;
        SharedPreferences sp = context.getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        String values;
        values = sp.getString(key, "");
        return values;
    }

    //保存字符串
    public static void setSharedPreferenceString(String key, String values,Context context) {
        String regularEx = "#";
        String str = values;
        SharedPreferences sp = context.getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor et = sp.edit();
        et.putString(key, str);
        et.commit();
    }

    //取出字符串数组
    public static String[] getSharedPreferenceArray(String key,Context context) {
        String regularEx = "#";
        String[] str = null;
        SharedPreferences sp = context.getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        String values;
        values = sp.getString(key, "");
        str = values.split(regularEx);

        return str;
    }

    //保存字符串数组
    public static void setSharedPreferenceArray(String key, String[] values,Context context) {
        String regularEx = "#";
        String str = "";
        SharedPreferences sp = context.getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
        if (values != null && values.length > 0) {
            for (String value : values) {
                str += value;
                str += regularEx;
            }
            SharedPreferences.Editor et = sp.edit();
            et.putString(key, str);
            et.commit();
        }
    }

    //获取视频地址
    public static String getFilePath(Context context, Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String realPath = null;
        if (scheme == null)
            realPath = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            realPath = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = context.getContentResolver().query(uri,
                    new String[]{MediaStore.Images.ImageColumns.DATA},
                    null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
                    if (index > -1) {
                        realPath = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        if (TextUtils.isEmpty(realPath)) {
            if (uri != null) {
                String uriString = uri.toString();
                int index = uriString.lastIndexOf("/");
                String imageName = uriString.substring(index);

                File storageDir;
                storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File file = new File(storageDir, imageName);
                if (file.exists()) {
                    realPath = file.getAbsolutePath();
                } else {
                    storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    File file1 = new File(storageDir, imageName);
                    realPath = file1.getAbsolutePath();
                }
            }
        }
        return Uri.decode(realPath);
    }

    //uri转url
    public static String getRealFilePath( final Context context, final Uri uri ) {
        if ( null == uri ) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if ( scheme == null )
            data = uri.getPath();
        else if ( ContentResolver.SCHEME_FILE.equals( scheme ) ) {
            data = uri.getPath();
        } else if ( ContentResolver.SCHEME_CONTENT.equals( scheme ) ) {
            Cursor cursor = context.getContentResolver().query( uri, new String[] { MediaStore.Images.ImageColumns.DATA }, null, null, null );
            if ( null != cursor ) {
                if ( cursor.moveToFirst() ) {
                    int index = cursor.getColumnIndex( MediaStore.Images.ImageColumns.DATA );
                    if ( index > -1 ) {
                        data = cursor.getString( index );
                    }
                }
                cursor.close();
            }
        }
        return data;
    }

    //计算sha1值
    public static String getsha1(String data) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(data.getBytes());
        StringBuffer buf = new StringBuffer();
        byte[] bits = md.digest();
        for (int i = 0; i < bits.length; i++) {
            int a = bits[i];
            if (a < 0) a += 256;
            if (a < 16) buf.append("0");
            buf.append(Integer.toHexString(a));
        }
        return buf.toString();
    }

    //计算经纬度之间的距离
    /**
     * @param longitude1 某个加油站的经度
     * @param latitude1  某个加油站的纬度
     * @param longitude2 用户当前位置的经度
     * @param latitude2  用户当前位置的纬度
     * @return 单位：千米/km 保留一位小数
     */
    private static final double EARTH_RADIUS = 6378.137;// 地球赤道半径
    private static final double PI = 3.14159265;//祖率

    public static String getDistance(double longitude1, double latitude1,
                                     double longitude2, double latitude2) {
        double Lat1 = rad(latitude1);
        double Lat2 = rad(latitude2);
        double a = Lat1 - Lat2;
        double b = rad(longitude1) - rad(longitude2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                + Math.cos(Lat1) * Math.cos(Lat2)
                * Math.pow(Math.sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        //有小数的情况;注意这里的10000d中的“d”
        s = Math.round(s * 10000d) / 10000d;
        s = s * 1000;//单位：米
        //s = Math.round(s/0.1d) /10000d   ;//单位：千米 保留两位小数
        s = Math.round(s / 100d) / 10d;//单位：千米 保留一位小数
        return String.valueOf(s);
    }
    /**
     * π是弧度制，180度是角度制，d*π／180,表示每角度等于多少弧度。
     *
     * @param d
     * @return
     */
    private static double rad(double d) {
        return d * PI / 180.0;
    }

    //检测应用是否安装
    public static boolean isAvilible(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();//获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);//获取所有已安装程序的包信息
        List<String> pName = new ArrayList<String>();//用于存储所有已安装程序的包名
        //从pinfo中将包名字逐一取出，压入pName list中
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                pName.add(pn);
            }
        }
        return pName.contains(packageName);//判断pName中是否有目标程序的包名，有TRUE，没有FALSE
    }

}
