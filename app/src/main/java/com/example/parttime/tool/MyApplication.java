package com.example.parttime.tool;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.example.parttime.StartActivity;
import com.mob.MobSDK;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.BuglyLog;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import io.rong.imkit.RongIM;

//APP初始化工具类
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //注册微信支付
        IWXAPI msgApi = WXAPIFactory.createWXAPI(getApplicationContext(),null);
        msgApi.registerApp("wx8748cf377b6a0bf2");

        //注册MOB隐私服务
        MobSDK.submitPolicyGrantResult(true, null);

        /**
         * 第三个参数为SDK调试模式开关，调试模式的行为特性如下：
         * 输出详细的Bugly SDK的Log；
         * 每一条Crash都会被立即上报；
         * 自定义日志将会在Logcat中输出。
         * 建议在测试阶段建议设置成true，发布时设置为false。
         * */

        //设置为只在主进程上传异常数据
        String packageName = getApplicationContext().getPackageName();//当前包名
        String processName = getCurProcessName(getApplicationContext());//当前进程名
        //设置是否为上报进程
        CrashReport.UserStrategy userStrategy  = new CrashReport.UserStrategy(getApplicationContext());
        userStrategy.setBuglyLogUpload(processName==null||processName.equals(packageName));
        //初始化bug日志工具
        //CrashReport.initCrashReport(getApplicationContext(), "4eef70a1f5", true);
        Bugly.init(getApplicationContext(),"4eef70a1f5",true);//(APP升级版初始化)

        /**
         * OnCreate 会被多个进程重入，这段保护代码，确保只有您需要使用 RongIM 的进程和 Push 进程执行了 init。
         * io.rong.push 为融云 push 进程名称，不可修改。
         */
        if (getApplicationInfo().packageName.equals(getCurProcessName(getApplicationContext())) ||
                "io.rong.push".equals(getCurProcessName(getApplicationContext()))) {

            /**
             * <meta-data
             android:name="RONG_CLOUD_APP_KEY"
             android:value="25wehl3u20b2w"/>
             * IMKit SDK调用第一步 初始化
             */
            RongIM.init(this, "25wehl3u20b2w", false);

        }


    }

    /**
     * 获得当前进程的名字
     *
     * @param context
     * @return 进程号
     */
    public static String getCurProcessName(Context context) {

        int pid = android.os.Process.myPid();

        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);

        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {

            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

}
