package com.example.parttime.tool;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.example.parttime.entity.Entity_rimData;
import com.google.gson.Gson;

import java.util.Random;

import io.rong.imkit.RongIM;
import io.rong.imkit.userInfoCache.RongUserInfoManager;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.UserInfo;
import rxhttp.wrapper.param.RxHttp;

//融云IM聊天工具类
public class HelpIM {

    //注册融云IM聊天
    public static void RIM_init(String userid, String username, String userimg, Context context){

        String key = "25wehl3u20b2w";
        String nonce = String.valueOf(new Random().nextInt(1000000));//随机数
        String time = String.valueOf(System.currentTimeMillis());//时间戳
        String sign = HelpTool.getsha1("5VpkWfqIUr" + nonce + time);//签名
        Log.v("随机数",nonce);
        Log.v("时间戳",time);
        Log.v("合并",key + nonce + time);
        Log.v("签名",sign);

        RxHttp.setDebug(true);
        RxHttp.postForm("https://api-cn.ronghub.com/user/getToken.json")//发送Get请求（获取使用中的任务）
                .addHeader("App-Key", key)
                .addHeader("Nonce", nonce)
                .addHeader("Timestamp", time)
                .addHeader("Signature", sign)
                .add("userId", userid)
                .add("name", username)
                //.add("portraitUri", "")
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    Log.v("数据",json);

                    Entity_rimData entity_rimData = new Entity_rimData();
                    entity_rimData = new Gson().fromJson(json, Entity_rimData.class);
                    //存储token
                    HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_RIMTOKEN,entity_rimData.getToken(),context);
                    //链接融云服务
                    RIM_connect(entity_rimData.getToken(),username,userimg);

                }, throwable -> {

                });

    }

    //链接融云IM服务
    public static void RIM_connect(String token,String username, String userimg){
        RongIM.connect(token, new RongIMClient.ConnectCallback() {
            /**
             * 数据库回调.
             * @param databaseOpenStatus 数据库打开状态. DATABASE_OPEN_SUCCESS 数据库打开成功; DATABASE_OPEN_ERROR 数据库打开失败
             */
            @Override
            public void onDatabaseOpened(RongIMClient.DatabaseOpenStatus databaseOpenStatus) {
                Log.v("链接", databaseOpenStatus.toString());
            }
            /**
             * 成功回调
             * @param userId 当前用户 ID
             */
            @Override
            public void onSuccess(String userId) {
                Log.v("链接", "成功");
                RIM_setUser(username,userimg);//设置当前登陆人信息
            }

            /**
             * 错误回调
             * @param connectionErrorCode 错误码
             */
            @Override
            public void onError(RongIMClient.ConnectionErrorCode connectionErrorCode) {
                Log.v("链接", connectionErrorCode.toString());
            }
        });
    }

    //设置用户信息
    public static void RIM_setUser(String name,String urlImg){

        RongIM.setUserInfoProvider(new RongIM.UserInfoProvider() {

            /**
             * 获取设置用户信息. 通过返回的 userId 来封装生产用户信息.
             * @param userId 用户 ID
             * userId 对应的名称
             * userId 对应的头像地址
             */
            @Override
            public UserInfo getUserInfo(String userId) {
                UserInfo userInfo;
                if(RongUserInfoManager.getInstance().getUserInfo(userId) == null){
                    userInfo = new UserInfo(userId, name, Uri.parse(urlImg));
                }else {
                    userInfo =RongUserInfoManager.getInstance().getUserInfo(userId);
                }

                return userInfo;
            }

        }, true);

    }

    //获取用户信息
    public static UserInfo RIM_getUser(String userId){

        UserInfo userInfo = RongUserInfoManager.getInstance().getUserInfo(userId);
        return userInfo;

    }

    //更新用户信息
    public static void RIM_upldataUser(String userId,String name,String urlImg){

        UserInfo userInfo = new UserInfo(userId, name, Uri.parse(urlImg));
        RongIM.getInstance().refreshUserInfoCache(userInfo);

    }

    //删除用户信息
    public static void RIM_clearUser(){

        RongUserInfoManager.getInstance().uninit();

    }


}
