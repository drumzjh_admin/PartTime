package com.example.parttime.tool;


import java.io.Serializable;

//位置信息对象类
public class LocationData implements Serializable {
    private String Latitude;//纬度
    private String Longitude;//经度
    private String Accuracy;//定位精度 单位:米
    private String Altitude;//海拔高度信息
    private String Speed;//速度米/秒
    private String Bearing;//方向角信息
    private String BuildingId;//室内定位建筑物Id
    private String Floor;//室内定位楼层
    private String Address;//地址描述
    private String Country;//国家
    private String Province;//省
    private String City;//城市
    private String District;//城区
    private String Street;//街道
    private String StreetNum;//街道门牌号
    private String CityCode;//城市编码
    private String AdCode;//区域编码
    private String PoiName;//当前位置POI名称
    private String AoiName;//当前位置所处AOI名称
    private String GpsAccuracyStatus;//设备当前GPS状态
    private String LocationType;//定位结果来源
    private String LocationDetail;//定位信息描述
    private String ErrorInfo;//定位错误信息描述

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(String accuracy) {
        Accuracy = accuracy;
    }

    public String getAltitude() {
        return Altitude;
    }

    public void setAltitude(String altitude) {
        Altitude = altitude;
    }

    public String getSpeed() {
        return Speed;
    }

    public void setSpeed(String speed) {
        Speed = speed;
    }

    public String getBearing() {
        return Bearing;
    }

    public void setBearing(String bearing) {
        Bearing = bearing;
    }

    public String getBuildingId() {
        return BuildingId;
    }

    public void setBuildingId(String buildingId) {
        BuildingId = buildingId;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getStreetNum() {
        return StreetNum;
    }

    public void setStreetNum(String streetNum) {
        StreetNum = streetNum;
    }

    public String getCityCode() {
        return CityCode;
    }

    public void setCityCode(String cityCode) {
        CityCode = cityCode;
    }

    public String getAdCode() {
        return AdCode;
    }

    public void setAdCode(String adCode) {
        AdCode = adCode;
    }

    public String getPoiName() {
        return PoiName;
    }

    public void setPoiName(String poiName) {
        PoiName = poiName;
    }

    public String getAoiName() {
        return AoiName;
    }

    public void setAoiName(String aoiName) {
        AoiName = aoiName;
    }

    public String getGpsAccuracyStatus() {
        return GpsAccuracyStatus;
    }

    public void setGpsAccuracyStatus(String gpsAccuracyStatus) {
        GpsAccuracyStatus = gpsAccuracyStatus;
    }

    public String getLocationType() {
        return LocationType;
    }

    public void setLocationType(String locationType) {
        LocationType = locationType;
    }

    public String getLocationDetail() {
        return LocationDetail;
    }

    public void setLocationDetail(String locationDetail) {
        LocationDetail = locationDetail;
    }

    public String getErrorInfo() {
        return ErrorInfo;
    }

    public void setErrorInfo(String errorInfo) {
        ErrorInfo = errorInfo;
    }
}
