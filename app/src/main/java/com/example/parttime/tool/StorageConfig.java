package com.example.parttime.tool;

//软件数据存储
public class StorageConfig {
    /*
    * 缓存中的key
    * loginState : 登录态
    * loginData : 登录人信息
    * */
    public static String STORAGE_DATA = "STORAGE_DATA";

    public static String EDITOR_LOGINSTATE = "loginState";//登录态
    public static String EDITOR_LOGINID = "EDITOR_LOGINID";//登录人id
    public static String EDITOR_LOGINDATA = "loginData";//登录人信息
    public static String EDITOR_LOGINMONEY = "EDITOR_LOGINMONEY";//登录人账户金额信息
    public static String EDITOR_TYPE = "EDITOR_TYPE";//任务类型
    public static String EDITOR_TYPEID = "EDITOR_TYPEID";//任务类型ID
    public static String EDITOR_RIMTOKEN = "EDITOR_RIMTOKEN";//融云聊天的TOKEN
    public static String EDITOR_LOCATIONJD = "EDITOR_LOCATIONJD";//经度
    public static String EDITOR_LOCATIONWD = "EDITOR_LOCATIONWD";//纬度
    public static String EDITOR_LOCATIONMZ = "EDITOR_LOCATIONMZ";//位置

    public static String EDITOR_TASKID_JXZ = "EDITOR_TASKID_JXZ";//进行中的任务ID列表

    public static String[] sexArray = {"男","女","不限"};//性别
    public static String[] sexArray2 = {"男","女","保密"};//性别
    public static String[] ageArray = {"18~22岁", "23~28岁", "29~30岁", "31~35岁", "36~40岁", "41~45岁", "46~50岁", "不限"};//年龄
    public static String[] schoolingArray = {"小学", "初中", "高中", "大专", "本科", "本科以上", "不限"};//学历
    public static String[] locationArray = {"黄浦区","徐汇区","长宁区","静安区","普陀区","虹口区","杨浦区","闵行区","宝山区","嘉定区","浦东新区","金山区","松江区","青浦区","奉贤区"};//地区


}
