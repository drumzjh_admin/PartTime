package com.example.parttime;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Toast;

import com.example.parttime.adapter.MyAcceptAdapter;
import com.example.parttime.task.MyAcceptActivity;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.gyf.immersionbar.ImmersionBar;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zbar.ZBarView;
import es.dmoral.toasty.Toasty;

//扫码识别界面
public class ScanCodeActivity extends AppCompatActivity implements QRCodeView.Delegate {

    QRCodeView SC_scanZbar;

    String[] jxzTask;//存储进行中的任务ID

    private boolean lampOpenOrClose = true;//控制闪光灯

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_code);
        initActivity();

        SC_scanZbar.startCamera();
        SC_scanZbar.showScanRect();
        SC_scanZbar.startSpot();
    }

    //扫码成功处理
    @Override
    public void onScanQRCodeSuccess(String result) {

        //震动
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(200);

        //判断是否为任务广场扫码
        if (getIntent().getStringExtra("squareScanButton") != null && getIntent().getStringExtra("squareScanButton").equals("1")) {

            jxzTask = HelpTool.getSharedPreferenceArray(StorageConfig.EDITOR_TASKID_JXZ, this);//已结任务的ID数组
            if (jxzTask != null && jxzTask.length > 0) {

                //循环判断当前扫描二维码数据是否在进行中任务
                String taskId = "";
                for (int i = 0; i < jxzTask.length; i++) {
                    if (jxzTask[i].equals(result)) {
                        taskId = jxzTask[i];
                        break;
                    }
                }

                if (!taskId.equals("")) {
                    startActivity(new Intent(this, MyAcceptActivity.class).putExtra("scanresult", result).putExtra("taskId", taskId));
                    finish();
                } else {
                    Toasty.warning(getApplicationContext(), "不可签到，扫描任务不在已进行任务中", Toasty.LENGTH_SHORT).show();
                    finish();
                }

            } else {
                Toasty.warning(getApplicationContext(), "不可签到，扫描任务不已在进行任务中", Toasty.LENGTH_SHORT).show();
                finish();
            }

        } else {
            //传值至上一页面(MyAcceptActivity)
            setResult(RESULT_OK, new Intent().putExtra("scanCode", result));
            finish();
        }
    }

    @Override
    public void onCameraAmbientBrightnessChanged(boolean isDark) {

    }

    //扫码失败处理
    @Override
    public void onScanQRCodeOpenCameraError() {
        Toasty.warning(getApplicationContext(), "初始化扫码失败请重试", Toasty.LENGTH_SHORT).show();
        SC_scanZbar.startSpot();
    }

    //打开闪光灯
    public void click_openLamp(View view) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//安卓版本大于7.0

            CameraManager cameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
            //获取当前手机所有摄像头ID
            try {
                String[] cameraIds = cameraManager.getCameraIdList();

                for (String id : cameraIds) {
                    CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(id);
                    //查询当前摄像头是否包含闪光灯
                    Boolean isLamp = cameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                    Integer lensFacing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                    if (isLamp != null && isLamp && lensFacing != null && lensFacing == CameraCharacteristics.LENS_FACING_BACK) {

                        //打开或关闭手电筒
                        cameraManager.setTorchMode(id, lampOpenOrClose);

                        if (lampOpenOrClose) {
                            lampOpenOrClose = false;
                        } else {
                            lampOpenOrClose = true;
                        }

                    }
                }

            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

        } else {//安卓版本小于7.0

            android.hardware.Camera camera = android.hardware.Camera.open();
            Camera.Parameters parameters = camera.getParameters();

            if (lampOpenOrClose) {

                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(parameters);
                lampOpenOrClose = false;

            } else {

                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(parameters);
                lampOpenOrClose = true;

            }

        }

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).init();

        //页面控件绑定
        SC_scanZbar = this.findViewById(R.id.SC_scanZbar);
        //设置代理
        SC_scanZbar.setDelegate(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        SC_scanZbar.stopCamera();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SC_scanZbar.onDestroy();
    }

}