package com.example.parttime.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_taskManagement;

import org.jetbrains.annotations.NotNull;

import java.util.List;
//个人收藏适配器
public class MyCollectionAdapter extends BaseQuickAdapter<Entity_taskManagement.ResultBean.DsBean, BaseViewHolder> {

    public MyCollectionAdapter(int layoutResId) {
        super(layoutResId);
    }
    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_taskManagement.ResultBean.DsBean dsBean) {
        //********************t******************************************绑定界面控件
        TextView IMC_textView_title = baseViewHolder.getView(R.id.IMC_textView_title);
        TextView IMC_textView_money = baseViewHolder.getView(R.id.IMC_textView_money);
        TextView IMC_textView_times = baseViewHolder.getView(R.id.IMC_textView_times);

        //****************************************************************赋值
        if(!dsBean.getM_task_listname().equals("") && dsBean.getM_task_listname()!=null){
            IMC_textView_title.setText(dsBean.getM_task_listname());
        }
        if(!dsBean.getM_task_listMoney().equals("") && dsBean.getM_task_listMoney()!=null){
            IMC_textView_money.setText("( "+dsBean.getM_task_listMoney()+" ￥ )");
        }
        if(!dsBean.getWorktimeStart().equals("") && dsBean.getWorktimeStart()!=null){
            if(!dsBean.getWorktimeEnd().equals("") && dsBean.getWorktimeEnd()!=null){
                IMC_textView_times.setText(dsBean.getWorktimeStart() +" - "+ dsBean.getWorktimeEnd());
            }
        }

    }





}