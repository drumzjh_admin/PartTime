package com.example.parttime.adapter;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_taskData;

import org.jetbrains.annotations.NotNull;

//草稿箱适配器
public class MyDraftAdapter extends BaseQuickAdapter<Entity_taskData.ResultBean.DsBean, BaseViewHolder> {


    public MyDraftAdapter(int layoutResId) {
        super(layoutResId);
    }
    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_taskData.ResultBean.DsBean dsBean) {
        //********************t******************************************绑定界面控件
        TextView IMC_textView_title = baseViewHolder.getView(R.id.IMC_textView_title);
        TextView IMC_textView_money = baseViewHolder.getView(R.id.IMC_textView_money);
        TextView IMC_textView_times = baseViewHolder.getView(R.id.IMC_textView_times);
        TextView IMC_textView_state = baseViewHolder.getView(R.id.IMC_textView_state);

        //****************************************************************赋值
        IMC_textView_state.setVisibility(View.VISIBLE);
        if(!dsBean.getM_task_list_remark5().equals("") && dsBean.getM_task_list_remark5()!=null){
            IMC_textView_title.setText(dsBean.getM_task_list_remark5());
        }
        if(!dsBean.getMoney().equals("") && dsBean.getMoney()!=null){
            IMC_textView_money.setText("( "+dsBean.getMoney()+" ￥ )");
        }
        if(!dsBean.getWorktimeStart().equals("") && dsBean.getWorktimeStart()!=null){
            if(!dsBean.getWorktimeEnd().equals("") && dsBean.getWorktimeEnd()!=null){
                IMC_textView_times.setText(dsBean.getWorktimeStart() +" - "+ dsBean.getWorktimeEnd());
            }
        }

        if(!dsBean.getM_task_list_remark9().equals("") && dsBean.getM_task_list_remark9()!=null){

            if(dsBean.getM_task_list_remark9().equals("0")){
                IMC_textView_state.setText("个人");
            }else {
                IMC_textView_state.setText("公司");
            }
        }
    }


}