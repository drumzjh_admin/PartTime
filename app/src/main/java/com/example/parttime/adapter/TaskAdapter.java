package com.example.parttime.adapter;

import android.net.Uri;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import cn.jzvd.JzvdStd;

//任务广场适配器
public class TaskAdapter extends BaseQuickAdapter<Entity_taskData.ResultBean.DsBean, BaseViewHolder> {

    //定位经纬度
    private String latitude = "";
    private String longitude = "";

    public TaskAdapter(int layoutResId, @Nullable List<Entity_taskData.ResultBean.DsBean> data, String latitude, String longitude) {
        super(layoutResId, data);
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_taskData.ResultBean.DsBean dsBean) {

        //********************t******************************************绑定界面控件
        TextView textView_title = baseViewHolder.getView(R.id.textView_title);
        TextView textView_money = baseViewHolder.getView(R.id.textView_money);
        TextView textView_timeData = baseViewHolder.getView(R.id.textView_timeData);
        TextView textView_location = baseViewHolder.getView(R.id.textView_location);
        TextView textView_characteristics0 = baseViewHolder.getView(R.id.textView_characteristics0);
        TextView textView_characteristics1 = baseViewHolder.getView(R.id.textView_characteristics1);
        TextView textView_characteristics2 = baseViewHolder.getView(R.id.textView_characteristics2);
        TextView textView_characteristics3 = baseViewHolder.getView(R.id.textView_characteristics3);
        TextView textView_peopleName = baseViewHolder.getView(R.id.textView_peopleName);
        TextView textView_wageState = baseViewHolder.getView(R.id.textView_wageState);
        TextView textView_distance = baseViewHolder.getView(R.id.textView_distance);
        ImageView imageView_peopleHead = baseViewHolder.getView(R.id.imageView_peopleHead);
        JzvdStd videoView_content = baseViewHolder.getView(R.id.videoView_content);

        String sex = "不限";
        String name = "***";
        if (dsBean.getSex().equals("1")) {
            sex = "男";
        } else if (dsBean.getSex().equals("2")) {
            sex = "女";
        } else if (dsBean.getSex().equals("3")) {
            sex = "不限";
        }
        if (!dsBean.getM_employee_list_remark1().equals("") && dsBean.getM_employee_list_remark1() != null) {
            name = dsBean.getM_employee_list_remark1();
        }

        //****************************************************************赋值
        textView_title.setText(dsBean.getM_task_list_remark5());
        textView_money.setText(dsBean.getMoney() + "元/天");
        textView_timeData.setText("工作时间:" + dsBean.getWorktimeStart() + "-" + dsBean.getWorktimeEnd());
        textView_location.setText("工作地点:" + dsBean.getAddress4());
        textView_characteristics0.setText("性别" + sex);
        textView_characteristics1.setText("年龄" + dsBean.getOld_year());
        textView_characteristics2.setText("学历" + dsBean.getEducation());
        textView_characteristics3.setText("籍贯不限");
        textView_peopleName.setText(name);
        //textView_wageState.setText(dsBean.getWorktype());
        //设置定位距离
        if(dsBean!=null && dsBean.getLatitude()!=null && dsBean.getDiameter()!=null &&latitude!=null && longitude!=null) {
            if (!dsBean.getLatitude().equals("") && !dsBean.getDiameter().equals("") && !latitude.equals("") && !longitude.equals("")) {
                textView_distance.setText(HelpTool.getDistance(Double.parseDouble(dsBean.getLatitude().trim()), Double.parseDouble(dsBean.getDiameter().trim())
                        , Double.parseDouble(latitude.trim()), Double.parseDouble(longitude.trim())) + "km");
            }
        }

        //设置发布头像
        if (dsBean.getM_employee_list_remark4().equals("") || dsBean.getM_employee_list_remark4() == null) {
            imageView_peopleHead.setImageResource(R.drawable.zhanweihead);
        } else {
            Glide.with(getContext()).load(Url.url_img + dsBean.getM_employee_list_remark4()).into(imageView_peopleHead);
        }
        //设置播放视频
        if (!dsBean.getAduioUrl().equals("") && dsBean.getAduioUrl() != null) {
            videoView_content.setUp(Url.url_img + dsBean.getAduioUrl(), "");
            if (!dsBean.getM_task_list_remark7().equals("") && dsBean.getM_task_list_remark7() != null) {
                videoView_content.posterImageView.setImageURI(Uri.parse(Url.url_img + dsBean.getM_task_list_remark7()));
                Glide.with(getContext()).load(Url.url_img + dsBean.getM_task_list_remark7()).into(videoView_content.posterImageView);
            } else {//显示暂无封面
                Glide.with(getContext()).load(R.drawable.wufengmian).into(videoView_content.posterImageView);
            }

        } else {//显示暂无视频
            Glide.with(getContext()).load(R.drawable.wushiping).into(videoView_content.posterImageView);
        }

    }

}
