package com.example.parttime.adapter;

import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_datawork;
import com.example.parttime.entity.Entity_taskManagement;

import org.jetbrains.annotations.NotNull;
//个人工作经历适配器
public class MyDataWorkAdapter extends BaseQuickAdapter<Entity_datawork.ResultBean.DsBean, BaseViewHolder> {

    public MyDataWorkAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_datawork.ResultBean.DsBean dsBean) {
//********************t******************************************绑定界面控件
        TextView IMW_textView_workName = baseViewHolder.getView(R.id.IMW_textView_workName);
        TextView IMW_textView_workTime = baseViewHolder.getView(R.id.IMW_textView_workTime);

        //****************************************************************赋值
        if(!dsBean.getWorksContent().equals("") && dsBean.getWorksContent()!=null){
            IMW_textView_workName.setText(dsBean.getWorksContent());
        }
        if(!dsBean.getRemark1().equals("") && dsBean.getRemark2()!=null){
            IMW_textView_workTime.setText(dsBean.getRemark1() +" - "+ dsBean.getRemark2());
        }
    }

}
