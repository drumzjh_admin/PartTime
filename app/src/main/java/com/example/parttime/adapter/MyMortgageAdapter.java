package com.example.parttime.adapter;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_mortgage;

import org.jetbrains.annotations.NotNull;

//押金详情适配器
public class MyMortgageAdapter extends BaseQuickAdapter<Entity_mortgage.ResultBean.DsBean, BaseViewHolder> {

    public MyMortgageAdapter(int layoutResId) {
        super(layoutResId);
    }
    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_mortgage.ResultBean.DsBean dsBean) {
        //********************t******************************************绑定界面控件
        TextView IMG_textView_money = baseViewHolder.getView(R.id.IMG_textView_money);
        TextView IMG_textView_type = baseViewHolder.getView(R.id.IMG_textView_type);
        TextView IMG_textView_time = baseViewHolder.getView(R.id.IMG_textView_time);
        TextView IMG_textView_state = baseViewHolder.getView(R.id.IMG_textView_state);
        TextView IMG_textView_click = baseViewHolder.getView(R.id.IMG_textView_click);
        TextView IMG_textView_show = baseViewHolder.getView(R.id.IMG_textView_show);

        //****************************************************************赋值
        if(!dsBean.getMoneyY().equals("") && dsBean.getMoneyY()!=null){
            IMG_textView_money.setText(dsBean.getMoneyY());
        }
        if(!dsBean.getRemark5().equals("") && dsBean.getRemark5()!=null){
            IMG_textView_type.setText("任务名称 : "+dsBean.getRemark5());
        }
        if(!dsBean.getAddtime().equals("") && dsBean.getAddtime()!=null){
            IMG_textView_time.setText("支付时间 : "+dsBean.getAddtime());
        }
        if(!dsBean.getType().equals("") && dsBean.getType()!=null){
            if(dsBean.getType().equals("0")){

                //IMG_textView_click.setVisibility(View.VISIBLE);
                IMG_textView_state.setText("付");
            }else {
                IMG_textView_state.setText("退");
                //IMG_textView_show.setVisibility(View.VISIBLE);
            }
        }

    }


}