package com.example.parttime.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_tasksign;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MyAcceptAdapter extends BaseQuickAdapter<Entity_tasksign.ResultBean.DsBean, BaseViewHolder> {

    TextView IMA_textView_date;
    TextView IMA_textView_dateIn;
    TextView IMA_textView_dateOut;
    ConstraintLayout constraintLayout1;
    ImageView IMA_imageView_in;
    ImageView IMA_imageView_out;


    public MyAcceptAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_tasksign.ResultBean.DsBean dsBean) {

        IMA_textView_date = baseViewHolder.getView(R.id.IMA_textView_date);
        IMA_textView_dateIn = baseViewHolder.getView(R.id.IMA_textView_dateIn);
        IMA_textView_dateOut = baseViewHolder.getView(R.id.IMA_textView_dateOut);
        constraintLayout1 = baseViewHolder.getView(R.id.constraintLayout1);
        IMA_imageView_in = baseViewHolder.getView(R.id.IMA_imageView_in);
        IMA_imageView_out = baseViewHolder.getView(R.id.IMA_imageView_out);


        if (dsBean.getAddtime() != null && !dsBean.getAddtime().equals("")) {
            String[] dateInList = dsBean.getAddtime().split(" ");
            IMA_textView_date.setText(dateInList[0]);
            IMA_textView_dateIn.setText("签到成功 " + dateInList[1]);
            IMA_imageView_in.setImageResource(R.drawable.signin);
        }
        if (dsBean.getEndtime() != null && !dsBean.getEndtime().equals("")) {
            String[] dateoutList = dsBean.getAddtime().split(" ");
            IMA_textView_dateOut.setText("签退成功 " + dateoutList[1]);
            IMA_imageView_out.setImageResource(R.drawable.signout);
        }


    }

}
