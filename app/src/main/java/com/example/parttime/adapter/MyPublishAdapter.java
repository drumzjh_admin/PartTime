package com.example.parttime.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_acceptPeopleNumber;
import com.example.parttime.service.Url;

import org.jetbrains.annotations.NotNull;

//接单人列表适配器
public class MyPublishAdapter extends BaseQuickAdapter<Entity_acceptPeopleNumber.ResultBean.DsBean, BaseViewHolder> {

    TextView IMP_textView_name;
    ImageView IMP_imageView_peopleHead;

    public MyPublishAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_acceptPeopleNumber.ResultBean.DsBean dsBean) {

        IMP_textView_name = baseViewHolder.getView(R.id.IMP_textView_name);
        IMP_imageView_peopleHead = baseViewHolder.getView(R.id.IMP_imageView_peopleHead);

        IMP_textView_name.setText(dsBean.getRemark1());
        if(dsBean.getRemark4().equals("") || dsBean.getRemark4() == null){
            IMP_imageView_peopleHead.setImageResource(R.drawable.zhanweihead);
        }else {
            Glide.with(getContext()).load(Url.url_img+dsBean.getRemark4()).into(IMP_imageView_peopleHead);
        }

    }

}
