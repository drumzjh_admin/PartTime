package com.example.parttime.adapter;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_moneyDetail;

import org.jetbrains.annotations.NotNull;

public class MyMoneyDetailAdapter extends BaseQuickAdapter<Entity_moneyDetail.ResultBean.DsBean, BaseViewHolder> {

    public MyMoneyDetailAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_moneyDetail.ResultBean.DsBean dsBean) {
        //********************t******************************************绑定界面控件
        TextView IMD_textView_money = baseViewHolder.getView(R.id.IMD_textView_money);
        TextView IMD_textView_moneyTime = baseViewHolder.getView(R.id.IMD_textView_moneyTime);
        TextView IMD_textView_states = baseViewHolder.getView(R.id.IMD_textView_states);

        //****************************************************************赋值
        if(!dsBean.getMoney().equals("") && dsBean.getMoney()!=null){
            IMD_textView_money.setText(dsBean.getMoney());
        }
        if(!dsBean.getAddtime().equals("") && dsBean.getAddtime()!=null){
            IMD_textView_moneyTime.setText(dsBean.getAddtime());
        }
        if(dsBean.getRemark1()!=null && !dsBean.getRemark1().equals("")){
            IMD_textView_states.setVisibility(View.VISIBLE);
            if(dsBean.getRemark1().equals("1")){
                IMD_textView_states.setText("已到账");
            }else {
                IMD_textView_states.setText("审核中");
            }
        }
    }
}
