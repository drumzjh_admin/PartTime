package com.example.parttime.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_taskManagement;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

//任务发布管理适配器
public class TaskPublishManagementAdapter extends BaseQuickAdapter<Entity_taskData.ResultBean.DsBean, BaseViewHolder> {

    TextView IT_textView_title;
    TextView IT_textView_money;
    TextView IT_textView_location;
    TextView IT_textView_timeData;
    Button IT_button_ok;
    Button IT_button_call;
    Button IT_button_cancel;
    Button IT_button_evaluation;
    View IT_view;

    public TaskPublishManagementAdapter(@Nullable List<Entity_taskData.ResultBean.DsBean> data) {
        super(R.layout.item_taskmanagement, data);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Entity_taskData.ResultBean.DsBean dsBean) {

        IT_textView_title = baseViewHolder.getView(R.id.IT_textView_title);
        IT_textView_money = baseViewHolder.getView(R.id.IT_textView_money);
        IT_textView_location = baseViewHolder.getView(R.id.IT_textView_location);
        IT_textView_timeData = baseViewHolder.getView(R.id.IT_textView_timeData);
        IT_button_ok = baseViewHolder.getView(R.id.IT_button_ok);
        IT_button_call = baseViewHolder.getView(R.id.IT_button_call);
        IT_button_cancel = baseViewHolder.getView(R.id.IT_button_cancel);
        IT_button_evaluation = baseViewHolder.getView(R.id.IT_button_evaluation);
        IT_view = baseViewHolder.getView(R.id.IT_view);

        IT_textView_title.setText(dsBean.getM_task_list_remark5());
        IT_textView_money.setText(dsBean.getMoney() + "元/天");
        IT_textView_location.setText("工作时间 : " + dsBean.getWorktimeStart() + " - " + dsBean.getWorktimeEnd());

        IT_button_call.setVisibility(View.GONE);
        IT_button_cancel.setVisibility(View.GONE);
        IT_view.setVisibility(View.GONE);

        if (dsBean.getM_task_list_remark2().equals("已完成")) {
            IT_button_ok.setVisibility(View.VISIBLE);
            IT_view.setVisibility(View.VISIBLE);
        }

    }


}
