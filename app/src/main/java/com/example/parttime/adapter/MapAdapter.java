package com.example.parttime.adapter;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.amap.api.services.help.Tip;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.example.parttime.R;
import org.jetbrains.annotations.NotNull;

//地图适配器
public class MapAdapter extends BaseQuickAdapter<Tip, BaseViewHolder> {

    ConstraintLayout IMAP_constraintLayout;
    TextView IMAP_textView_title;
    ImageView IMAP_imageView;

    public MapAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(@NotNull BaseViewHolder baseViewHolder, Tip tip) {

        IMAP_constraintLayout = baseViewHolder.getView(R.id.IMAP_constraintLayout);
        IMAP_textView_title = baseViewHolder.getView(R.id.IMAP_textView_title);
        IMAP_imageView = baseViewHolder.getView(R.id.IMAP_imageView);

        IMAP_textView_title.setText(tip.getName());

    }



}