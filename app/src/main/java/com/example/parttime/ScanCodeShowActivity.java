package com.example.parttime;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gyf.immersionbar.ImmersionBar;

import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder;

public class ScanCodeShowActivity extends AppCompatActivity {

    ImageView SCS_imageView;
    TextView SCS_textView_title;
    Button SCS_button_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_code_show);
        initActivity();

        String taskId = getIntent().getStringExtra("taskId");
        //制作二维码为耗时操作需要开线程处理
        Bitmap bitmapCode = QRCodeEncoder.syncEncodeQRCode(taskId,200);//制作二维码
        SCS_imageView.setImageBitmap(bitmapCode);

    }

    //返回上一页
    public void click_close(View view){
        finish();
    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).init();

        //页面控件绑定
        SCS_imageView = this.findViewById(R.id.SCS_imageView);
        SCS_textView_title = this.findViewById(R.id.SCS_textView_title);
        SCS_button_close = this.findViewById(R.id.SCS_button_close);

    }
}