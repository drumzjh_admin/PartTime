package com.example.parttime.my;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_moneyAndMortgage;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.rxjava.rxlife.RxLife;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人中心钱包页面
public class MyMoneyActivity extends AppCompatActivity {

    Button MM_button_extract;
    TextView MM_textView_money;
    TextView MM_textView_addBankCard;
    TextView MM_textView_bankCard;
    EditText MM_editText_inputCardName;
    EditText MM_editText_inputCardNumber;
    EditText MM_editText_inputCardNumber2;
    ConstraintLayout MM_constraintLayout_addBankCard;
    ConstraintLayout MM_constraintLayout_bankCard;
    ConstraintLayout MM_constraintLayout_inputCard;

    Entity_phoneData phoneData;
    Entity_phoneData.ResultBean.DsBean entity_phoneData;
    private Entity_moneyAndMortgage.ResultBean.DsBean entity_moneyAndMortgage;
    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_money);
        initActivity();

        entity_moneyAndMortgage = (Entity_moneyAndMortgage.ResultBean.DsBean) getIntent().getSerializableExtra("mymoney");

        phoneData = (Entity_phoneData) getIntent().getSerializableExtra("phoneData");
        entity_phoneData = phoneData.getResult().getDs().get(0);

        if (entity_moneyAndMortgage != null && phoneData != null) {//页面赋值和获取用户信息
            String mymoney = entity_moneyAndMortgage.getBalance();
            MM_textView_money.setText(mymoney + "元");
            getPhoneData(entity_phoneData.getPhone());
        } else {
            finish();
        }

    }

    //明细
    public void click_detail(View view) {

        startActivity(new Intent(this,MyMoneyDetailActivity.class));

    }

    //充值
    public void click_addMoney(View view) {
        startActivity(new Intent(this, MyAddMoneyActivity.class).putExtra("mymoney", entity_moneyAndMortgage));
    }

    //提现
    public void click_extract(View view) {

        //检测是否已绑定银行卡
        if (entity_phoneData.getRemark10() == null || entity_phoneData.getRemark10().equals("")) {
            Toasty.warning(this, "请先绑定银行卡", Toasty.LENGTH_SHORT).show();
            return;
        }

        startActivity(new Intent(this, MyTakeMoneyActivity.class).putExtra("mymoney", entity_moneyAndMortgage));

    }

    //判断是否数字
//    public static boolean isNumeric(String str){
//
//        for (int i = str.length();--i>=0;){
//
//            if (!Character.isDigit(str.charAt(i))){
//
//                return false;
//
//            }
//
//        }
//
//        return true;
//
//    }

    //添加银行卡
    public void click_addBankCard(View view) {
        MM_constraintLayout_addBankCard.setVisibility(View.GONE);
        MM_constraintLayout_inputCard.setVisibility(View.VISIBLE);
    }

    //提交银行卡信息
    public void click_submitBankCard(View view) {

        if (MM_editText_inputCardName.getText().toString().trim().equals("")) {
            Toasty.warning(this, "请输入持卡人姓名", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (MM_editText_inputCardNumber.getText().toString().trim().equals("")) {
            Toasty.warning(this, "请输入银行卡卡号", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (MM_editText_inputCardNumber2.getText().toString().trim().equals("")) {
            Toasty.warning(this, "请输入同样的银行卡卡号进行核对", Toasty.LENGTH_SHORT).show();
            return;
        }

        String textOne = MM_editText_inputCardNumber.getText().toString().trim();
        String textTwo = MM_editText_inputCardNumber2.getText().toString().trim();
        if(!textOne.equals(textTwo)){
            Toasty.warning(this,"两次输入的银行卡号不一致，请检查!").show();
            return;
        }

        UploadBankCard uploadBankCard = new UploadBankCard();
        uploadBankCard.setId(phoneData.getResult().getDs().get(0).getId());
        uploadBankCard.setRemark10(MM_editText_inputCardNumber.getText().toString().trim());
        uploadBankCard.setRemark11(MM_editText_inputCardName.getText().toString().trim());

        uploadBankCard(new Gson().toJson(uploadBankCard));
    }

    //上传银行卡信息
    private void uploadBankCard(String stringUploadBankCard) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("绑定中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_uploadBankCard)//发送Get请求
                .add("strjSon", stringUploadBankCard)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(this, "绑定成功", Toasty.LENGTH_SHORT).show();

                        getPhoneData(entity_phoneData.getPhone());

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("姓名不一致")) {
                            Toasty.warning(this, "姓名不一致", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(this, "绑定失败请重试", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "绑定失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //获取用户信息
    private void getPhoneData(String phone) {

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getLoginPhoneData)//发送Get请求
                .add("phone", phone)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        phoneData = gson.fromJson(jsonResult, Entity_phoneData.class);
                        entity_phoneData = phoneData.getResult().getDs().get(0);
                        Log.v("获取用户信息", "登录成功");

                        //数据存储加页面设置
                        HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOGINDATA, jsonResult, MyMoneyActivity.this);

                        if (entity_phoneData.getRemark10() != null && !entity_phoneData.getRemark10().equals("")) {
                            MM_constraintLayout_inputCard.setVisibility(View.GONE);
                            MM_constraintLayout_addBankCard.setVisibility(View.GONE);
                            MM_constraintLayout_bankCard.setVisibility(View.VISIBLE);
                            MM_textView_bankCard.setText(entity_phoneData.getRemark10());
                        } else {
                            MM_constraintLayout_addBankCard.setVisibility(View.VISIBLE);
                        }

                        //设置个人信息
                        String peopleID = entity_phoneData.getId();
                        //获取用户押金和余额数据
                        getMoneyAndMortgage(peopleID);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("帐号不存在。")) {
                            Log.v("获取用户信息", "当前用户未注册");
                        } else if (entity_httpResult.getErrorMsg().equals("密码错误。")) {
                            Log.v("获取用户信息", "密码错误");
                        } else {
                            Toasty.warning(this, "获取用户信息失败", Toasty.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                }, throwable -> {
                    //请求失败
                    Toasty.warning(this, "获取用户信息失败", Toasty.LENGTH_SHORT).show();
                    finish();
                });

    }

    //获取用户押金和余额数据
    private void getMoneyAndMortgage(String peopleID) {
        //new XPopup.Builder(getActivity()).dismissOnTouchOutside(false).asLoading("保存草稿中").show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getMoney)//发送Get请求（获取使用中的任务）
                .add("employee_list_id", peopleID)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        //数据存储加页面设置
                        HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOGINMONEY, jsonResult, getApplicationContext());

                        Entity_moneyAndMortgage entity_money = gson.fromJson(jsonResult, Entity_moneyAndMortgage.class);
                        entity_moneyAndMortgage = entity_money.getResult().getDs().get(0);
                        MM_textView_money.setText(entity_moneyAndMortgage.getBalance() + "元");

                    }

                }, throwable -> {
                    //请求失败
                });
    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarColor(R.color.colorAppYellow).navigationBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MM_button_extract = this.findViewById(R.id.MM_button_extract);
        MM_textView_money = this.findViewById(R.id.MM_textView_money);
        MM_textView_addBankCard = this.findViewById(R.id.MM_textView_addBankCard);
        MM_textView_bankCard = this.findViewById(R.id.MM_textView_bankCard);
        MM_editText_inputCardName = this.findViewById(R.id.MM_editText_inputCardName);
        MM_editText_inputCardNumber = this.findViewById(R.id.MM_editText_inputCardNumber);
        MM_constraintLayout_addBankCard = this.findViewById(R.id.MM_constraintLayout_addBankCard);
        MM_constraintLayout_bankCard = this.findViewById(R.id.MM_constraintLayout_bankCard);
        MM_constraintLayout_inputCard = this.findViewById(R.id.MM_constraintLayout_inputCard);
        MM_editText_inputCardNumber2 = this.findViewById(R.id.MM_editText_inputCardNumber2);

        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);

        //设置进入界面时所有控件全部隐藏
        MM_constraintLayout_addBankCard.setVisibility(View.GONE);
        MM_constraintLayout_bankCard.setVisibility(View.GONE);
        MM_constraintLayout_inputCard.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //刷新个人余额
        getMoneyAndMortgage(entity_phoneData.getId());
    }

    //绑定银行卡对象类
    class UploadBankCard {

        /**
         * id : //对应用户的id
         * remark10 ://身份证号
         * remark11 ://姓名
         */

        private String id;
        private String remark10;
        private String remark11;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRemark10() {
            return remark10;
        }

        public void setRemark10(String remark10) {
            this.remark10 = remark10;
        }

        public String getRemark11() {
            return remark11;
        }

        public void setRemark11(String remark11) {
            this.remark11 = remark11;
        }
    }

}