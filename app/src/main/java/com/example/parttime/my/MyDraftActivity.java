package com.example.parttime.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.example.parttime.R;
import com.example.parttime.adapter.MyCollectionAdapter;
import com.example.parttime.adapter.MyDraftAdapter;
import com.example.parttime.add.CompanyPublishActivity;
import com.example.parttime.add.PeoplePublishActivity;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_taskManagement;
import com.example.parttime.service.Url;
import com.example.parttime.square.SquareDetailedActivity;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.rxjava.rxlife.RxLife;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人中心草稿界面
public class MyDraftActivity extends AppCompatActivity {

    ConstraintLayout MDF_constraintLayout_nodata;
    RecyclerView MDF_recyclerView;


    private MyDraftAdapter myDraftAdapter;
    private Entity_taskData entity_taskData;
    private List<Entity_taskData.ResultBean.DsBean> entity_taskDataList;
    private String stringPeopleId;//登录人ID

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_draft);
        initActivity();
        //getDraftData();//获取草稿数据
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //获取草稿数据
    private void getDraftData() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", "M_task_list.remark2='0' and employee_list_id='" + stringPeopleId + "'")//添加请求参数，该方法可调用多次
                .add("sindex","0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        //显示任务至页面
                        if (entity_taskData.getResult().getDs().size() != 0) {
                            MDF_constraintLayout_nodata.setVisibility(View.GONE);
                            MDF_recyclerView.setVisibility(View.VISIBLE);
                            entity_taskDataList = new ArrayList<>();
                            entity_taskDataList = entity_taskData.getResult().getDs();
                            myDraftAdapter.setList(entity_taskDataList);
                        } else {
                            Toasty.warning(this, "暂无草稿", Toasty.LENGTH_SHORT).show();
                        }
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Toasty.warning(this, "暂无草稿", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(this, "获取草稿失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "获取草稿出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //取消草稿
    private void cancelDraft(int position) {

        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("删除中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_deleteTaskDraft)//发送Get请求（获取使用中的任务）
                .add("M_task_list_id", entity_taskDataList.get(position).getM_task_list_id())//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(this, "删除成功", Toasty.LENGTH_SHORT).show();
                        getDraftData();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "删除出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //单击
    OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
            if (entity_taskDataList.get(position).getM_task_list_remark9().equals("0")) {//个人
                startActivity(new Intent(MyDraftActivity.this, PeoplePublishActivity.class).putExtra("entity_taskData", entity_taskDataList.get(position)));
            } else {//企业
                startActivity(new Intent(MyDraftActivity.this, CompanyPublishActivity.class).putExtra("entity_taskData", entity_taskDataList.get(position)));
            }
        }
    };

    //长按
    OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
            //弹出选择框
            String[] selectString = {"删除草稿"};

            new XPopup.Builder(MyDraftActivity.this).asCenterList("", selectString, new OnSelectListener() {
                @Override
                public void onSelect(int position, String text) {

                    cancelDraft(position-1);

                }
            }).show();

            return false;
        }
    };

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();
        //页面控件绑定
        MDF_constraintLayout_nodata = this.findViewById(R.id.MDF_constraintLayout_nodata);
        MDF_recyclerView = this.findViewById(R.id.MDF_recyclerView);
        MDF_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myDraftAdapter = new MyDraftAdapter(R.layout.item_mycollection);
        MDF_recyclerView.setAdapter(myDraftAdapter);
        //监听处理事件
        myDraftAdapter.setOnItemClickListener(onItemClickListener);//单击
        myDraftAdapter.setOnItemLongClickListener(onItemLongClickListener);//长按

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

    }

    @Override
    protected void onResume() {
        super.onResume();
        getDraftData();//获取草稿数据
    }
}