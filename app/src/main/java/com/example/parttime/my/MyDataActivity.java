package com.example.parttime.my;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.parttime.R;
import com.example.parttime.add.PeoplePublishActivity;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpIM;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.loper7.layout.DateTimePicker;
import com.loper7.layout.StringUtils;
import com.loper7.layout.dialog.CardDatePickerDialog;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.impl.InputConfirmPopupView;
import com.lxj.xpopup.interfaces.OnInputConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.rxjava.rxlife.RxLife;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;
import com.example.parttime.my.MyFileProvider;

//个人中心信息界面
public class MyDataActivity extends AppCompatActivity {

    ConstraintLayout MD_constraintLayout_headImg;
    ConstraintLayout MD_constraintLayout_name;
    ConstraintLayout MD_constraintLayout_sex;
    ConstraintLayout MD_constraintLayout_birthday;
    ConstraintLayout MD_constraintLayout_phone;
    ConstraintLayout MD_constraintLayout_mail;
    ConstraintLayout MD_constraintLayout_location;
    ConstraintLayout MD_constraintLayout_work;
    ConstraintLayout MD_constraintLayout_certification;

    TextView MD_text_name;
    TextView MD_text_sex;
    TextView MD_text_birthday;
    TextView MD_text_phone;
    TextView MD_text_mail;
    TextView MD_text_location;
    TextView MD_text_certification;
    TextView MD_text_cardValidDate;//身份证有效期
    TextView MD_text_cardnumber;//身份证号
    TextView MD_text_nativeplace;//籍贯
    TextView MD_text_cardAddress;//身份证地址
    TextView MD_text_username;//身份证姓名

    Entity_phoneData phoneDataF;
    Entity_phoneData.ResultBean.DsBean phoneData;
    private List<Uri> myselectUri;//所选照片的Uri
    private List<String> myselectPath;//所选照片的路径
    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_data);
        initActivity();
        showPhoneData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        initActivity();

        //重新显示用户信息
        //updatePhoneData
        RxHttp.get(Url.url_getLoginPhoneData)//发送Get请求
                .add("phone",phoneData.getPhone())//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                        if (jsonStates.equals("1")) {
//                        Log.v("获取用户信息","登录成功");

                        //数据存储加页面设置
//                        HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOGINDATA,jsonResult,getActivity());

                        //设置个人信息

                        phoneDataF = gson.fromJson(jsonResult,Entity_phoneData.class);
                        if(phoneDataF!=null){
                            phoneData = phoneDataF.getResult().getDs().get(0);
                            showPhoneData();
                        }

                    }

                }, throwable -> {
                    //请求失败
                    Log.v("获取用户信息","登录失败请重试");
                });
//        onCreate(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //获取选择到的照片
        if (requestCode == 6 && resultCode == RESULT_OK) {
            //在manifest中设置不以沙盒模式运行android:requestLegacyExternalStorage="true"
            myselectUri = Matisse.obtainResult(data);
            myselectPath = Matisse.obtainPathResult(data);
            //判断视频大小
            File imageFile = new File(myselectPath.get(0));
            uploadPhoneDataHead(imageFile);
        }else if(requestCode == 8 && resultCode == RESULT_OK) {
            List<Uri> mSelected = Matisse.obtainResult(data);
            Log.d("Matisse", "mSelected: " + mSelected);
        }
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //选头像
    public void click_slectHean(View view) {
        Matisse.from(this).choose(MimeType.ofImage()).capture(true).
                captureStrategy(new CaptureStrategy(true,"com.example.parttime.fileProvider","photo")).
                showSingleMediaType(true).maxSelectable(1).thumbnailScale(0.8f).imageEngine(new GlideEngine()).forResult(6);
    }

    //改名字
    public void click_slectName(View view) {
//        InputConfirmPopupView popupView = new XPopup.Builder(this).asInputConfirm("提示", "请输入昵称","取消","确定",
//                new OnInputConfirmListener() {
//                    @Override
//                    public void onConfirm(String text) {
//                        phoneData.setRemark1(text);
//                        uploadPhoneData(phoneData);
//                    }
//                });
//        popupView.setConfirmText("确定");
//        popupView.setCancelText("取消");
//        popupView.show();
    }

    //改性别
    public void click_slectSex(View view) {
//        new XPopup.Builder(this).asBottomList("请选择", StorageConfig.sexArray2, new OnSelectListener() {
//            @Override
//            public void onSelect(int position, String text) {
//                if (text.equals("男")) {
//                    phoneData.setSex("1");
//                } else if (text.equals("女")) {
//                    phoneData.setSex("2");
//                } else if (text.equals("保密")) {
//                    phoneData.setSex("3");
//                }
//                uploadPhoneData(phoneData);
//            }
//        }).show();
    }

    //改电话
    public void click_slectPhone(View view) {

    }

    //改邮箱
    public void click_slectMail(View view) {
        new XPopup.Builder(this).asInputConfirm("提示", "请输入邮箱",
                new OnInputConfirmListener() {
                    @Override
                    public void onConfirm(String text) {
                        if (text.contains("@")) {
                            phoneData.setRemark10(text);
                            uploadPhoneData(phoneData);
                        } else {
                            Toasty.warning(MyDataActivity.this, "请输入正确的邮箱", Toasty.LENGTH_SHORT).show();
                        }
                    }
                })
                .show();
    }

    //改地址
    public void click_slectlocation(View view) {
        new XPopup.Builder(this).asInputConfirm("提示", "请输入地址",
                new OnInputConfirmListener() {
                    @Override
                    public void onConfirm(String text) {
                        phoneData.setAddress4(text);
                        uploadPhoneData(phoneData);
                    }
                })
                .show();
    }

    //改工作
    public void click_slectWork(View view) {
        startActivity(new Intent(this, MyDataWorkActivity.class));
    }

    //改认证
    public void click_slectCertification(View view) {
            startActivity(new Intent(this, MyDataCerActivity.class).putExtra("phoneData",phoneData));
    }

    //改生日
    public void click_slectBirthday(View view) {
//        new CardDatePickerDialog(this).setTitle("请选择").setBackGroundModel(2)
//                .showBackNow(false).showFocusDateInfo(false).setThemeColor(Color.parseColor("#FFCC33"))
//                .setMinTime(HelpTool.getDateTimeLong("1900年01月01日")).setMaxTime(System.currentTimeMillis()).setDisplayType(DateTimePicker.YEAR, DateTimePicker.MONTH, DateTimePicker.DAY)
//                .setOnChooseListener(onChooseListenerSelectBirthday).show();
    }

    //改生日
//    CardDatePickerDialog.OnChooseListener onChooseListenerSelectBirthday = new CardDatePickerDialog.OnChooseListener() {
//        @Override
//        public void onChoose(long l) {
//            String timeDate = StringUtils.conversionTime(l, "yyyy/MM/dd");
//            phoneData.setRemark3(timeDate);
//            uploadPhoneData(phoneData);
//        }
//    };

    //展示用户数据
    private void showPhoneData() {
        if(phoneData.getRemark4() != null && !phoneData.getRemark4().equals("")){
            //更新用户聊天信息
            HelpIM.RIM_upldataUser(phoneData.getId(),phoneData.getPhone(),Url.url_img+phoneData.getRemark4());
        }
        if (phoneData.getRemark1() != null && !phoneData.getRemark1().equals("")) {
            if(phoneData.getRemark11()!=null){
                MD_text_name.setText(phoneData.getRemark11());
            }else{
                MD_text_name.setText("游客");
            }
//            MD_text_name.setText(phoneData.getRemark1());
        }
        MD_text_cardValidDate = this.findViewById(R.id.MD_text_cardValidDate);//身份证有效期
        MD_text_cardnumber = this.findViewById(R.id.MD_text_cardnumber);//身份证号
        MD_text_nativeplace = this.findViewById(R.id.MD_text_nativeplace);//籍贯
        MD_text_cardAddress = this.findViewById(R.id.MD_text_cardAddress);//身份证地址
        MD_text_username = this.findViewById(R.id.MD_text_username);//身份证姓名
        if(phoneData.getRemark11()!=null && !phoneData.getRemark11().equals("")){
            MD_text_username.setText(phoneData.getRemark11());
        }

        if(phoneData.getId_card()!=null && !phoneData.getId_card().equals("")){
            MD_text_cardnumber.setText(phoneData.getId_card());
        }

        if(phoneData.getRemark14()!=null && !phoneData.getRemark14().equals("")){
            MD_text_nativeplace.setText(phoneData.getRemark14());
        }

        if(phoneData.getRemark13()!=null&&!phoneData.getRemark13().equals("")){
            MD_text_cardAddress.setText(phoneData.getRemark13());
        }

        if(phoneData.getRemark7()!=null && !phoneData.getRemark7().equals("")){
            MD_text_cardValidDate.setText(phoneData.getRemark7());
        }
        if (phoneData.getSex() != null && !phoneData.getSex().equals("")) {
            if (phoneData.getSex().equals("1")) {
                MD_text_sex.setText("男");
            } else if (phoneData.getSex().equals("2")) {
                MD_text_sex.setText("女");
            } else if (phoneData.getSex().equals("3")) {
                MD_text_sex.setText("保密");
            }
        }
        if (phoneData.getRemark3() != null && !phoneData.getRemark3().equals("")) {
            MD_text_birthday.setText(phoneData.getRemark3());
        }
        if (phoneData.getPhone() != null && !phoneData.getPhone().equals("")) {
            MD_text_phone.setText(phoneData.getPhone());
        }
        if (phoneData.getAddress4() != null && !phoneData.getAddress4().equals("")) {
            MD_text_location.setText(phoneData.getAddress4());
        }
        if (phoneData.getRemark10() != null && !phoneData.getRemark10().equals("")) {
            MD_text_mail.setText(phoneData.getRemark10());
        }
//        phoneData.setRemark8("0");
        if (phoneData.getRemark8() != null && !phoneData.getRemark8().equals("")) {
            if(phoneData.getRemark8().equals("1")){
                MD_text_certification.setText("已认证");
//                MD_constraintLayout_certification.setEnabled(false);
            }else {
                MD_text_certification.setText("未认证");
            }
        }else {
            MD_text_certification.setText("未认证");
        }
    }

    //修改头像
    private void uploadPhoneDataHead(File headFile) {

        phoneData.setId(phoneData.getId());
        String urlData = new Gson().toJson(phoneData);
        Log.v("上传JSON", urlData);
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("修改中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addTaskVideoImg)
                .addFile("index", headFile)
                .upload(progress -> {
                    //上传进度回调,0-100，仅在进度有更新时才会回调
                    int currentProgress = progress.getProgress(); //当前进度 0-100
                    long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                    long totalSize = progress.getTotalSize();     //要上传的总字节大小
                }, AndroidSchedulers.mainThread())
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        //上传成功
                        phoneData.setRemark4(entity_httpResult.getResult());
                        uploadPhoneData(phoneData);
                    } else if (jsonStates.equals("0")) {
                        Toasty.error(this, "修改失败请重试", Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "修改失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //修改个人信息
    private void uploadPhoneData(Entity_phoneData.ResultBean.DsBean phoneData) {
        phoneData.setId(phoneData.getId());
        String urlData = new Gson().toJson(phoneData);
        Log.v("上传JSON", urlData);
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("修改中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addUploadUser)//发送Get请求
                .add("strjSon", urlData)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(this, "修改成功", Toasty.LENGTH_SHORT).show();
                        showPhoneData();
                    } else if (jsonStates.equals("0")) {
                        Toasty.error(this, "修改失败请重试", Toasty.LENGTH_SHORT).show();
                    }

                },throwable -> {
                    String s =throwable.getMessage();
                    Log.d("throwable", "uploadPhoneData: "+s);
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "修改失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MD_constraintLayout_headImg = this.findViewById(R.id.MD_constraintLayout_headImg);
        MD_constraintLayout_name = this.findViewById(R.id.MD_constraintLayout_name);
        MD_constraintLayout_sex = this.findViewById(R.id.MD_constraintLayout_sex);
        MD_constraintLayout_birthday = this.findViewById(R.id.MD_constraintLayout_birthday);
        MD_constraintLayout_phone = this.findViewById(R.id.MD_constraintLayout_phone);
        MD_constraintLayout_mail = this.findViewById(R.id.MD_constraintLayout_mail);
        MD_constraintLayout_location = this.findViewById(R.id.MD_constraintLayout_location);
        MD_constraintLayout_work = this.findViewById(R.id.MD_constraintLayout_work);
        MD_constraintLayout_certification = this.findViewById(R.id.MD_constraintLayout_certification);

        MD_text_name = this.findViewById(R.id.MD_text_name);
        MD_text_sex = this.findViewById(R.id.MD_text_sex);
        MD_text_birthday = this.findViewById(R.id.MD_text_birthday);
        MD_text_phone = this.findViewById(R.id.MD_text_phone);
        MD_text_mail = this.findViewById(R.id.MD_text_mail);
        MD_text_location = this.findViewById(R.id.MD_text_location);
        MD_text_certification = this.findViewById(R.id.MD_text_certification);
        MD_text_cardValidDate = this.findViewById(R.id.MD_text_cardValidDate);//身份证有效期
        MD_text_cardnumber = this.findViewById(R.id.MD_text_cardnumber);//身份证号
        MD_text_nativeplace = this.findViewById(R.id.MD_text_nativeplace);//籍贯
        MD_text_cardAddress = this.findViewById(R.id.MD_textView_cardAddress);//身份证地址
        MD_text_username = this.findViewById(R.id.MD_textView_username);//身份证姓名


        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);

        //接收myfragment传参
        phoneDataF = (Entity_phoneData) getIntent().getSerializableExtra("phoneData");
        phoneData = phoneDataF.getResult().getDs().get(0);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}