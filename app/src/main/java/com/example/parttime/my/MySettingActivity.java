package com.example.parttime.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.parttime.LoginActivity;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.massage.ImCommunicationActivity;
import com.example.parttime.tool.StorageConfig;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;

//个人设置界面
public class MySettingActivity extends AppCompatActivity {

    TextView MST_textView_ver;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_setting);
        initActivity();
        //获取版本号
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(),0);
            MST_textView_ver.setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //退出登录
    public void click_outLogin(View view){

        new XPopup.Builder(this).asConfirm("提示", "是否退出登录","取消","确认", new OnConfirmListener() {
            @Override
            public void onConfirm() {
                SharedPreferences sharedPreferences = getSharedPreferences(StorageConfig.STORAGE_DATA, Context.MODE_PRIVATE);
                SharedPreferences.Editor editorUP = sharedPreferences.edit();
                editorUP.clear();
                editorUP.commit();

                startActivity(new Intent(MySettingActivity.this, LoginActivity.class));
                finish();
            }
        },null,false).show();

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MST_textView_ver = this.findViewById(R.id.MST_textView_ver);
        MST_textView_ver.setText("v1.0");

        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);

    }


}