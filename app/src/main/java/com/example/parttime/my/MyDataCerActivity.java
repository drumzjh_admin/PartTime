package com.example.parttime.my;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_peopleCard;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.CameraSurfaceView;
import com.example.parttime.tool.HelpTool;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.matisse.CaptureMode;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.wildma.idcardcamera.camera.IDCardCamera;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人认证界面
public class MyDataCerActivity extends AppCompatActivity {
    MyDataCerActivity thisActivity;
    ImageView MDC_imageView_zhengmian;
    ImageView MDC_imageView_fangmian;
    TextView MDC_textView_name;
    TextView MDC_textView_sex;
    TextView MDC_textView_cardNumber;
    Button MDC_button_addSubmit;
    FrameLayout frm_takepicture;
    private Button takepicture_button;
    private CameraSurfaceView mCameraSurfaceView;


    Entity_phoneData.ResultBean.DsBean phoneData;
    UploadPeopleCard uploadPeopleCard;

    private List<String> myselectPathOne;//所选正面照片的路径
    private List<String> myselectPathTwo;//所选反面照片的路径
    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_data_cer);
        initActivity();
        //接收myData传参
        phoneData = (Entity_phoneData.ResultBean.DsBean) getIntent().getSerializableExtra("phoneData");

        //判断当前是否已认证
        if (phoneData != null && phoneData.getRemark8() != null && phoneData.getRemark8().equals("1")) {
            String zhengmianurl = Url.url_img_idcard + phoneData.getCardURl();
            String fanmianurl = Url.url_img_idcard + phoneData.getCardURl2();
            MDC_imageView_zhengmian.setScaleType(ImageView.ScaleType.FIT_XY);
            MDC_imageView_fangmian.setScaleType(ImageView.ScaleType.FIT_XY);
            Glide.with(this).load(zhengmianurl).into(MDC_imageView_zhengmian);
            Glide.with(this).load(fanmianurl).into(MDC_imageView_fangmian);
            MDC_button_addSubmit.setVisibility(View.INVISIBLE);
            MDC_imageView_fangmian.setOnClickListener(null);
            MDC_imageView_zhengmian.setOnClickListener(null);

            MDC_textView_name.setVisibility(View.VISIBLE);
            MDC_textView_name.setText("姓名: " + phoneData.getRemark11());
            MDC_textView_sex.setVisibility(View.VISIBLE);
            MDC_textView_sex.setText(phoneData.getSex().equals("1") ? "性别: 男" : "性别:女");
            MDC_textView_cardNumber.setVisibility(View.VISIBLE);
            MDC_textView_cardNumber.setText("身份证号: " + phoneData.getId_card());
        }
        //上传数据赋值
        uploadPeopleCard = new UploadPeopleCard();
        uploadPeopleCard.setId(phoneData.getId());

        //如果phoneData有照片则显示
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 17 && data != null) {
            if (resultCode == IDCardCamera.RESULT_CODE) {
                //获取图片路径，显示图片
                final String path = IDCardCamera.getImagePath(data);
                if (!TextUtils.isEmpty(path)) {
                    if (requestCode == IDCardCamera.TYPE_IDCARD_FRONT) { //身份证正面
//                        mIvFront.setImageBitmap(BitmapFactory.decodeFile(path));
                        myselectPathOne.add(0,path);
                        MDC_imageView_zhengmian.setScaleType(ImageView.ScaleType.FIT_XY);
//                        Glide.with(this).load(myselectPathOne.get(0)).into(MDC_imageView_zhengmian);
                        Glide.with(this).load(path).into(MDC_imageView_zhengmian);
                    } else if (requestCode == IDCardCamera.TYPE_IDCARD_BACK) {  //身份证反面
//                        mIvBack.setImageBitmap(BitmapFactory.decodeFile(path));
//                        String captureImagePath = com.lxj.matisse.Matisse.obtainCaptureImageResult(data);
                        myselectPathTwo.add(0, path);
                        MDC_imageView_fangmian.setScaleType(ImageView.ScaleType.FIT_XY);
                        Glide.with(this).load(myselectPathTwo.get(0)).into(MDC_imageView_fangmian);
                    }
                }
            }
//            if (requestCode == 6) {//正面-选
//                myselectPathOne = Matisse.obtainPathResult(data);
//                MDC_imageView_zhengmian.setScaleType(ImageView.ScaleType.FIT_XY);
//                Glide.with(this).load(myselectPathOne.get(0)).into(MDC_imageView_zhengmian);
//            } else if (requestCode == 7) {//反面-选
//                myselectPathTwo = Matisse.obtainPathResult(data);
//                MDC_imageView_fangmian.setScaleType(ImageView.ScaleType.FIT_XY);
//                Glide.with(this).load(myselectPathTwo.get(0)).into(MDC_imageView_fangmian);
//            }else if (requestCode == 16) {//正面-拍
//                //获取拍摄的图片路径，如果是录制视频则是视频的第一帧图片路径
//                String captureImagePath = com.lxj.matisse.Matisse.obtainCaptureImageResult(data);
//                myselectPathOne.add(0,captureImagePath);
//                MDC_imageView_zhengmian.setScaleType(ImageView.ScaleType.FIT_XY);
//                Glide.with(this).load(myselectPathOne.get(0)).into(MDC_imageView_zhengmian);
//            }else if (requestCode == 17) {//反面-拍
//                //获取拍摄的图片路径，如果是录制视频则是视频的第一帧图片路径
//                String captureImagePath = com.lxj.matisse.Matisse.obtainCaptureImageResult(data);
//                myselectPathTwo.add(0,captureImagePath);
//                MDC_imageView_fangmian.setScaleType(ImageView.ScaleType.FIT_XY);
//                Glide.with(this).load(myselectPathTwo.get(0)).into(MDC_imageView_fangmian);
//            }

        }

    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //选择正面照片
    public void click_selectImgOne(View view) {
//        Intent intent1 = new Intent(this,
//                IdCameraActivity.class);
//        startActivity(intent1);
//
//        frm_takepicture.setVisibility(View.VISIBLE);
//        frm_takepicture.bringToFront();
        IDCardCamera.create(this).openCamera(IDCardCamera.TYPE_IDCARD_FRONT);
//        choosePhoto(6);
    }

    //选择反面照片
    public void click_selectImgTwo(View view) {

//        choosePhoto(7);
        IDCardCamera.create(this).openCamera(IDCardCamera.TYPE_IDCARD_BACK);
    }

    private void choosePhoto(int tagCode) {

        //弹出选择框
        String[] selectString = {"拍摄照片", "选择照片"};

        new XPopup.Builder(this).asCenterList("", selectString, new OnSelectListener() {
            @Override
            public void onSelect(int position, String text) {

                if (position == 0) {

                    //拍摄照片代码
                    com.lxj.matisse.Matisse.from(MyDataCerActivity.this)
                            //.jumpCapture()//直接跳拍摄，默认可以同时拍摄照片和视频
                            .jumpCapture(CaptureMode.Image)//只拍照片
                            //.jumpCapture(CaptureMode.Video)//只拍视频
                            .isCrop(false) //开启裁剪
                            .forResult(tagCode + 10);

                } else {

                    Matisse.from(MyDataCerActivity.this)
                            .choose(MimeType.ofImage())
                            .showSingleMediaType(true)
                            .maxSelectable(1)
                            .thumbnailScale(0.8f)
                            .imageEngine(new GlideEngine()).forResult(tagCode);

                }

            }
        }).show();
    }

    //认证
    public void click_submit(View view) {

        if (myselectPathOne == null || myselectPathOne.size() == 0) {
            Toasty.warning(this, "请选择身份证正面照", Toasty.LENGTH_SHORT).show();
            return;
        }
        if (myselectPathTwo == null || myselectPathTwo.size() == 0) {
            Toasty.warning(this, "请选择身份证背面照", Toasty.LENGTH_SHORT).show();
            return;
        }

        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("认证中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗
        uploadCheckCardOne();

    }


    //正面
    private void uploadCheckCardOne() {

        uploadPeopleCard.setType("0");
        String uploadData = new Gson().toJson(uploadPeopleCard);

        File imageFile = new File(myselectPathOne.get(0));

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addPeopleCard)
                .add("json", uploadData)
                .addFile("index", imageFile)
                .upload(progress -> {
                    //上传进度回调,0-100，仅在进度有更新时才会回调
                    int currentProgress = progress.getProgress(); //当前进度 0-100
                    long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                    long totalSize = progress.getTotalSize();     //要上传的总字节大小
                }, AndroidSchedulers.mainThread())
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_peopleCard entity_peopleCard = gson.fromJson(jsonResult, Entity_peopleCard.class);
                        //上传成功
                        MDC_textView_name.setText(entity_peopleCard.getResult().getValues().get(0).get姓名());
                        MDC_textView_sex.setText(entity_peopleCard.getResult().getValues().get(0).get性别());
                        MDC_textView_cardNumber.setText(entity_peopleCard.getResult().getValues().get(0).get公民身份号码());

                        MDC_textView_cardNumber.setVisibility(View.VISIBLE);
                        MDC_textView_name.setVisibility(View.VISIBLE);
                        MDC_textView_sex.setVisibility(View.VISIBLE);

                        uploadCheckCardTwo();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.error(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "认证失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //反面
    private void uploadCheckCardTwo() {
        uploadPeopleCard.setType("1");
        String uploadData = new Gson().toJson(uploadPeopleCard);

        File imageFile = new File(myselectPathTwo.get(0));

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addPeopleCard)
                .add("json", uploadData)
                .addFile("index", imageFile)
                .upload(progress -> {
                    //上传进度回调,0-100，仅在进度有更新时才会回调
                    int currentProgress = progress.getProgress(); //当前进度 0-100
                    long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                    long totalSize = progress.getTotalSize();     //要上传的总字节大小
                }, AndroidSchedulers.mainThread())
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    xPopupLoading.dismiss();


                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];
                    Entity_peopleCard entity_peopleCard = gson.fromJson(jsonResult, Entity_peopleCard.class);

                    if (jsonStates.equals("1")) {
                        //上传成功
                        Toasty.warning(this, "认证成功", Toasty.LENGTH_SHORT).show();
                        finish();
                        //刷新当前页面

                    } else if (jsonStates.equals("0")) {
                        Toasty.error(this, entity_peopleCard.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "认证失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        thisActivity = this;
        frm_takepicture = this.findViewById(R.id.frm_takepicture);
        //页面控件绑定
        MDC_imageView_zhengmian = this.findViewById(R.id.MDC_imageView_zhengmian);
        MDC_imageView_fangmian = this.findViewById(R.id.MDC_imageView_fangmian);
        MDC_textView_cardNumber = this.findViewById(R.id.MDC_textView_cardNumber);
        MDC_textView_sex = this.findViewById(R.id.MDC_textView_sex);
        MDC_textView_name = this.findViewById(R.id.MDC_textView_name);
        MDC_button_addSubmit = this.findViewById(R.id.MDC_button_addSubmit);

        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);

        myselectPathOne = new ArrayList<>();
        myselectPathTwo = new ArrayList<>();

        mCameraSurfaceView = (CameraSurfaceView) findViewById(R.id.cameraSurfaceView);
        takepicture_button = (Button) findViewById(R.id.takePic);
        takepicture_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCameraSurfaceView.takePicture(thisActivity);
            }
        });
    }

    //type:1.正面 2.反面
    public void setPicturePath(int type, String path, Bitmap bmp) {
        if (type == 1) {//正面-拍
            //获取拍摄的图片路径，如果是录制视频则是视频的第一帧图片路径
//            String captureImagePath = com.lxj.matisse.Matisse.obtainCaptureImageResult(path);
            myselectPathOne.add(0, path);
            MDC_imageView_zhengmian.setImageBitmap(bmp);
//            MDC_imageView_zhengmian.setScaleType(ImageView.ScaleType.FIT_CENTER);
            Glide.with(this).load(myselectPathOne.get(0)).into(MDC_imageView_zhengmian);
        } else if (type == 2) {//反面-拍
            //获取拍摄的图片路径，如果是录制视频则是视频的第一帧图片路径
//            String captureImagePath = com.lxj.matisse.Matisse.obtainCaptureImageResult(data);
            myselectPathTwo.add(0, path);
            MDC_imageView_fangmian.setScaleType(ImageView.ScaleType.FIT_XY);
            Glide.with(this).load(myselectPathTwo.get(0)).into(MDC_imageView_fangmian);
        }

        this.frm_takepicture.setVisibility(View.INVISIBLE);
    }

    //身份照片上传数据
    class UploadPeopleCard {

        /**
         * id : 3
         * type : 0
         */

        private String id;
        private String type;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}
