package com.example.parttime.my;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.parttime.R;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.rxjava.rxlife.RxLife;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人中心意见反馈页面
public class MyFeedBackActivity extends AppCompatActivity {



    EditText MFB_editTextText_input;

    private String stringPeopleId;//用户的ID

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_feed_back);
        initActivity();
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //提交
    public void click_submit(View view) {

        if(MFB_editTextText_input.getText().toString()==""){
            Toasty.warning(this,"请输入您的意见或反馈",Toasty.LENGTH_SHORT).show();
        }else {
            xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("提交中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

            //上传数据赋值
            UploadFeedback uploadFeedback = new UploadFeedback();
            uploadFeedback.setEmpid(stringPeopleId);
            uploadFeedback.setContent(MFB_editTextText_input.getText().toString());
            String uploadData = new Gson().toJson(uploadFeedback);

            RxHttp.setDebug(true);
            RxHttp.get(Url.url_submitFeedBack)//发送Get请求（获取使用中的任务）
                    .add("strJson", uploadData)//添加请求参数，该方法可调用多次
                    .asString()//指定返回类型数据
                    .observeOn(AndroidSchedulers.mainThread())
                    .as(RxLife.as(this))
                    .subscribe(json -> {
                        xPopupLoading.dismiss();

                        //请求成功
                        String jsonStates = HelpTool.ParseHttpResult(json)[0];

                        if (jsonStates.equals("1")) {
                            Toasty.warning(this, "反馈提交成功", Toasty.LENGTH_SHORT).show();
                        } else if (jsonStates.equals("0")) {
                            Toasty.error(this, "反馈出错请重试", Toasty.LENGTH_SHORT).show();
                        }

                    }, throwable -> {
                        //请求失败
                        xPopupLoading.dismiss();
                        Toasty.error(this, "反馈出错请重试", Toasty.LENGTH_SHORT).show();
                    });

        }

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MFB_editTextText_input = this.findViewById(R.id.MFB_editTextText_input);

        //获取用户的ID
        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());
        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);

    }

}

class UploadFeedback{

    /**
     * empid : 3
     * content : lalala
     */

    private String empid;
    private String content;

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}