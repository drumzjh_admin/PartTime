package com.example.parttime.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.example.parttime.R;
import com.example.parttime.adapter.MyMortgageAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_mortgage;
import com.example.parttime.entity.Entity_taskManagement;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.rxjava.rxlife.RxLife;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//押金详情界面
public class MyMortgageActivity extends AppCompatActivity {

    ConstraintLayout MMG_constraintLayout_nodata;
    RecyclerView MMG_recyclerView;

    private MyMortgageAdapter myMortgageAdapter;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_mortgage);
        initActivity();
        String phoneDataId = getIntent().getStringExtra("phoneDataId");
        getMortgage(phoneDataId);
    }
    //返回上一页
    public void click_finish(View view) {
        finish();
    }
    //获取押金详情
    private void getMortgage(String phoneDataId){
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getMortgageList)//发送Get请求（获取使用中的任务）
                .add("employee_list_id", phoneDataId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_mortgage entity_mortgage = gson.fromJson(jsonResult, Entity_mortgage.class);
                        //显示任务至页面
                        if (entity_mortgage.getResult().getDs().size() != 0) {
                            MMG_constraintLayout_nodata.setVisibility(View.GONE);
                            MMG_recyclerView.setVisibility(View.VISIBLE);
                            myMortgageAdapter.setList(entity_mortgage.getResult().getDs());
                        } else {
                            Toasty.warning(this, "暂无押金明细", Toasty.LENGTH_SHORT).show();
                        }
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Toasty.warning(this, "暂无押金明细", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(this, "获取押金明细失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "获取押金明细出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //子元素被单击
    OnItemChildClickListener onItemChildClickListener = new OnItemChildClickListener() {
        @Override
        public void onItemChildClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {

            if(view.getId()==R.id.IMG_textView_click){//触发退还押金

            }

        }
    };

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();
        //页面控件绑定
        MMG_constraintLayout_nodata = this.findViewById(R.id.MMG_constraintLayout_nodata);
        MMG_recyclerView = this.findViewById(R.id.MMG_recyclerView);
        MMG_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myMortgageAdapter = new MyMortgageAdapter(R.layout.item_mymortgage);
        MMG_recyclerView.setAdapter(myMortgageAdapter);

        //监听处理事件
        myMortgageAdapter.setOnItemChildClickListener(onItemChildClickListener);//单击

    }

}