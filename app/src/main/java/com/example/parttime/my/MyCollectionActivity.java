package com.example.parttime.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupWindow;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.example.parttime.R;
import com.example.parttime.adapter.MyCollectionAdapter;
import com.example.parttime.adapter.TaskAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_taskManagement;
import com.example.parttime.service.Url;
import com.example.parttime.square.SquareDetailedActivity;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.rxjava.rxlife.RxLife;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人中心收藏界面
public class MyCollectionActivity extends AppCompatActivity {

    ConstraintLayout MCC_constraintLayout_nodata;
    RecyclerView MCC_recyclerView;

    private MyCollectionAdapter myCollectionAdapter;

    private Entity_taskManagement entity_taskManagement;
    private List<Entity_taskManagement.ResultBean.DsBean> entity_taskManagementList;
    private String stringPeopleId;//登录人ID

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_collection);
        initActivity();
        getCollectionData();//获取收藏数据
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //获取收藏数据
    private void getCollectionData() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getCollectionList)//发送Get请求（获取使用中的任务）
                .add("employee_listid", stringPeopleId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        entity_taskManagement = gson.fromJson(jsonResult, Entity_taskManagement.class);
                        //显示任务至页面
                        if (entity_taskManagement.getResult().getDs().size() != 0) {
                            MCC_constraintLayout_nodata.setVisibility(View.GONE);
                            MCC_recyclerView.setVisibility(View.VISIBLE);
                            entity_taskManagementList = new ArrayList<>();
                            entity_taskManagementList = entity_taskManagement.getResult().getDs();
                            myCollectionAdapter.setList(entity_taskManagementList);
                        } else {
                            Toasty.warning(this, "暂无收藏", Toasty.LENGTH_SHORT).show();
                        }
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Toasty.warning(this, "暂无收藏", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(this, "获取收藏失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "获取收藏出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //取消收藏
    private void cancelCollection(int position) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("取消中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        //上传类赋值
        UploadCollection uploadCollection = new UploadCollection();
        uploadCollection.setEmpid(stringPeopleId);
        uploadCollection.setTask_id(entity_taskManagement.getResult().getDs().get(position).getM_task_listid());
        String uploadData = new Gson().toJson(uploadCollection);

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_cancelCollection)//发送Get请求（获取使用中的任务）
                .add("strjSon", uploadData)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(this, "取消成功", Toasty.LENGTH_SHORT).show();
                        getCollectionData();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "取消出错", Toasty.LENGTH_SHORT).show();
                });

    }

    //获取广场数据
    private void getTaskData(String addData) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", addData)//添加请求参数，该方法可调用多次
                .add("sindex","0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);
                        //跳转任务详情界面
                        if (entity_taskData.getResult().getDs().size() != 0) {
                            startActivity(new Intent(MyCollectionActivity.this, SquareDetailedActivity.class).putExtra("squareDetailed", entity_taskData.getResult().getDs().get(0)));
                        } else {
                            Toasty.warning(this, "暂无任务信息", Toasty.LENGTH_SHORT).show();
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            Toasty.warning(this, "未搜索到任务", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(this, "任务详情加载失败请重试", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "任务详情加载出错请重试", Toasty.LENGTH_SHORT).show();

                });

    }

    //单击
    OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
            //获取任务详情
            getTaskData("M_task_list.id=" + entity_taskManagement.getResult().getDs().get(position).getM_task_listid());
        }
    };

    //长按
    OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
            //弹出选择框
            String[] selectString = {"取消收藏"};

            new XPopup.Builder(MyCollectionActivity.this).asCenterList("", selectString, new OnSelectListener() {
                @Override
                public void onSelect(int position, String text) {

                    cancelCollection(position-1);

                }
            }).show();

            return false;
        }
    };

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();
        //页面控件绑定
        MCC_constraintLayout_nodata = this.findViewById(R.id.MCC_constraintLayout_nodata);
        MCC_recyclerView = this.findViewById(R.id.MCC_recyclerView);
        MCC_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myCollectionAdapter = new MyCollectionAdapter(R.layout.item_mycollection);
        MCC_recyclerView.setAdapter(myCollectionAdapter);
        //监听处理事件
        myCollectionAdapter.setOnItemClickListener(onItemClickListener);//单击
        myCollectionAdapter.setOnItemLongClickListener(onItemLongClickListener);//长按

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

    }

    class UploadCollection {

        /**
         * task_id : 1
         * empid : 1
         */

        private String task_id;
        private String empid;

        public String getTask_id() {
            return task_id;
        }

        public void setTask_id(String task_id) {
            this.task_id = task_id;
        }

        public String getEmpid() {
            return empid;
        }

        public void setEmpid(String empid) {
            this.empid = empid;
        }
    }

}