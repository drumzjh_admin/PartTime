package com.example.parttime.my;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.parttime.HomeActivity;
import com.example.parttime.LoginActivity;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_moneyAndMortgage;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lihang.smartloadview.SmartLoadingView;
import com.lxj.xpopup.core.BasePopupView;
import com.rxjava.rxlife.RxLife;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

/**
 * A simple {@link Fragment} subclass.
 */
//个人中心页面
public class myFragment extends Fragment {

    TextView FM_textView_setting;
    CircleImageView FM_imageView_peopleHead;
    TextView FM_textView_name;
    TextView FM_textView_phone;
    TextView FM_textView_mortgage;
    Button FM_button_lookMortgage;
    ConstraintLayout FM_constraintLayout_wallet;
    ConstraintLayout FM_constraintLayout_collection;
    ConstraintLayout FM_constraintLayout_certification;
    ConstraintLayout FM_constraintLayout_draft;
    ConstraintLayout FM_constraintLayout_feedback;
    View FM_view6;
    ImageButton FM_imageButton_walletOpen;
    ImageButton FM_imageButton_collectionOpen;
    ImageButton FM_imageButton_certificationOpen;
    ImageButton FM_imageButton_draftOpen;
    ImageButton FM_imageButton_feedbackOpen;
    //个人的信息
    Entity_phoneData phoneData;
    Entity_moneyAndMortgage entity_moneyAndMortgage;
    //加载窗
    private BasePopupView xPopupLoading;

    public myFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initActivity();
    }

    //设置
    public void click_setting() {
        startActivity(new Intent(getActivity(),MySettingActivity.class));
    }

    //个人信息
    public void click_peopleData() {
        startActivity(new Intent(getActivity(),MyDataActivity.class).putExtra("phoneData",phoneData));
    }

    //押金详情
    public void click_lookMortgage() {
        startActivity(new Intent(getActivity(),MyMortgageActivity.class).putExtra("phoneDataId",phoneData.getResult().getDs().get(0).getId()));
    }

    //钱包
    public void click_wallet() {
        startActivity(new Intent(getActivity(),MyMoneyActivity.class).putExtra("mymoney",entity_moneyAndMortgage.getResult().getDs().get(0)).putExtra("phoneData",phoneData));
    }

    //收藏
    public void click_collection() {
        startActivity(new Intent(getActivity(),MyCollectionActivity.class));
    }

    //认证
    public void click_certification() {
        startActivity(new Intent(getActivity(),MyCertificationActivity.class).putExtra("phoneData",phoneData));
    }

    //草稿箱
    public void click_draft() {
        startActivity(new Intent(getActivity(),MyDraftActivity.class));
    }

    //意见反馈
    public void click_feedback() {
        startActivity(new Intent(getActivity(),MyFeedBackActivity.class));
    }

    //获取用户信息
    private void getPhoneData(String phone){

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getLoginPhoneData)//发送Get请求
                .add("phone",phone)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Log.v("获取用户信息","登录成功");

                        //数据存储加页面设置
                        HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOGINDATA,jsonResult,getActivity());

                        //设置个人信息
                        phoneData = new Gson().fromJson(jsonResult,Entity_phoneData.class);
                        String peopleID = phoneData.getResult().getDs().get(0).getId();

                        if(phoneData.getResult().getDs().get(0).getRemark4()!=null && !phoneData.getResult().getDs().get(0).getRemark4().equals("")){
                            Glide.with(getContext()).load(Url.url_img+phoneData.getResult().getDs().get(0).getRemark4()).into(FM_imageView_peopleHead);
                        }
                        if(phoneData.getResult().getDs().get(0).getRemark1()!=null && !phoneData.getResult().getDs().get(0).getRemark1().equals("")){
                            FM_textView_name.setText(phoneData.getResult().getDs().get(0).getRemark1());
                        }
                        if(phoneData.getResult().getDs().get(0).getPhone()!=null && !phoneData.getResult().getDs().get(0).getPhone().equals("")){
                            FM_textView_phone.setText(phoneData.getResult().getDs().get(0).getPhone());
                        }

                        //获取用户押金和余额数据
                        getMoneyAndMortgage(peopleID);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("帐号不存在。")) {
                            Log.v("获取用户信息","当前用户未注册");
                        } else if (entity_httpResult.getErrorMsg().equals("密码错误。")) {
                            Log.v("获取用户信息","密码错误");
                        }else {
                            Log.v("获取用户信息","获取用户信息失败");
                        }
                    }

                }, throwable -> {
                    //请求失败
                    Log.v("获取用户信息","登录失败请重试");
                });

    }

    //获取用户押金和余额数据
    private void getMoneyAndMortgage(String peopleID){
        //new XPopup.Builder(getActivity()).dismissOnTouchOutside(false).asLoading("保存草稿中").show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getMoney)//发送Get请求（获取使用中的任务）
                .add("employee_list_id", peopleID)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        //数据存储加页面设置
                        HelpTool.setSharedPreferenceString(StorageConfig.EDITOR_LOGINMONEY,jsonResult,getActivity());

                        entity_moneyAndMortgage = gson.fromJson(jsonResult, Entity_moneyAndMortgage.class);
                        FM_textView_mortgage.setText("押金  "+entity_moneyAndMortgage.getResult().getDs().get(0).getMargin()+" ￥");
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (!entity_taskData.getErrorMsg().equals("无数据")) {
                            FM_textView_mortgage.setText("押金  *** ￥");
                        }
                    }

                }, throwable -> {
                    //请求失败
                    FM_textView_mortgage.setText("押金  *** ￥");
                });
    }

    //单击
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.FM_textView_setting) {//设置
                click_setting();
            } else if (v.getId() == R.id.FM_imageView_peopleHead) {//个人信息
                click_peopleData();
            } else if (v.getId() == R.id.FM_textView_name) {//个人信息
                click_peopleData();
            }else if (v.getId() == R.id.FM_view6){
                click_peopleData();
            }else if (v.getId() == R.id.FM_button_lookMortgage) {//押金详情
                click_lookMortgage();
            } else if (v.getId() == R.id.FM_constraintLayout_wallet || v.getId() == R.id.FM_imageButton_walletOpen) {//钱包
                click_wallet();
            } else if (v.getId() == R.id.FM_constraintLayout_collection || v.getId() == R.id.FM_imageButton_collectionOpen) {//收藏
                click_collection();
            } else if (v.getId() == R.id.FM_constraintLayout_certification || v.getId() == R.id.FM_imageButton_certificationOpen) {//认证
                click_certification();
            } else if (v.getId() == R.id.FM_constraintLayout_draft || v.getId() == R.id.FM_imageButton_draftOpen) {//草稿箱
                click_draft();
            } else if (v.getId() == R.id.FM_constraintLayout_feedback || v.getId() == R.id.FM_imageButton_feedbackOpen) {//意见反馈
                click_feedback();
            }

        }
    };

    //初始化页面工作
    private void initActivity() {

        //页面控件绑定
        FM_textView_setting = getActivity().findViewById(R.id.FM_textView_setting);
        FM_imageView_peopleHead = getActivity().findViewById(R.id.FM_imageView_peopleHead);
        FM_textView_name = getActivity().findViewById(R.id.FM_textView_name);
        FM_textView_phone = getActivity().findViewById(R.id.FM_textView_phone);
        FM_textView_mortgage = getActivity().findViewById(R.id.FM_textView_mortgage);
        FM_button_lookMortgage = getActivity().findViewById(R.id.FM_button_lookMortgage);
        FM_constraintLayout_wallet = getActivity().findViewById(R.id.FM_constraintLayout_wallet);
        FM_constraintLayout_collection = getActivity().findViewById(R.id.FM_constraintLayout_collection);
        FM_constraintLayout_certification = getActivity().findViewById(R.id.FM_constraintLayout_certification);
        FM_constraintLayout_draft = getActivity().findViewById(R.id.FM_constraintLayout_draft);
        FM_constraintLayout_feedback = getActivity().findViewById(R.id.FM_constraintLayout_feedback);
        FM_view6 = getActivity().findViewById(R.id.FM_view6);
        FM_imageButton_walletOpen = getActivity().findViewById(R.id.FM_imageButton_walletOpen);
        FM_imageButton_collectionOpen = getActivity().findViewById(R.id.FM_imageButton_collectionOpen);
        FM_imageButton_certificationOpen = getActivity().findViewById(R.id.FM_imageButton_certificationOpen);
        FM_imageButton_draftOpen = getActivity().findViewById(R.id.FM_imageButton_draftOpen);
        FM_imageButton_feedbackOpen = getActivity().findViewById(R.id.FM_imageButton_feedbackOpen);

        //监听单击事件
        FM_textView_setting.setOnClickListener(onClickListener);
        FM_imageView_peopleHead.setOnClickListener(onClickListener);
        FM_textView_name.setOnClickListener(onClickListener);
        FM_button_lookMortgage.setOnClickListener(onClickListener);
        FM_constraintLayout_wallet.setOnClickListener(onClickListener);
        FM_constraintLayout_collection.setOnClickListener(onClickListener);
        FM_constraintLayout_certification.setOnClickListener(onClickListener);
        FM_constraintLayout_draft.setOnClickListener(onClickListener);
        FM_constraintLayout_feedback.setOnClickListener(onClickListener);
        FM_view6.setOnClickListener(onClickListener);
        FM_imageButton_walletOpen.setOnClickListener(onClickListener);
        FM_imageButton_collectionOpen.setOnClickListener(onClickListener);
        FM_imageButton_certificationOpen.setOnClickListener(onClickListener);
        FM_imageButton_draftOpen.setOnClickListener(onClickListener);
        FM_imageButton_feedbackOpen.setOnClickListener(onClickListener);

    }

    @Override
    public void onResume() {
        super.onResume();

        //获取用户信息
        String phoneDataString = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINDATA,getActivity());
        phoneData = new Gson().fromJson(phoneDataString,Entity_phoneData.class);
        getPhoneData(phoneData.getResult().getDs().get(0).getPhone());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //ImmersionBar.destroy(this);
    }

}
