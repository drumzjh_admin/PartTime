package com.example.parttime.my;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_peopleCard;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//个人中心企业认证界面
public class MyCertificationActivity extends AppCompatActivity {

    ImageView MCF_imageView_zhengmian;
    Button MCF_button_addSubmit;
    TextView MCF_textView_top;
    TextView txt_societycreditcode;
    TextView txt_organizationform;//组织形式
    TextView txt_businessscope;
    TextView txt_startdate;
    TextView txt_legalperson;
    TextView txt_registercapital;
    TextView txt_address;
    TextView txt_companyname;
    ConstraintLayout ll_businessinfo;

    Entity_phoneData phoneData;
    private List<String> myselectPath;//所选照片的路径
    private List<Uri> myselectUri;//所选照片的Uri

    //加载窗
    private BasePopupView xPopupLoading;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_certification);
        initActivity();
        //接收myData传参
        phoneData = (Entity_phoneData) getIntent().getSerializableExtra("phoneData");
        //判断是否已认证
        if(phoneData.getResult().getDs().get(0).getRemark6() != null && !phoneData.getResult().getDs().get(0).getRemark6().equals("")){//已认证
            MCF_textView_top.setText("已认证");
            MCF_imageView_zhengmian.setEnabled(false);
            Glide.with(this).load(Url.url_img+phoneData.getResult().getDs().get(0).getRemark6()).into(MCF_imageView_zhengmian);
            MCF_button_addSubmit.setVisibility(View.GONE);

            Entity_phoneData.ResultBean.DsBean bean = phoneData.getResult().getDs().get(0);
            iniCompanyInfo(bean);
        }else{
            ll_businessinfo.setVisibility(View.INVISIBLE);
        }
    }

    private void iniCompanyInfo(Entity_phoneData.ResultBean.DsBean phoneData){
        txt_societycreditcode.setText(phoneData.getCompany1());
        String businesScope = phoneData.getCompany3();
        if(businesScope.length()>35){
            businesScope = businesScope.substring(0,20)+"...";
        }
        txt_businessscope.setText(businesScope);
        txt_startdate.setText(phoneData.getCompany4());
        txt_legalperson.setText(phoneData.getCompany5());
        txt_registercapital.setText(phoneData.getCompany6());
        txt_address.setText(phoneData.getCompany8());
        txt_companyname.setText(phoneData.getCompany9());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && data != null && requestCode == 6) {
            myselectPath = Matisse.obtainPathResult(data);
            myselectUri = Matisse.obtainResult(data);
            MCF_imageView_zhengmian.setImageURI(myselectUri.get(0));
            MCF_imageView_zhengmian.setScaleType(ImageView.ScaleType.FIT_XY);
        }

    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //选择执照照片
    public void click_selectImg(View view) {
        Matisse.from(this).choose(MimeType.ofImage()).capture(true).
                captureStrategy(new CaptureStrategy(true,"com.example.parttime.fileProvider","photo")).showSingleMediaType(true).maxSelectable(1).thumbnailScale(0.8f).imageEngine(new GlideEngine()).forResult(6);
    }

    //认证
    public void click_submit(View view) {
        if(myselectPath!=null) {
            if (myselectPath.get(0) == null || myselectPath.get(0).equals("")) {
                Toasty.warning(this, "请选择企业营业执照", Toasty.LENGTH_SHORT).show();
                return;
            }
            uploadCheckCard();
        }
    }

    //上传
    private void uploadCheckCard(){
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("认证中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        UploadCompanyCard uploadCompanyCard = new UploadCompanyCard();
        uploadCompanyCard.setId(phoneData.getResult().getDs().get(0).getId());
        String uploadData = new Gson().toJson(uploadCompanyCard);

        File imageFile = new File(myselectPath.get(0));

        RxHttp.setDebug(true);
        RxHttp.postForm(Url.url_addCompanyCard)
                .add("json", uploadData)
                .addFile("index",imageFile)
                .upload(progress -> {
                    //上传进度回调,0-100，仅在进度有更新时才会回调
                    int currentProgress = progress.getProgress(); //当前进度 0-100
                    long currentSize = progress.getCurrentSize(); //当前已上传的字节大小
                    long totalSize = progress.getTotalSize();     //要上传的总字节大小
                }, AndroidSchedulers.mainThread())
                .asString()//指定返回类型数据
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        //上传成功
                        Toasty.warning(this, "认证成功", Toasty.LENGTH_SHORT).show();
                        finish();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.error(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    //请求失败
                    xPopupLoading.dismiss();
                    Toasty.error(this, "认证失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }


    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        ll_businessinfo = this.findViewById(R.id.ll_businessinfo);
        MCF_imageView_zhengmian = this.findViewById(R.id.MCF_imageView_zhengmian);
        MCF_button_addSubmit = this.findViewById(R.id.MCF_button_addSubmit);
        MCF_textView_top = this.findViewById(R.id.MCF_textView_top);

        txt_societycreditcode = this.findViewById(R.id.txt_societycreditcode);
        txt_organizationform = this.findViewById(R.id.txt_organizationform);
        txt_businessscope = this.findViewById(R.id.txt_businessscope);
        txt_startdate = this.findViewById(R.id.txt_startdate);
        txt_address = this.findViewById(R.id.txt_address);
        txt_companyname = this.findViewById(R.id.txt_companyname);
        txt_legalperson = this.findViewById(R.id.txt_legalperson);
        txt_registercapital = this.findViewById(R.id.txt_registercapital);

        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);
    }


    //身份照片上传数据
    class UploadCompanyCard{

        /**
         * id : 3
         */

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

    }

}