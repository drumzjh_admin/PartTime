package com.example.parttime.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.example.parttime.R;
import com.example.parttime.adapter.MyDataWorkAdapter;
import com.example.parttime.adapter.MyMoneyDetailAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_datawork;
import com.example.parttime.entity.Entity_moneyDetail;
import com.example.parttime.service.Url;
import com.example.parttime.tool.Dialog_data_work;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.rxjava.rxlife.RxLife;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//余额明细界面
public class MyMoneyDetailActivity extends AppCompatActivity {

    ConstraintLayout MMD_constraintLayout_nodata;
    RecyclerView MMD_recyclerView;
    TabLayout MMD_tabLayout;

    private MyMoneyDetailAdapter myMoneyDetailAdapter;
    private List<Entity_moneyDetail.ResultBean.DsBean> entity_moneyDetailList;
    private String stringPeopleId;//登录人ID

    Entity_moneyDetail.ResultBean.DsBean dsBean;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_money_detail);
        initActivity();
        getDetailCZ();
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //获取充值明细
    private void getDetailCZ() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_addMoneyDetail)//发送Get请求（获取使用中的任务）
                .add("employee_id", stringPeopleId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_moneyDetail entity_moneyDetail = gson.fromJson(jsonResult, Entity_moneyDetail.class);
                        //跳转任务详情界面
                        if (entity_moneyDetail.getResult().getDs().size() != 0) {
                            MMD_constraintLayout_nodata.setVisibility(View.GONE);
                            MMD_recyclerView.setVisibility(View.VISIBLE);
                            entity_moneyDetailList = new ArrayList<>();
                            entity_moneyDetailList = entity_moneyDetail.getResult().getDs();
                            Collections.reverse(entity_moneyDetailList);
                            myMoneyDetailAdapter.setList(entity_moneyDetailList);
                        } else {
                            Toasty.warning(this, "暂无充值明细", Toasty.LENGTH_SHORT).show();
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            MMD_recyclerView.setVisibility(View.GONE);
                            MMD_constraintLayout_nodata.setVisibility(View.VISIBLE);
                            entity_moneyDetailList = new ArrayList<>();
                            myMoneyDetailAdapter.setList(entity_moneyDetailList);
                        }else {
                            Toasty.warning(this, "充值明细加载失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "充值明细加载出错", Toasty.LENGTH_SHORT).show();

                });
    }

    //获取提现明细
    private void getDetailTX() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_takeMoneyDetail)//发送Get请求（获取使用中的任务）
                .add("employee_id", stringPeopleId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_moneyDetail entity_moneyDetail = gson.fromJson(jsonResult, Entity_moneyDetail.class);
                        //跳转任务详情界面
                        if (entity_moneyDetail.getResult().getDs().size() != 0) {
                            MMD_constraintLayout_nodata.setVisibility(View.GONE);
                            MMD_recyclerView.setVisibility(View.VISIBLE);
                            entity_moneyDetailList = new ArrayList<>();
                            entity_moneyDetailList = entity_moneyDetail.getResult().getDs();
                            Collections.reverse(entity_moneyDetailList);
                            myMoneyDetailAdapter.setList(entity_moneyDetailList);
                        } else {
                            Toasty.warning(this, "暂无提现明细", Toasty.LENGTH_SHORT).show();
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            MMD_recyclerView.setVisibility(View.GONE);
                            MMD_constraintLayout_nodata.setVisibility(View.VISIBLE);
                            entity_moneyDetailList = new ArrayList<>();
                            myMoneyDetailAdapter.setList(entity_moneyDetailList);
                        }else {
                            Toasty.warning(this, "提现明细加载失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "提现明细加载出错", Toasty.LENGTH_SHORT).show();

                });
    }

    //顶部tab切换
    TabLayout.OnTabSelectedListener onTabSelectedListenerTabMoney = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {//选中

            //初始化数据
            entity_moneyDetailList = new ArrayList<>();
            myMoneyDetailAdapter.setList(entity_moneyDetailList);

            if (tab.getText().equals("充值")) {//充值

                getDetailCZ();

            } else if (tab.getText().equals("提现")) {//提现

                getDetailTX();

            }

        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {//取消选中时

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {//点击已选中

        }
    };

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MMD_constraintLayout_nodata = this.findViewById(R.id.MMD_constraintLayout_nodata);
        MMD_tabLayout = this.findViewById(R.id.MMD_tabLayout);
        MMD_recyclerView = this.findViewById(R.id.MMD_recyclerView);
        MMD_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myMoneyDetailAdapter = new MyMoneyDetailAdapter(R.layout.item_money_detail);
        MMD_recyclerView.setAdapter(myMoneyDetailAdapter);
        //监听处理事件
        MMD_tabLayout.addOnTabSelectedListener(onTabSelectedListenerTabMoney);

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

    }

}