package com.example.parttime.my;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_moneyAndMortgage;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.rxjava.rxlife.RxLife;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//金额提现界面
public class MyTakeMoneyActivity extends AppCompatActivity {

    EditText MTM_editTextText_money;

    private String stringPeopleId;//登陆人ID
    private UploadExtractMoney uploadExtractMoney;
    private Entity_moneyAndMortgage.ResultBean.DsBean entity_moneyAndMortgage;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_take_money);
        initActivity();
        uploadExtractMoney = new UploadExtractMoney();
        entity_moneyAndMortgage = (Entity_moneyAndMortgage.ResultBean.DsBean) getIntent().getSerializableExtra("mymoney");
        if (entity_moneyAndMortgage != null) {
            uploadExtractMoney.setId(stringPeopleId);
            uploadExtractMoney.setLockbalance(entity_moneyAndMortgage.getLockbalance());
            uploadExtractMoney.setLockmargin(entity_moneyAndMortgage.getLockmargin());
        } else {
            finish();
        }
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //提现
    public void click_takeMoney(View view){
        HelpTool.showhideKeyboard(MyTakeMoneyActivity.this);//关闭软键盘

        if (MTM_editTextText_money.getText().toString().length() <= 0) return;

        int money = Integer.parseInt(MTM_editTextText_money.getText().toString().trim());

        if (money <= 0) {
            Toasty.warning(this, "请输入正确的金额", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (uploadExtractMoney == null) {
            Toasty.warning(this, "提现出错请重试", Toasty.LENGTH_SHORT).show();
            return;
        }

        uploadExtractMoney.setMoney(MTM_editTextText_money.getText().toString().trim());

        new XPopup.Builder(this).asConfirm("提示", "确认提现", new OnConfirmListener() {
            @Override
            public void onConfirm() {

                xPopupLoading = new XPopup.Builder(MyTakeMoneyActivity.this).dismissOnTouchOutside(false).asLoading("提现中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

                //调用服务器接口获取提现数据
                extractMoney();

            }
        }).show();

    }

    //提现
    private void extractMoney() {

        String uploadMoneyString = new Gson().toJson(uploadExtractMoney);

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_extractMoney)//发送Get请求
                .add("strjSon", uploadMoneyString)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {

                        Toasty.warning(this, "提现成功进入审核期(7天)", Toasty.LENGTH_SHORT).show();
                        finish();

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_httpResult.getErrorMsg().equals("提现金额太大")) {
                            Toasty.warning(this, "提现金额超出余额", Toasty.LENGTH_SHORT).show();
                        } else {
                            Toasty.warning(this, "提现失败请重试", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "提现失败请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());//登陆人ID

        //页面控件绑定
        MTM_editTextText_money = this.findViewById(R.id.MTM_editTextText_money);

        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);
    }

    //提现对象类
    class UploadExtractMoney {

        /**
         * id : 3
         * money : 2
         * lockbalance : 20200403121212000
         * lockmargin : 20200403121212000
         */

        private String id;
        private String money;
        private String lockbalance;
        private String lockmargin;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getLockbalance() {
            return lockbalance;
        }

        public void setLockbalance(String lockbalance) {
            this.lockbalance = lockbalance;
        }

        public String getLockmargin() {
            return lockmargin;
        }

        public void setLockmargin(String lockmargin) {
            this.lockmargin = lockmargin;
        }
    }

}