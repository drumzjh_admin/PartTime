package com.example.parttime.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.example.parttime.R;
import com.example.parttime.adapter.MyCollectionAdapter;
import com.example.parttime.adapter.MyDataWorkAdapter;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_datawork;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Entity_taskManagement;
import com.example.parttime.massage.ImCommunicationActivity;
import com.example.parttime.service.Url;
import com.example.parttime.square.SquareDetailedActivity;
import com.example.parttime.tool.Dialog_data_work;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.lxj.xpopup.interfaces.OnSelectListener;
import com.rxjava.rxlife.RxLife;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//工作经历界面
public class MyDataWorkActivity extends AppCompatActivity {

    ConstraintLayout MDW_constraintLayout_nodata;
    RecyclerView MDW_recyclerView;

    private MyDataWorkAdapter myDataWorkAdapter;
    private List<Entity_datawork.ResultBean.DsBean> entity_dataworkList;
    private String stringPeopleId;//登录人ID

    Entity_datawork.ResultBean.DsBean dsBean;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_data_work);
        initActivity();
        getWork();
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    //添加工作经历
    public void click_addWork(View view) {

        Dialog_data_work dialog_data_work = new Dialog_data_work(this,null,null,null);
        dialog_data_work.setOnClickListenerAdd(onClickListenerAdd);
        new XPopup.Builder(this).asCustom(dialog_data_work).show();

    }
    Dialog_data_work.OnClickListenerAdd onClickListenerAdd = new Dialog_data_work.OnClickListenerAdd() {
        @Override
        public void onClick(String nameText, String start, String end) {

            //确定后实体类赋值
            dsBean = new Entity_datawork.ResultBean.DsBean();
            dsBean.setWorksContent(nameText);
            dsBean.setRemark1(start);
            dsBean.setRemark2(end);
            dsBean.setEmpid(stringPeopleId);
            postModifyaAddWork(new Gson().toJson(dsBean), "添加");

        }
    };

    //修改工作经历
    private void modifyWork(String nameText, String start, String end) {

        Dialog_data_work dialog_data_work = new Dialog_data_work(this,nameText, start, end);
        dialog_data_work.setOnClickListenerAdd(onClickListenerModify);
        new XPopup.Builder(this).asCustom(dialog_data_work).show();

    }
    Dialog_data_work.OnClickListenerAdd onClickListenerModify = new Dialog_data_work.OnClickListenerAdd() {
        @Override
        public void onClick(String nameText, String start, String end) {

            //确定后实体类赋值
            dsBean.setWorksContent(nameText);
            dsBean.setRemark1(start);
            dsBean.setRemark2(end);
            dsBean.setEmpid(stringPeopleId);
            postModifyaAddWork(new Gson().toJson(dsBean), "修改");

        }
    };

    //删除工作经历
    private void clearWork(int position) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("删除中").show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_clearWork)//发送Get请求（获取使用中的任务）
                .add("id", entity_dataworkList.get(position).getId())//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(this, "删除成功", Toasty.LENGTH_SHORT).show();
                        getWork();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "删除出错", Toasty.LENGTH_SHORT).show();
                });
    }

    //获取工作经历
    private void getWork() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").show();//初始化加载窗

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getWorkList)//发送Get请求（获取使用中的任务）
                .add("employee_listid", stringPeopleId)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_datawork entity_datawork = gson.fromJson(jsonResult, Entity_datawork.class);
                        //跳转任务详情界面
                        if (entity_datawork.getResult().getDs().size() != 0) {
                            MDW_constraintLayout_nodata.setVisibility(View.GONE);
                            MDW_recyclerView.setVisibility(View.VISIBLE);
                            entity_dataworkList = new ArrayList<>();
                            entity_dataworkList = entity_datawork.getResult().getDs();
                            myDataWorkAdapter.setList(entity_dataworkList);

                        } else {
                            Toasty.warning(this, "暂无工作经历信息", Toasty.LENGTH_SHORT).show();
                        }

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            MDW_recyclerView.setVisibility(View.GONE);
                            MDW_constraintLayout_nodata.setVisibility(View.VISIBLE);
                            entity_dataworkList = new ArrayList<>();
                            myDataWorkAdapter.setList(entity_dataworkList);
                        }else {
                            Toasty.warning(this, "工作经历加载失败", Toasty.LENGTH_SHORT).show();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "工作经历加载出错", Toasty.LENGTH_SHORT).show();

                });
    }

    //修改添加工作经历
    private void postModifyaAddWork(String uploadJson, String type) {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading(type+"中").show();//初始化加载窗

        String url = Url.url_modifyWork;
        if (type.equals("添加")) {
            url = Url.url_addWork;
        }

        RxHttp.setDebug(true);
        RxHttp.get(url)//发送Get请求（获取使用中的任务）
                .add("strjSon", uploadJson)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Toasty.warning(this, type + "成功", Toasty.LENGTH_SHORT).show();
                        getWork();
                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, entity_httpResult.getErrorMsg(), Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, type + "出错", Toasty.LENGTH_SHORT).show();
                });
    }

    //长按
    OnItemLongClickListener onItemLongClickListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {

            //弹出选择框
            String[] selectString = {"修改", "删除"};

            new XPopup.Builder(MyDataWorkActivity.this).asCenterList("", selectString, new OnSelectListener() {
                @Override
                public void onSelect(int position, String text) {
                    if (position == 0) {

                        dsBean = new Entity_datawork.ResultBean.DsBean();
                        dsBean.setId(entity_dataworkList.get(position-1).getId());
                        modifyWork(dsBean.getWorksContent(),dsBean.getRemark1(),dsBean.getRemark2());

                    } else {
                        clearWork(position-1);
                    }
                }
            }).show();

            return false;
        }
    };

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        MDW_constraintLayout_nodata = this.findViewById(R.id.MDW_constraintLayout_nodata);
        MDW_recyclerView = this.findViewById(R.id.MDW_recyclerView);
        MDW_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        myDataWorkAdapter = new MyDataWorkAdapter(R.layout.item_mydatawork);
        MDW_recyclerView.setAdapter(myDataWorkAdapter);
        //监听处理事件
        myDataWorkAdapter.setOnItemLongClickListener(onItemLongClickListener);//长按

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());

    }

}