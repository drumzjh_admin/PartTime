package com.example.parttime.my;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.parttime.HomeActivity;
import com.example.parttime.LoginActivity;
import com.example.parttime.R;
import com.example.parttime.RegisteredActivity;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_addMoney;
import com.example.parttime.entity.Entity_moneyAndMortgage;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.massage.ImCommunicationActivity;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.example.parttime.wxapi.WXPayBean;
import com.example.parttime.wxapi.WXPayTool;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.rxjava.rxlife.RxLife;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.wechat.friends.Wechat;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import rxhttp.wrapper.param.RxHttp;

//金额充值界面
public class MyAddMoneyActivity extends AppCompatActivity {

    EditText MAM_editTextText_money;

    private String stringPeopleId;//登陆人ID
    private UploadMoney uploadMoney;
    private Entity_moneyAndMortgage.ResultBean.DsBean entity_moneyAndMortgage;

    //加载窗
    private BasePopupView xPopupLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_add_money);
        initActivity();
        uploadMoney = new UploadMoney();
        entity_moneyAndMortgage = (Entity_moneyAndMortgage.ResultBean.DsBean) getIntent().getSerializableExtra("mymoney");
        if (entity_moneyAndMortgage != null) {
            uploadMoney.setId(stringPeopleId);
            uploadMoney.setLockbalance(entity_moneyAndMortgage.getLockbalance());
            uploadMoney.setLockmargin(entity_moneyAndMortgage.getLockmargin());
        } else {
            finish();
        }
    }

    //返回上一页
    public void click_finish(View view) {
        finish();
    }

    public void click_addMoney(View view) {

        HelpTool.showhideKeyboard(MyAddMoneyActivity.this);//关闭软键盘

        Platform platform = ShareSDK.getPlatform(Wechat.NAME);
        if (!platform.isClientValid()) {//判断是否已安装微信客户端
            Toasty.warning(MyAddMoneyActivity.this, "请先安装微信客户端", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (MAM_editTextText_money.getText().toString().length() <= 0) return;

        int money = Integer.parseInt(MAM_editTextText_money.getText().toString().trim());

        if (money <= 0) {
            Toasty.warning(this, "请输入正确的金额", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (uploadMoney == null) {
            Toasty.warning(this, "充值出错请重试", Toasty.LENGTH_SHORT).show();
            return;
        }

        uploadMoney.setMoney(MAM_editTextText_money.getText().toString().trim());

        new XPopup.Builder(this).asConfirm("提示", "确认充值", new OnConfirmListener() {
            @Override
            public void onConfirm() {

                xPopupLoading = new XPopup.Builder(MyAddMoneyActivity.this).dismissOnTouchOutside(false).asLoading("充值中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

                //调用服务器接口获取充值数据
                getAddMoneyResult();

            }
        }).show();

    }

    //调用服务器接口获取充值数据
    private void getAddMoneyResult() {

        String uploadMoneyString = new Gson().toJson(uploadMoney);

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_addMoneyOne)//发送Get请求
                .add("strJson", uploadMoneyString)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();
                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    if (jsonStates.equals("1")) {
                        Entity_addMoney entity_addMoney = gson.fromJson(jsonResult, Entity_addMoney.class);
                        Entity_addMoney.ResultBean.WeiXinBean weiXinBean = entity_addMoney.getResult().getWeiXin();

                        //存储调用对象用于充值完成后传值至后台
                        uploadMoney.setOrderNo(weiXinBean.getOrderNo());
                        HelpTool.setSharedPreferenceString("uploadMoneyString",new Gson().toJson(uploadMoney),MyAddMoneyActivity.this);

                        WXPayBean wxPayBean = new WXPayBean();
                        wxPayBean.setAppid(weiXinBean.getAppid());
                        wxPayBean.setPartnerid(weiXinBean.getPartnerid());
                        wxPayBean.setPrepayid(weiXinBean.getPrepayid());
                        wxPayBean.setPackageVlaue(weiXinBean.getPackageX());
                        wxPayBean.setNoncestr(weiXinBean.getNoncestr());
                        wxPayBean.setTimestamp(weiXinBean.getTimestamp());
                        wxPayBean.setSign(weiXinBean.getSign());

                        WXPayTool.startWxPay(wxPayBean,MyAddMoneyActivity.this);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_httpResult = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        Toasty.warning(this, "支付失败，请重试", Toasty.LENGTH_SHORT).show();
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(this, "支付失败，请重试", Toasty.LENGTH_SHORT).show();
                });

    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());//登陆人ID

        //页面控件绑定
        MAM_editTextText_money = this.findViewById(R.id.MAM_editTextText_money);

        //监听单击事件
        //textView_search.setOnClickListener(onClickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MAM_editTextText_money.setText("");
    }

    class UploadMoney {
        String id;
        String money;
        String lockbalance;
        String lockmargin;
        String orderNo;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getLockbalance() {
            return lockbalance;
        }

        public void setLockbalance(String lockbalance) {
            this.lockbalance = lockbalance;
        }

        public String getLockmargin() {
            return lockmargin;
        }

        public void setLockmargin(String lockmargin) {
            this.lockmargin = lockmargin;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }
    }

}