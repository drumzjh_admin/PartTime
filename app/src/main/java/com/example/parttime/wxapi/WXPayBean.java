package com.example.parttime.wxapi;
//微信支付实体类
public class WXPayBean {

    private String appid;//微信开放平台审核通过的应用APPID
    private String partnerid;//微信支付分配的商户号1601139984
    private String prepayid;//微信返回的支付交易会话ID
    private String packageVlaue;//暂填写固定值Sign=WXPay
    private String noncestr;//随机字符串，不长于32位
    private String timestamp;//时间戳
    private String sign;//签名

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getPrepayid() {
        return prepayid;
    }

    public void setPrepayid(String prepayid) {
        this.prepayid = prepayid;
    }

    public String getPackageVlaue() {
        return packageVlaue;
    }

    public void setPackageVlaue(String packageVlaue) {
        this.packageVlaue = packageVlaue;
    }

    public String getNoncestr() {
        return noncestr;
    }

    public void setNoncestr(String noncestr) {
        this.noncestr = noncestr;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
