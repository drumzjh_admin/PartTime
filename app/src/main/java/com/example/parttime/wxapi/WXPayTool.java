package com.example.parttime.wxapi;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

//微信支付帮助类
public class WXPayTool {

    //调起微信支付
    public static void startWxPay(WXPayBean wxPayBean, Activity activity) {

        Log.v("wxpayBean",new Gson().toJson(wxPayBean));
        IWXAPI msgApi = WXAPIFactory.createWXAPI(activity, "wx8748cf377b6a0bf2");
        msgApi.registerApp("wx8748cf377b6a0bf2");

        PayReq payReq = new PayReq();
        payReq.appId = wxPayBean.getAppid();
        payReq.partnerId = wxPayBean.getPartnerid();
        payReq.prepayId = wxPayBean.getPrepayid();
        payReq.packageValue = wxPayBean.getPackageVlaue();
        payReq.nonceStr = wxPayBean.getNoncestr();
        payReq.timeStamp = wxPayBean.getTimestamp();
        payReq.sign = wxPayBean.getSign();
        //发起支付请求
        msgApi.sendReq(payReq);

    }


}
