package com.example.parttime.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.parttime.R;
import com.example.parttime.my.MyMoneyActivity;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import rxhttp.wrapper.param.RxHttp;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {
    private static String TAG = "MicroMsg.WXEntryActivity";

    private IWXAPI api;
    private String appId = "wx8748cf377b6a0bf2";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        api = WXAPIFactory.createWXAPI(this, appId);
        api.handleIntent(getIntent(), this);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
//		switch (req.getType()) {
//		case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
//			Toast.makeText(this, "1", Toast.LENGTH_LONG).show();
//			break;
//		case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
//			Toast.makeText(this, "2", Toast.LENGTH_LONG).show();
//			break;
//		default:
//			break;
//		}
//        finish();
    }

    //支付结果回调
    @Override
    public void onResp(BaseResp resp) {
        int result = 0;

        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK://支付成功返回
                result = R.string.errcode_success;
                //调用支付成功接口
                uploadAddMoneySuccess();
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL://支付被用户取消
                result = R.string.errcode_cancel;
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED://支付出现异常
                result = R.string.errcode_deny;
                break;
            case BaseResp.ErrCode.ERR_UNSUPPORT:
                result = R.string.errcode_unsupported;
                break;
            default:
                result = R.string.errcode_unknown;
                break;
        }

        //Toast.makeText(this, getString(result) + ", type=" + resp.getType(), Toast.LENGTH_SHORT).show();

        Log.v("支付情况：",getString(result) + ", type=" + resp.getType());


//		if (resp.getType() == ConstantsAPI.COMMAND_SUBSCRIBE_MESSAGE) {
//			SubscribeMessage.Resp subscribeMsgResp = (SubscribeMessage.Resp) resp;
//			String text = String.format("openid=%s\ntemplate_id=%s\nscene=%d\naction=%s\nreserved=%s",
//					subscribeMsgResp.openId, subscribeMsgResp.templateID, subscribeMsgResp.scene, subscribeMsgResp.action, subscribeMsgResp.reserved);
//
//			Toast.makeText(this, text, Toast.LENGTH_LONG).show();
//		}
//
//        if (resp.getType() == ConstantsAPI.COMMAND_LAUNCH_WX_MINIPROGRAM) {
//            WXLaunchMiniProgram.Resp launchMiniProgramResp = (WXLaunchMiniProgram.Resp) resp;
//            String text = String.format("openid=%s\nextMsg=%s\nerrStr=%s",
//                    launchMiniProgramResp.openId, launchMiniProgramResp.extMsg,launchMiniProgramResp.errStr);
//
//            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
//        }
//
//        if (resp.getType() == ConstantsAPI.COMMAND_OPEN_BUSINESS_VIEW) {
//            WXOpenBusinessView.Resp launchMiniProgramResp = (WXOpenBusinessView.Resp) resp;
//            String text = String.format("openid=%s\nextMsg=%s\nerrStr=%s\nbusinessType=%s",
//                    launchMiniProgramResp.openId, launchMiniProgramResp.extMsg,launchMiniProgramResp.errStr,launchMiniProgramResp.businessType);
//
//            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
//        }
//
//        if (resp.getType() == ConstantsAPI.COMMAND_OPEN_BUSINESS_WEBVIEW) {
//            WXOpenBusinessWebview.Resp response = (WXOpenBusinessWebview.Resp) resp;
//            String text = String.format("businessType=%d\nresultInfo=%s\nret=%d",response.businessType,response.resultInfo,response.errCode);
//
//            Toast.makeText(this, text, Toast.LENGTH_LONG).show();
//        }
//
//		if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
//			SendAuth.Resp authResp = (SendAuth.Resp)resp;
//			final String code = authResp.code;
//			Toast.makeText(this, code, Toast.LENGTH_LONG).show();
//		}
        finish();
    }


    //支付成功调用
    private void uploadAddMoneySuccess() {

        String uploadMoneyString = HelpTool.getSharedPreferenceString("uploadMoneyString", WXPayEntryActivity.this);

        RxHttp.setDebug(true);
        RxHttp.get(Url.url_addMoneyTwo)//发送Get请求
                .add("strJson", uploadMoneyString)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .subscribe(json -> {
                }, throwable -> {
                });

    }


}