package com.example.parttime.massage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.parttime.R;
import com.gyf.immersionbar.ImmersionBar;

//系统消息界面
public class SystemMsgActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_system_msg);
        initActivity();
    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //初始化页面工作
    private void initActivity() {
        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        //页面控件绑定
        //IC_textView_titleName = this.findViewById(R.id.IC_textView_titleName);

    }

}