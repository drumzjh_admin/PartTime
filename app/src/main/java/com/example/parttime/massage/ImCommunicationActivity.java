package com.example.parttime.massage;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.parttime.LoginActivity;
import com.example.parttime.R;
import com.example.parttime.entity.Entity_HttpResult;
import com.example.parttime.entity.Entity_peopleCard;
import com.example.parttime.entity.Entity_phoneData;
import com.example.parttime.entity.Entity_taskData;
import com.example.parttime.entity.Upload_addTask;
import com.example.parttime.service.Url;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.google.gson.Gson;
import com.gyf.immersionbar.ImmersionBar;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BasePopupView;
import com.lxj.xpopup.interfaces.OnConfirmListener;
import com.rxjava.rxlife.RxLife;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.rong.callkit.RongCallKit;
import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationFragment;
import io.rong.imkit.userInfoCache.RongUserInfoManager;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.UserInfo;
import io.rong.message.TextMessage;
import rxhttp.wrapper.param.RxHttp;

//任务沟通页面
public class ImCommunicationActivity extends AppCompatActivity implements RongIM.UserInfoProvider {

    TextView IC_textView_titleName;
    TextView IC_textView_acceptTask;

    ConstraintLayout IC_constraintLayout16;
    ConstraintLayout IC_constraintLayout17;
    ConstraintLayout IC_constraintLayout19;
    FrameLayout IC_frameLayout;
    TextView IC_textView_title;
    TextView IC_textView_money;
    TextView IC_textView_timeData;
    TextView IC_textView_peopleName;
    TextView IC_textView_wageState;
    CircleImageView IC_imageView_peopleHead;

    ImageButton IC_imageButton_acceptTask;

    Entity_taskData.ResultBean.DsBean squareDetailed;
    private String taskId;
    private String stringPeopleId;
    private Entity_phoneData phoneData;

    //加载窗
    private BasePopupView xPopupLoading;
    private int currentTaskStatus;//1.能接受 2.已接受 -1.已接受且无法取消

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_im_communication);
        initActivity();

        //获取传值任务的ID
        taskId = HelpTool.getSharedPreferenceString("taskId", this);
        if (taskId != null) {
            getTaskData();
        } else {
            Toasty.warning(getApplicationContext(), "对话加载出错", Toasty.LENGTH_SHORT).show();
            finish();
        }

        initTaskStatus();
    }

    //返回上一页
    public void click_finish(View view) {
        this.finish();
    }

    //打开视频通话
    public void click_videoCall(View view) {
        RongCallKit.startSingleCall(ImCommunicationActivity.this, squareDetailed.getEmployee_list_id(), RongCallKit.CallMediaType.CALL_MEDIA_TYPE_VIDEO);
    }

    //接受这个任务
    public void click_acceptTask(View view) {

        new XPopup.Builder(ImCommunicationActivity.this).asConfirm("提示", "是否接受任务","取消","确定", new OnConfirmListener() {
            @Override
            public void onConfirm() {
                acceptTask();
            }
        },null,false).show();

    }
    private void acceptTask() {

        String stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());
        //校验
        if (stringPeopleId == null || stringPeopleId.equals("")) {

            Toasty.error(this, "请重新登录", Toasty.LENGTH_SHORT).show();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();

        } else {
            xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading().show();//初始化加载窗

            Upload_addTask upload_addTask = new Upload_addTask();
            upload_addTask.setEmpid(stringPeopleId);
            upload_addTask.setTask_id(taskId);

            String uploadData = new Gson().toJson(upload_addTask);

            RxHttp.setDebug(true);
            RxHttp.get(Url.url_acceptTask)//发送Get请求
                    .add("strjSon", uploadData)//添加请求参数，该方法可调用多次
                    .asString()//指定返回类型数据
                    .observeOn(AndroidSchedulers.mainThread())
                    .as(RxLife.as(this))
                    .subscribe(json -> {
                        xPopupLoading.dismiss();

                        //请求成功
                        String jsonStates = HelpTool.ParseHttpResult(json)[0];

                        if (jsonStates.equals("1")) {
                            Toasty.warning(this, "已添加至我的任务", Toasty.LENGTH_SHORT).show();
                        } else if (jsonStates.equals("0")) {
                            Toasty.error(this, "任务接受失败", Toasty.LENGTH_SHORT).show();
                        }

                    }, throwable -> {
                        //请求失败
                        xPopupLoading.dismiss();
                        Toasty.error(this, "任务接受失败请重试", Toasty.LENGTH_SHORT).show();
                    });

        }
    }


    //发送任务详情
    public void click_sendTaskCenter(View view){

        //发送消息内容
        String contentString = "任务名称 : "+squareDetailed.getM_task_list_remark5()+"\n任务薪资 : "+squareDetailed.getMoney() + "元\n开始时间 : "+
                squareDetailed.getWorktimeStart() + "\n完成时间 : " + squareDetailed.getWorktimeEnd()+"\n任务地址 : "+
                squareDetailed.getAddress1() + "-" + squareDetailed.getAddress2() + "-" + squareDetailed.getAddress3() + "-" + squareDetailed.getAddress4();

        String targetId=squareDetailed.getEmployee_list_id();//对方ID

        Conversation.ConversationType conversationType = Conversation.ConversationType.PRIVATE;

        TextMessage textMessage  = TextMessage.obtain(contentString);
        Message message = Message.obtain(targetId,conversationType,textMessage);
        RongIM.getInstance().sendMessage(message,null,null,iSendMessageCallback);

    }

    IRongCallback.ISendMessageCallback iSendMessageCallback = new IRongCallback.ISendMediaMessageCallback() {
        @Override
        public void onProgress(Message message, int i) {

        }

        @Override
        public void onCanceled(Message message) {

        }

        @Override
        public void onAttached(Message message) {
            /**
             * 消息发送前回调, 回调时消息已存储数据库
             * @param message 已存库的消息体
             */

        }

        @Override
        public void onSuccess(Message message) {
            /**
             * 消息发送成功。
             * @param message 发送成功后的消息体
             */

        }

        @Override
        public void onError(Message message, RongIMClient.ErrorCode errorCode) {
            /**
             * 消息发送失败
             * @param message   发送失败的消息体
             * @param errorCode 具体的错误
             */

        }
    };

    //界面信息赋值
    private void viewAssignment(Entity_taskData entity_taskData){

        squareDetailed = entity_taskData.getResult().getDs().get(0);

        String name = "***";
        if (!squareDetailed.getM_employee_list_remark1().equals("") && squareDetailed.getM_employee_list_remark1() != null) {
            name = squareDetailed.getM_employee_list_remark1();
        }

        IC_textView_titleName.setText(name);
        IC_textView_peopleName.setText(name);
        IC_textView_title.setText(squareDetailed.getM_task_list_remark5());
        IC_textView_money.setText(squareDetailed.getMoney() + "元/天");
        IC_textView_timeData.setText(squareDetailed.getWorktimeStart() + "-" + squareDetailed.getWorktimeEnd());
        //设置发布头像
        if (squareDetailed.getM_employee_list_remark4().equals("") || squareDetailed.getM_employee_list_remark4() == null) {
            IC_imageView_peopleHead.setImageResource(R.drawable.zhanweihead);
        } else {
            Glide.with(this).load(Url.url_img + squareDetailed.getM_employee_list_remark4()).into(IC_imageView_peopleHead);
        }


    }

    //加载任务信息
    private void getTaskData() {
        xPopupLoading = new XPopup.Builder(this).dismissOnTouchOutside(false).asLoading("加载中").bindLayout(R.layout.dialog_loading).show();//初始化加载窗

        //String uploadData = "M_task_list.remark2='1' and M_task_list_id ='"+taskId+"'";
        String uploadData = "M_task_list.id ='" + taskId + "'";
        RxHttp.setDebug(true);
        RxHttp.get(Url.url_getTaskList)//发送Get请求（获取使用中的任务）
                .add("strwhere", uploadData)//添加请求参数，该方法可调用多次
                .add("sindex","0")
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];

                    //添加会话界面
                    ConversationFragment conversationFragment = new ConversationFragment();
                    FragmentManager manager = getSupportFragmentManager();
                    FragmentTransaction transaction = manager.beginTransaction();
                    transaction.replace(R.id.IC_frameLayoutIm, conversationFragment);
                    transaction.commit();

                    if (jsonStates.equals("1")) {
                        IC_constraintLayout16.setVisibility(View.VISIBLE);
                        IC_constraintLayout17.setVisibility(View.VISIBLE);
                        Entity_taskData entity_taskData = gson.fromJson(jsonResult, Entity_taskData.class);

                        viewAssignment(entity_taskData);

                    } else if (jsonStates.equals("0")) {
                        Entity_HttpResult entity_taskData = gson.fromJson(jsonResult, Entity_HttpResult.class);
                        if (entity_taskData.getErrorMsg().equals("无数据")) {
                            IC_constraintLayout19.setVisibility(View.VISIBLE);
                            Log.v("entity_taskData", "无数据");
                        } else {
                            Toasty.warning(getApplicationContext(), "未获取到任务信息", Toasty.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), "任务加载错误", Toasty.LENGTH_SHORT).show();
                    finish();
                });


    }

    //初始化页面工作
    private void initActivity() {

        ImmersionBar.with(this).statusBarDarkFont(true).navigationBarColor(R.color.colorWhite).statusBarColor(R.color.colorWhite).init();

        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());//登陆人ID
        //获取用户信息
        String phoneDataString = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINDATA,getApplicationContext());
        phoneData = new Gson().fromJson(phoneDataString,Entity_phoneData.class);

        RongIM.setUserInfoProvider(this,true);//设置聊天对象信息

        //页面控件绑定
        IC_textView_titleName = this.findViewById(R.id.IC_textView_titleName);
        IC_textView_acceptTask = this.findViewById(R.id.IC_textView_acceptTask);
        IC_constraintLayout16 = this.findViewById(R.id.IC_constraintLayout16);
        IC_constraintLayout17 = this.findViewById(R.id.IC_constraintLayout17);
        IC_constraintLayout19 = this.findViewById(R.id.IC_constraintLayout19);
        IC_frameLayout = this.findViewById(R.id.IC_frameLayoutIm);
        IC_textView_title = this.findViewById(R.id.IC_textView_title);
        IC_textView_money = this.findViewById(R.id.IC_textView_money);
        IC_textView_timeData = this.findViewById(R.id.IC_textView_timeData);
        IC_textView_peopleName = this.findViewById(R.id.IC_textView_peopleName);
        IC_textView_wageState = this.findViewById(R.id.IC_textView_wageState);
        IC_imageView_peopleHead = this.findViewById(R.id.IC_imageView_peopleHead);

        IC_imageButton_acceptTask = this.findViewById(R.id.IC_imageButton_acceptTask);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //ImmersionBar.destroy(this);

    }

    //初始化任务状态改变任务图标
    private void initTaskStatus(){
        String stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getApplicationContext());
        String taskId = HelpTool.getSharedPreferenceString("taskId", this);
        String url = Url.url_taskStatus;
        RxHttp.setDebug(true);

        TaskStatusPara taskStatusPara = new TaskStatusPara();
        taskStatusPara.setEmpid(stringPeopleId);
        taskStatusPara.setTask_id(taskId);
        String uploadData = new Gson().toJson(taskStatusPara);

        RxHttp.get(url)//发送Get请求（获取使用中的任务）
                .add("jsons", uploadData)//添加请求参数，该方法可调用多次
                .asString()//指定返回类型数据
                .observeOn(AndroidSchedulers.mainThread())
                .as(RxLife.as(this))
                .subscribe(json -> {
                    xPopupLoading.dismiss();

                    //请求成功
                    Gson gson = new Gson();
                    String jsonStates = HelpTool.ParseHttpResult(json)[0];
                    String jsonResult = HelpTool.ParseHttpResult(json)[1];
                    Entity_HttpResult taskStatus = gson.fromJson(jsonResult, Entity_HttpResult.class);

                    if (taskStatus.getResult().equals("3")) {//可以接单

                        //提示无法接收的原因，并将接受图标更换
//                        IC_imageButton_acceptTask.setImageDrawable(R.drawable.accpettask);
//                        IC_imageButton_acceptTask.setImageResource((int)R.drawable.acceptedtask);
                        IC_imageButton_acceptTask.setBackgroundResource((int) R.drawable.acceptedtask);
                    }
//                        //设置当前任务接受状态并查看是否可以取消
//                        else if(taskStatus.getResult().equals("3")){//已接任务可取消
//                            //可以接单
//                        }else if(){//已接任务不能取消
//
//                    }
                        switch (taskStatus.getResult()){
                            case "0"://获取任务失败
                                break;
                            case "1"://您已接收任务并且能取消
                                currentTaskStatus = 2;
                                IC_imageButton_acceptTask.setBackgroundResource((int) R.drawable.acceptedtask);
                                IC_imageButton_acceptTask.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //取消任务
                                    }
                                });
                                break;
                            case "2"://人数已满
                                break;
                            case "3"://可以接
                                currentTaskStatus = 1;
                                IC_imageButton_acceptTask.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //接受任务
                                    }
                                });
                                break;
                            case "4"://已接并且人数已满并且能取消
                                currentTaskStatus = 2;
                                IC_imageButton_acceptTask.setBackgroundResource((int) R.drawable.acceptedtask);
                                int people = 1;
                                IC_imageButton_acceptTask.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //取消任务
                                    }
                                });
                                break;
                            case "5"://已接并且人数已满并且不能取消
                                currentTaskStatus = -3;
                                IC_imageButton_acceptTask.setBackgroundResource((int) R.drawable.acceptedtask);
                                break;
                            case "6"://您已接收任务并且不能取消
                                currentTaskStatus = -3;
                                IC_imageButton_acceptTask.setBackgroundResource((int) R.drawable.acceptedtask);
                                IC_imageButton_acceptTask.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //提示人数已满且不能取消任务
                                    }
                                });
                                break;
                        }
                }, throwable -> {
                    xPopupLoading.dismiss();
                    //请求失败
                    Toasty.warning(getApplicationContext(), "任务加载错误", Toasty.LENGTH_SHORT).show();
                    finish();
                });
    }

    @Override
    public UserInfo getUserInfo(String userId) {

        //设置聊天对象信息
        UserInfo userInfo;
        String name = "***";
        String urlImg = getResources().getResourceName(R.drawable.logo);

        if(RongUserInfoManager.getInstance().getUserInfo(userId) == null){

            if(userId.equals(stringPeopleId)){

                if (!phoneData.getResult().getDs().get(0).getRemark1().equals("") && phoneData.getResult().getDs().get(0).getRemark1() != null) {
                    name = phoneData.getResult().getDs().get(0).getRemark1();
                }

                //设置发布头像
                if (!phoneData.getResult().getDs().get(0).getRemark4().equals("") || phoneData.getResult().getDs().get(0).getRemark4() != null) {
                    urlImg = Url.url_img + phoneData.getResult().getDs().get(0).getRemark4();
                }

            }else {
                if(squareDetailed!=null) {
                    if (!squareDetailed.getM_employee_list_remark1().equals("") && squareDetailed.getM_employee_list_remark1() != null) {
                        name = squareDetailed.getM_employee_list_remark1();
                    }

                    //设置发布头像
                    if (!squareDetailed.getM_employee_list_remark4().equals("") || squareDetailed.getM_employee_list_remark4() != null) {
                        urlImg = Url.url_img + squareDetailed.getM_employee_list_remark4();
                    }
                }

            }
            userInfo = new UserInfo(userId, name, Uri.parse(urlImg));
        }else {
            userInfo = RongUserInfoManager.getInstance().getUserInfo(userId);
        }

        return userInfo;

    }
}

class TaskStatusPara{

    /**
     * empid : 3
     * content : lalala
     */

    private String empid;

    public String getTask_id() {
        return Task_id;
    }

    public void setTask_id(String task_id) {
        Task_id = task_id;
    }

    private String Task_id;

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }
}