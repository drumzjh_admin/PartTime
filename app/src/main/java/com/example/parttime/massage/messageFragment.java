package com.example.parttime.massage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.duma.ld.mylibrary.SwitchView;
import com.example.parttime.R;
import com.example.parttime.tool.HelpTool;
import com.example.parttime.tool.StorageConfig;
import com.gyf.immersionbar.ImmersionBar;

import java.util.HashMap;
import java.util.Map;

import io.rong.imkit.RongIM;
import io.rong.imkit.fragment.ConversationListFragment;
import io.rong.imkit.userInfoCache.RongUserInfoManager;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.UserInfo;
import io.rong.push.RongPushClient;

/**
 * A simple {@link Fragment} subclass.
 */
//消息对话的列表页面
public class messageFragment extends Fragment implements RongIM.UserInfoProvider {

    SwitchView FMG_switchView;
    FrameLayout FMG_frameLayout_yinping;
    FrameLayout FMG_frameLayout_fabu;
    ImageButton FMG_imageButton_shoucang;

    //聊天
    ConversationListFragment conversationListFragmentYP;
    FragmentManager managerYP;
    FragmentTransaction transactionYP;
    Uri uriYP;

    private String stringPeopleId;

    public messageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initActivity();
    }


    //系统的消息
    public void click_sysMSg(){
        startActivity(new Intent(getActivity(),SystemMsgActivity.class));
    }

    //顶部switch切换
    SwitchView.onClickCheckedListener onClickCheckedListenerSwitchTask = new SwitchView.onClickCheckedListener() {
        @Override
        public void onClick() {

            if (FMG_switchView.isChecked()) {//应聘
                FMG_frameLayout_yinping.setVisibility(View.VISIBLE);
                FMG_frameLayout_fabu.setVisibility(View.GONE);
            } else {//发布
                FMG_frameLayout_yinping.setVisibility(View.GONE);
                FMG_frameLayout_fabu.setVisibility(View.VISIBLE);
            }

        }
    };

    //单击
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.FMG_imageButton_shoucang){
                click_sysMSg();
            }
        }
    };

    //初始化页面工作
    private void initActivity() {

        //页面控件绑定
        FMG_switchView = getActivity().findViewById(R.id.FMG_switchView);
        FMG_frameLayout_yinping = getActivity().findViewById(R.id.FMG_frameLayout_yinping);
        FMG_frameLayout_fabu = getActivity().findViewById(R.id.FMG_frameLayout_fabu);
        FMG_imageButton_shoucang = getActivity().findViewById(R.id.FMG_imageButton_shoucang);

        //监听控件事件
        FMG_switchView.setOnClickCheckedListener(onClickCheckedListenerSwitchTask);
        FMG_imageButton_shoucang.setOnClickListener(onClickListener);

        //登录人ID
        stringPeopleId = HelpTool.getSharedPreferenceString(StorageConfig.EDITOR_LOGINID, getContext());

        RongIM.setUserInfoProvider(this,true);//设置聊天对象信息

        //设置应聘消息列表
        conversationListFragmentYP = new ConversationListFragment();
        // 此处设置 Uri. 通过 appendQueryParameter 去设置所要支持的会话类型. 例如
        // .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(),"false")
        // 表示支持单聊会话, false 表示不聚合显示, true 则为聚合显示
        uriYP = Uri.parse("rong://" +
                getActivity().getApplicationInfo().packageName).buildUpon()
                .appendPath("conversationlist")
                .appendQueryParameter(Conversation.ConversationType.PRIVATE.getName(), "false") //设置私聊会话是否聚合显示
                .build();

        conversationListFragmentYP.setUri(uriYP);
        managerYP = getFragmentManager();
        transactionYP = managerYP.beginTransaction();
        transactionYP.replace(R.id.FMG_frameLayout_yinping, conversationListFragmentYP);
        transactionYP.commit();

    }

    @Override
    public void onResume() {
        super.onResume();

        //刷新会话列表
        conversationListFragmentYP.setUri(uriYP);
        managerYP = getFragmentManager();
        transactionYP = managerYP.beginTransaction();
        transactionYP.replace(R.id.FMG_frameLayout_yinping, conversationListFragmentYP);
        transactionYP.commit();
    }

    @Override
    public UserInfo getUserInfo(String userId) {
        return RongUserInfoManager.getInstance().getUserInfo(userId);
    }
}